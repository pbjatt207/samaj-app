<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sender_id')->nullable();
            $table->foreign('sender_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('receiver_id')->nullable();
            $table->foreign('receiver_id')->references('id')->on('users')->onDelete('cascade');
            $table->enum('type', ['event', 'news', 'like', 'comment', 'post', 'reply'])->nullable();
            $table->longText('message')->nullable();
            $table->unsignedBigInteger('event_id')->nullable();
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->unsignedBigInteger('post_id')->nullable();
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
            $table->unsignedBigInteger('like_post_id')->nullable();
            $table->foreign('like_post_id')->references('id')->on('like_posts')->onDelete('cascade');
            $table->unsignedBigInteger('comment_post_id')->nullable();
            $table->foreign('comment_post_id')->references('id')->on('comment_posts')->onDelete('cascade');
            $table->unsignedBigInteger('like_comment_id')->nullable();
            $table->foreign('like_comment_id')->references('id')->on('like_comments')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
