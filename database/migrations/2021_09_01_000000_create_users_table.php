<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->enum('title', ['Mr', 'Mrs', 'Miss'])->default('Mr');
            $table->string('fname');
            $table->string('lname')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile')->unique();
            $table->string('user_name')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('otp')->nullable();
            $table->enum('type', ['vip', 'personal', 'business'])->default('personal');
            $table->enum('privacy_status', ['private', 'public', 'lock'])->default('public');
            $table->enum('is_mobile', ['true', 'false'])->default('false');
            $table->enum('is_email', ['true', 'false'])->default('false');
            $table->enum('is_active', ['true', 'false'])->default('false');
            $table->unsignedBigInteger('gotra_id')->nullable();
            $table->foreign('gotra_id')->references('id')->on('gotras')->onDelete('cascade');
            $table->unsignedBigInteger('role_id')->nullable();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->unsignedBigInteger('profession_id')->nullable();
            $table->foreign('profession_id')->references('id')->on('professions')->onDelete('cascade');
            $table->unsignedBigInteger('meritalstatus_id')->nullable();
            $table->foreign('meritalstatus_id')->references('id')->on('meritalstatuses')->onDelete('cascade');
            $table->unsignedBigInteger('city_id')->nullable();
            $table->string('image')->nullable();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->string('pincode')->nullable();
            $table->text('address')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
