<?php

namespace App;

use App\Model\City;
use App\Model\Country;
use App\Model\Connection;
use App\Model\Gotra;
use App\Model\Post;
use App\Model\Role;
use App\Model\Site;
use App\Model\SiteUser;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $guarded = [];
    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function site_user()
    {
        return $this->belongsToMany(Site::class, SiteUser::class);
    }
    public function gotra()
    {
        return $this->belongsTo(Gotra::class);
    }
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }
    public function posts()
    {
        return $this->hasMany(Post::class, 'user_id', 'id');
    }
    public function following()
    {
        return $this->hasMany(Connection::class, 'connection_id', 'id');
    }
    public function follower()
    {
        return $this->hasMany(Connection::class, 'user_id', 'id');
    }
}
