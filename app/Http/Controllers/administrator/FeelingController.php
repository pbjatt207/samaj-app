<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Feeling;
use Illuminate\Http\Request;


class FeelingController extends Controller
{
    public function index()
    {
        return view('admin.pages.feeling.index');
    }

    public function create()
    {
        return view('admin.pages.feeling.create');
    }

    public function store(Request $request)
    {
        $feeling = new Feeling($request->except(['_token']));
        $feeling->save();

        return redirect(route('admin.feeling.index'))->with('success', '<strong>Success!!</strong> Feeling has been created.');
    }

    public function show(Feeling $feeling)
    {
        //
    }

    public function edit(Feeling $feeling)
    {
        $input = $feeling->toArray();
        $input['feeling_id'] = $feeling->feeling_id;
        request()->merge($input);
        request()->flash();

        $feelings = Feeling::pluck('name', 'id');

        return view('admin.pages.feeling.edit', compact('feeling', 'feelings'));
    }

    public function update(Request $request, Feeling $feeling)
    {
        $input = $request->except(['_method', '_token']);

        $feeling->fill($input);
        $feeling->save();

        return redirect(route('admin.feeling.index'))->with('success', '<strong>Success!!</strong> Feeling information has been updated.');
    }

    public function destroy(Feeling $feeling)
    {
        $feeling->delete();
        return redirect()->back()->with('success', '<strong>Success!!</strong> Selected record has been successfully deleted.');
    }
}
