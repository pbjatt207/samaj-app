<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Religion;
use Illuminate\Http\Request;


class ReligionController extends Controller
{
    public function index()
    {
        return view('admin.pages.religion.index');
    }

    public function create()
    {
        request()->replace([]);
        request()->flash();


        return view('admin.pages.religion.create');
    }

    public function store(Request $request)
    {
        $religion = new Religion($request->except(['country_id', '_token']));
        $religion->save();

        return redirect(route('admin.religion.index'))->with('success', '<strong>Success!!</strong> Religion has been created.');
    }

    public function show(Religion $religion)
    {
        //
    }

    public function edit(Religion $religion)
    {
        $input = $religion->toArray();
        request()->merge($input);
        request()->flash();


        return view('admin.pages.religion.edit', compact('religion'));
    }

    public function update(Request $request, Religion $religion)
    {
        $input = $request->except(['_method', '_token']);

        $religion->fill($input);
        $religion->save();

        return redirect(route('admin.religion.index'))->with('success', '<strong>Success!!</strong> Religion information has been updated.');
    }

    public function destroy(Religion $religion)
    {
        $religion->delete();
        return redirect()->back()->with('success', '<strong>Success!!</strong> Selected record has been successfully deleted.');
    }
}
