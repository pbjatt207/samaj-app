<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use Illuminate\Http\Request;


class CityController extends Controller
{
    public function index()
    {
        return view('admin.pages.city.index');
    }

    public function create()
    {
        request()->replace([]);
        request()->flash();

        $countries = Country::pluck('name', 'id');
        $states    = [];

        return view('admin.pages.city.create', compact('countries', 'states'));
    }

    public function store(Request $request)
    {
        $city = new City($request->except(['country_id', '_token']));
        $city->save();

        return redirect(route('admin.city.index'))->with('success', '<strong>Success!!</strong> City has been created.');
    }

    public function show(City $city)
    {
        //
    }

    public function edit(City $city)
    {
        $input = $city->toArray();
        $input['state_id'] = $city->state_id;
        $input['country_id'] = $city->state->country_id;
        request()->merge($input);
        request()->flash();

        $countries = Country::pluck('name', 'id');
        $states    =  [];

        if ($city->state_id) {
            $states = State::where('country_id', $city->state->country_id)->pluck('name', 'id');
        }
        return view('admin.pages.city.edit', compact('city', 'countries', 'states'));
    }

    public function update(Request $request, City $city)
    {
        $input = $request->except(['_method', '_token', 'country_id']);

        $city->fill($input);
        $city->save();

        return redirect(route('admin.city.index'))->with('success', '<strong>Success!!</strong> City information has been updated.');
    }

    public function destroy(City $city)
    {
        $city->delete();
        return redirect()->back()->with('success', '<strong>Success!!</strong> Selected record has been successfully deleted.');
    }
}
