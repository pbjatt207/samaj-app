<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Religion;
use App\Model\Cast;
use App\Model\Gotra;
use Illuminate\Http\Request;


class GotraController extends Controller
{
    public function index()
    {
        return view('admin.pages.gotra.index');
    }

    public function create()
    {
        request()->replace([]);
        request()->flash();

        $religions = Religion::pluck('name', 'id');
        $casts    = [];

        return view('admin.pages.gotra.create', compact('religions', 'casts'));
    }

    public function store(Request $request)
    {
        $gotra = new Gotra($request->except(['religion_id', '_token']));
        $gotra->save();

        return redirect(route('admin.gotra.index'))->with('success', '<strong>Success!!</strong> Gotra has been created.');
    }

    public function show(Gotra $gotra)
    {
        //
    }

    public function edit(Gotra $gotra)
    {
        $input = $gotra->toArray();
        $input['state_id'] = $gotra->state_id;
        $input['religion_id'] = $gotra->state->religion_id;
        request()->merge($input);
        request()->flash();

        $religions = Country::pluck('name', 'id');
        $casts    =  [];

        if ($gotra->state_id) {
            $casts = State::where('religion_id', $gotra->state->religion_id)->pluck('name', 'id');
        }
        return view('admin.pages.gotra.edit', compact('gotra', 'religions', 'casts'));
    }

    public function update(Request $request, Gotra $gotra)
    {
        $input = $request->except(['_method', '_token', 'religion_id']);

        $gotra->fill($input);
        $gotra->save();

        return redirect(route('admin.gotra.index'))->with('success', '<strong>Success!!</strong> Gotra information has been updated.');
    }

    public function destroy(Gotra $gotra)
    {
        $gotra->delete();
        return redirect()->back()->with('success', '<strong>Success!!</strong> Selected record has been successfully deleted.');
    }
}
