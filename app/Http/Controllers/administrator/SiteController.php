<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Religion;
use App\Model\Cast;
use App\Model\Site;
use App\Model\SiteReligion;
use Illuminate\Http\Request;


class SiteController extends Controller
{
    protected function upload_image($request, $item)
    {
        if ($request->hasFile('logo')) {
            // if (file_exists($item->logo)) {
            // unlink(storage_path('app/public/' . $item->logo));
            // }
            $image = $request->logo;
            $filename = 'IMAGE_' . sprintf('%06d', $item->id) . '.' . $image->getClientOriginalExtension();
            // dd($item);
            // $item->logo = $image->storeAs('public/logo', $filename);

            $image->storeAs('public/logo/', $filename);
            $item->logo = 'logo/' . $filename;

            $item->logo .= '?v=' . time();
            $item->save();
        }
    }
    public function index()
    {
        return view('admin.pages.site.index');
    }

    public function create()
    {
        $religions = Religion::pluck('name', 'id');
        $casts    = [];

        return view('admin.pages.site.create', compact('religions', 'casts'));
    }

    public function store(Request $request)
    {
        $site = new Site($request->except(['religion_id', 'cast_id', '_token']));
        $site->save();

        $this->upload_image($request, $site);

        $site->religions()->attach($request->religion_id);
        $site->casts()->attach($request->cast_id);

        return redirect(route('admin.site.index'))->with('success', '<strong>Success!!</strong> Site has been created.');
    }

    public function show(Site $site)
    {
        //
    }

    public function edit(Site $site)
    {
        $input = $site->toArray();
        $reli = [];
        foreach ($site->religions as $key => $r) {
            $reli[] = $r->pivot->religion_id;
        }
        $cast = [];
        foreach ($site->casts as $key => $r) {
            $cast[] = $r->pivot->cast_id;
        }
        $input['cast_id'] = $cast;
        $input['religion_id'] = $reli;
        // dd($input);
        request()->merge($input);
        request()->flash();

        $religions = Religion::pluck('name', 'id');
        $casts    =  [];

        if ($input['religion_id']) {
            $casts = Cast::where('religion_id', $input['religion_id'])->pluck('name', 'id');
        }
        return view('admin.pages.site.edit', compact('site', 'religions', 'casts'));
    }

    public function update(Request $request, Site $site)
    {
        $input = $request->except(['religion_id', 'cast_id', '_method', '_token']);

        $site->fill($input);
        $site->save();

        $this->upload_image($request, $site);

        $site->religions()->attach($request->religion_id);
        $site->casts()->attach($request->cast_id);

        return redirect(route('admin.site.index'))->with('success', '<strong>Success!!</strong> Site information has been updated.');
    }

    public function destroy(Site $site)
    {
        $site->delete();
        return redirect()->back()->with('success', '<strong>Success!!</strong> Selected record has been successfully deleted.');
    }
}
