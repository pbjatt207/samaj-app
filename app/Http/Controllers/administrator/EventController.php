<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Religion;
use App\Model\Event;
use App\Model\userReligion;
use Illuminate\Http\Request;
use ImageKit\ImageKit;


class EventController extends Controller
{
    public function index()
    {
        return view('admin.pages.event.index');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Event $event)
    {
        //
    }

    public function edit(Event $event)
    {
        //
    }

    public function update(Request $request, Event $event)
    {
        //
    }

    public function destroy(Event $event)
    {
        //
    }

    public function status(Request $request, Event $event)
    {
        if ($event->status == 'true') {
            $event->status = 'false';
            // dd($event);
        } else {
            $event->status = 'true';
        }
        $event->save();

        return redirect()->back()->with('success', 'Success!! Status change successfully.');
    }
}
