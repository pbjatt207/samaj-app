<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Role;
use Illuminate\Http\Request;


class RoleController extends Controller
{
    public function index()
    {
        return view('admin.pages.role.index');
    }

    public function create()
    {
        return view('admin.pages.role.create');
    }

    public function store(Request $request)
    {
        $role = new Role($request->except(['_token']));
        $role->save();

        return redirect(route('admin.role.index'))->with('success', '<strong>Success!!</strong> Role has been created.');
    }

    public function show(Role $role)
    {
        //
    }

    public function edit(Role $role)
    {
        $input = $role->toArray();
        $input['role_id'] = $role->role_id;
        request()->merge($input);
        request()->flash();

        $roles = Role::pluck('name', 'id');

        return view('admin.pages.role.edit', compact('role', 'roles'));
    }

    public function update(Request $request, Role $role)
    {
        $input = $request->except(['_method', '_token']);

        $role->fill($input);
        $role->save();

        return redirect(route('admin.role.index'))->with('success', '<strong>Success!!</strong> Role information has been updated.');
    }

    public function destroy(Role $role)
    {
        $role->delete();
        return redirect()->back()->with('success', '<strong>Success!!</strong> Selected record has been successfully deleted.');
    }
}
