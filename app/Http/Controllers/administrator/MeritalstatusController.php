<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Meritalstatus;
use Illuminate\Http\Request;


class MeritalstatusController extends Controller
{
    public function index()
    {
        return view('admin.pages.meritalstatus.index');
    }

    public function create()
    {
        request()->replace([]);
        request()->flash();


        return view('admin.pages.meritalstatus.create');
    }

    public function store(Request $request)
    {
        $meritalstatus = new Meritalstatus($request->except(['country_id', '_token']));
        $meritalstatus->save();

        return redirect(route('admin.meritalstatus.index'))->with('success', '<strong>Success!!</strong> Meritalstatus has been created.');
    }

    public function show(Meritalstatus $meritalstatus)
    {
        //
    }

    public function edit(Meritalstatus $meritalstatus)
    {
        $input = $meritalstatus->toArray();
        request()->merge($input);
        request()->flash();


        return view('admin.pages.meritalstatus.edit', compact('meritalstatus'));
    }

    public function update(Request $request, Meritalstatus $meritalstatus)
    {
        $input = $request->except(['_method', '_token']);

        $meritalstatus->fill($input);
        $meritalstatus->save();

        return redirect(route('admin.meritalstatus.index'))->with('success', '<strong>Success!!</strong> Meritalstatus information has been updated.');
    }

    public function destroy(Meritalstatus $meritalstatus)
    {
        $meritalstatus->delete();
        return redirect()->back()->with('success', '<strong>Success!!</strong> Selected record has been successfully deleted.');
    }
}
