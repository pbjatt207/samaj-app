<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Religion;
use App\Model\Cast;
use Illuminate\Http\Request;


class CastController extends Controller
{
    public function index()
    {
        return view('admin.pages.cast.index');
    }

    public function create()
    {
        request()->replace([]);
        request()->flash();

        $religions = Religion::pluck('name', 'id');

        return view('admin.pages.cast.create', compact('religions'));
    }

    public function store(Request $request)
    {
        $cast = new Cast($request->except(['_token']));
        $cast->save();

        return redirect(route('admin.cast.index'))->with('success', '<strong>Success!!</strong> Cast has been created.');
    }

    public function show(Cast $cast)
    {
        //
    }

    public function edit(Cast $cast)
    {
        $input = $cast->toArray();
        $input['religion_id'] = $cast->religion_id;
        request()->merge($input);
        request()->flash();

        $religions = Religion::pluck('name', 'id');

        return view('admin.pages.cast.edit', compact('cast', 'religions'));
    }

    public function update(Request $request, Cast $cast)
    {
        $input = $request->except(['_method', '_token']);

        $cast->fill($input);
        $cast->save();

        return redirect(route('admin.cast.index'))->with('success', '<strong>Success!!</strong> Cast information has been updated.');
    }

    public function destroy(Cast $cast)
    {
        $cast->delete();
        return redirect()->back()->with('success', '<strong>Success!!</strong> Selected record has been successfully deleted.');
    }
}
