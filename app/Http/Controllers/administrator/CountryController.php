<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Country;
use Illuminate\Http\Request;


class CountryController extends Controller
{
    public function index()
    {
        return view('admin.pages.country.index');
    }

    public function create()
    {
        request()->replace([]);
        request()->flash();

        $countries = Country::pluck('name', 'id');

        return view('admin.pages.country.create', compact('countries'));
    }

    public function store(Request $request)
    {
        $country = new Country($request->except(['_token']));
        $country->save();

        return redirect(route('admin.country.index'))->with('success', '<strong>Success!!</strong> Country has been created.');
    }

    public function show(Country $country)
    {
        //
    }

    public function edit(Country $country)
    {
        $input = $country->toArray();
        $input['country_id'] = $country->country_id;
        request()->merge($input);
        request()->flash();

        $countries = Country::pluck('name', 'id');

        return view('admin.pages.country.edit', compact('country', 'countries'));
    }

    public function update(Request $request, Country $country)
    {
        $input = $request->except(['_method', '_token']);

        $country->fill($input);
        $country->save();

        return redirect(route('admin.country.index'))->with('success', '<strong>Success!!</strong> Country information has been updated.');
    }

    public function destroy(Country $country)
    {
        $country->delete();
        return redirect()->back()->with('success', '<strong>Success!!</strong> Selected record has been successfully deleted.');
    }
}
