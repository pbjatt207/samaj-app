<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Religion;
use App\Model\Cast;
use App\User;
use App\Model\userReligion;
use Illuminate\Http\Request;


class UserController extends Controller
{
    protected function upload_image($request, $item)
    {
        if ($request->hasFile('logo')) {
            // if (file_exists($item->logo)) {
            // unlink(storage_path('app/public/' . $item->logo));
            // }
            $image = $request->logo;
            $filename = 'IMAGE_' . sprintf('%06d', $item->id) . '.' . $image->getClientOriginalExtension();
            // dd($item);
            // $item->logo = $image->storeAs('public/logo', $filename);

            $image->storeAs('public/logo/', $filename);
            $item->logo = 'logo/' . $filename;

            $item->logo .= '?v=' . time();
            $item->save();
        }
    }
    public function index()
    {
        return view('admin.pages.user.index');
    }

    public function create()
    {
        $religions = Religion::pluck('name', 'id');
        $casts    = [];

        return view('admin.pages.user.create', compact('religions', 'casts'));
    }

    public function store(Request $request)
    {
        $user = new User($request->except(['religion_id', 'cast_id', '_token']));
        $user->save();

        $this->upload_image($request, $user);

        $user->religions()->attach($request->religion_id);
        $user->casts()->attach($request->cast_id);

        return redirect(route('admin.user.index'))->with('success', '<strong>Success!!</strong> User has been created.');
    }

    public function show(User $user)
    {
        //
    }

    public function edit(User $user)
    {
        $input = $user->toArray();
        $reli = [];
        foreach ($user->religions as $key => $r) {
            $reli[] = $r->pivot->religion_id;
        }
        $cast = [];
        foreach ($user->casts as $key => $r) {
            $cast[] = $r->pivot->cast_id;
        }
        $input['cast_id'] = $cast;
        $input['religion_id'] = $reli;
        // dd($input);
        request()->merge($input);
        request()->flash();

        $religions = Religion::pluck('name', 'id');
        $casts    =  [];

        if ($input['religion_id']) {
            $casts = Cast::where('religion_id', $input['religion_id'])->pluck('name', 'id');
        }
        return view('admin.pages.user.edit', compact('user', 'religions', 'casts'));
    }

    public function update(Request $request, User $user)
    {
        $input = $request->except(['religion_id', 'cast_id', '_method', '_token']);

        $user->fill($input);
        $user->save();

        $this->upload_image($request, $user);

        $user->religions()->attach($request->religion_id);
        $user->casts()->attach($request->cast_id);

        return redirect(route('admin.user.index'))->with('success', '<strong>Success!!</strong> User information has been updated.');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->back()->with('success', '<strong>Success!!</strong> Selected record has been successfully deleted.');
    }
}
