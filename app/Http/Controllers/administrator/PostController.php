<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Religion;
use App\Model\Post;
use App\Model\userReligion;
use Illuminate\Http\Request;
use ImageKit\ImageKit;


class PostController extends Controller
{
    public function index()
    {
        return view('admin.pages.post.index');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Post $user)
    {
        //
    }

    public function edit(Post $user)
    {
        //
    }

    public function update(Request $request, Post $user)
    {
        //
    }

    public function destroy(Post $user)
    {
        //
    }
}
