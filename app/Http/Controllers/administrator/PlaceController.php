<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Religion;
use App\Model\Place;
use App\Model\userReligion;
use Illuminate\Http\Request;
use ImageKit\ImageKit;


class PlaceController extends Controller
{
    public function index()
    {
        return view('admin.pages.place.index');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Place $place)
    {
        //
    }

    public function edit(Place $place)
    {
        //
    }

    public function update(Request $request, Place $place)
    {
        //
    }

    public function destroy(Place $place)
    {
        //
    }
}
