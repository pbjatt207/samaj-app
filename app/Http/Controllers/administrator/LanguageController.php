<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Country;
use App\Model\State;
use App\Model\Language;
use Illuminate\Http\Request;


class LanguageController extends Controller
{
    public function index()
    {
        return view('admin.pages.language.index');
    }

    public function create()
    {
        request()->replace([]);
        request()->flash();

        $countries = Country::pluck('name', 'id');
        $states    = [];

        return view('admin.pages.language.create', compact('countries', 'states'));
    }

    public function store(Request $request)
    {
        $language = new Language($request->except(['country_id', '_token']));
        $language->save();

        return redirect(route('admin.language.index'))->with('success', '<strong>Success!!</strong> language has been created.');
    }

    public function show(Language $language)
    {
        //
    }

    public function edit(Language $language)
    {
        $input = $language->toArray();
        request()->merge($input);
        request()->flash();


        return view('admin.pages.language.edit', compact('language'));
    }

    public function update(Request $request, Language $language)
    {
        $input = $request->except(['_method', '_token']);

        $language->fill($input);
        $language->save();

        return redirect(route('admin.language.index'))->with('success', '<strong>Success!!</strong> language information has been updated.');
    }

    public function destroy(Language $language)
    {
        $language->delete();
        return redirect()->back()->with('success', '<strong>Success!!</strong> Selected record has been successfully deleted.');
    }
}
