<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Profession;
use Illuminate\Http\Request;


class ProfessionController extends Controller
{
    public function index()
    {
        return view('admin.pages.profession.index');
    }

    public function create()
    {
        request()->replace([]);
        request()->flash();


        return view('admin.pages.profession.create');
    }

    public function store(Request $request)
    {
        $profession = new Profession($request->except(['country_id', '_token']));
        $profession->save();

        return redirect(route('admin.profession.index'))->with('success', '<strong>Success!!</strong> Profession has been created.');
    }

    public function show(Profession $profession)
    {
        //
    }

    public function edit(Profession $profession)
    {
        $input = $profession->toArray();
        request()->merge($input);
        request()->flash();


        return view('admin.pages.profession.edit', compact('profession'));
    }

    public function update(Request $request, Profession $profession)
    {
        $input = $request->except(['_method', '_token']);

        $profession->fill($input);
        $profession->save();

        return redirect(route('admin.profession.index'))->with('success', '<strong>Success!!</strong> Profession information has been updated.');
    }

    public function destroy(Profession $profession)
    {
        $profession->delete();
        return redirect()->back()->with('success', '<strong>Success!!</strong> Selected record has been successfully deleted.');
    }
}
