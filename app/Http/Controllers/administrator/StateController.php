<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use Illuminate\Http\Request;


class StateController extends Controller
{
    public function index()
    {
        return view('admin.pages.state.index');
    }

    public function create()
    {
        request()->replace([]);
        request()->flash();

        $countries = Country::pluck('name', 'id');

        return view('admin.pages.state.create', compact('countries'));
    }

    public function store(Request $request)
    {
        $state = new State($request->except(['_token']));
        $state->save();

        return redirect(route('admin.state.index'))->with('success', '<strong>Success!!</strong> State has been created.');
    }

    public function show(State $state)
    {
        //
    }

    public function edit(State $state)
    {
        $input = $state->toArray();
        $input['country_id'] = $state->country_id;
        request()->merge($input);
        request()->flash();

        $countries = Country::pluck('name', 'id');

        return view('admin.pages.state.edit', compact('state', 'countries'));
    }

    public function update(Request $request, State $state)
    {
        $input = $request->except(['_method', '_token']);

        $state->fill($input);
        $state->save();

        return redirect(route('admin.state.index'))->with('success', '<strong>Success!!</strong> State information has been updated.');
    }

    public function destroy(State $state)
    {
        $state->delete();
        return redirect()->back()->with('success', '<strong>Success!!</strong> Selected record has been successfully deleted.');
    }
}
