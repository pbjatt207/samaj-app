<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Religion;
use App\Model\Tag;
use App\Model\userReligion;
use Illuminate\Http\Request;
use ImageKit\ImageKit;


class TagController extends Controller
{
    public function index()
    {
        return view('admin.pages.tag.index');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Tag $tag)
    {
        //
    }

    public function edit(Tag $tag)
    {
        //
    }

    public function update(Request $request, Tag $tag)
    {
        //
    }

    public function destroy(Tag $tag)
    {
        //
    }

    public function status(Request $request, Tag $tag)
    {
        if ($tag->status == 'true') {
            $tag->status = 'false';
            // dd($tag);
        } else {
            $tag->status = 'true';
        }
        $tag->save();

        return redirect()->back()->with('success', 'Success!! Status change successfully.');
    }
}
