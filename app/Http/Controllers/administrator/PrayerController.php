<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Model\Religion;
use App\Model\Prayer;
use App\Model\userReligion;
use Illuminate\Http\Request;
use ImageKit\ImageKit;


class PrayerController extends Controller
{
    public function index()
    {
        return view('admin.pages.prayer.index');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Prayer $prayer)
    {
        //
    }

    public function edit(Prayer $prayer)
    {
        //
    }

    public function update(Request $request, Prayer $prayer)
    {
        //
    }

    public function destroy(Prayer $prayer)
    {
        //
    }
}
