<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Model\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'mobile'    => 'required|numeric|regex:/^\d{10}/'
        ]);

        if (empty($request->otp_code)) {
            $admin = Admin::where('mobile', $request->mobile)->first();

            if (!empty($admin->id)) {
                $otp = rand(100000, 999999);
                $admin->otp_code = $otp;
                $admin->save();

                $message = urlencode('Your OTP Code to login in your Admin Panel is: ' . $otp);
                $key     = "VPX5ztwBUU6EtDgq55xrkA";
                $sender  = "SchERP";
                // $url     = "http://sms.suncitytechno.com/api/mt/SendSMS?APIKey={$key}&senderid={$sender}&channel=Trans&DCS=0&flashsms=0&number={$request->mobile}&text={$message}&route=8";

                // file_get_contents($url);

                return response()->json([
                    'message'   => 'Success! OTP code has been sent to your mobile number.',
                    'otp' => $otp,
                    'auth'      => false
                ], 200);
            } else {
                return response()->json([
                    'message'   => 'Failed! Mobile no. is not exists.'
                ], 401);
            }
        } else {
            $request->validate([
                'mobile'    => 'required|numeric|regex:/^\d{10}/',
                'password'  => 'required',
                'otp_code'  => 'required'
            ]);


            if (auth()->guard('admin')->attempt($request->only(['mobile', 'password', 'otp_code']))) {
                return response()->json([
                    'message'   => 'Success! Redirecting please wait..',
                    'auth'      => true
                ], 201);
            } else {
                return response()->json([
                    'message'   => 'Failed! Password or otp code is not matched.'
                ], 401);
            }
        }
    }

    public function logout()
    {
        auth()->guard('admin')->logout();

        return redirect(route('admin.dashboard'))->with('success', 'Success! You\'ve logged out successfully.');
    }

    public function change_password_view()
    {
        return view('admin.pages.change_password');
    }

    public function change_password(Request $request)
    {
        // dd($request);

        $request->validate([
            'mobile'    => 'required|numeric|regex:/^\d{10}/'
        ]);

        if (empty($request->otp_code)) {
            $admin = Admin::where('mobile', $request->mobile)->first();

            if (!empty($admin->id)) {

                $otp = rand(100000, 999999);
                $admin->otp_code = $otp;
                $admin->save();

                $message = urlencode('Your OTP Code to login in your Agent Panel is: ' . $otp);
                $key     = "VPX5ztwBUU6EtDgq55xrkA";
                $sender  = "SchERP";
                // $url     = "http://sms.suncitytechno.com/api/mt/SendSMS?APIKey={$key}&senderid={$sender}&channel=Trans&DCS=0&flashsms=0&number={$request->mobile}&text={$message}&route=8";

                // file_get_contents($url);

                return response()->json([
                    'message'   => 'Success! OTP code has been sent to your mobile number.',
                    'otp' => $otp,
                    'auth'      => false
                ], 200);
            } else {
                return response()->json([
                    'message'   => 'Failed! Mobile no. is not exists.'
                ], 401);
            }
        } else {
            $validator = Validator::make($request->all(), [
                'mobile'    => 'required|numeric|regex:/^\d{10}/',
                'new_password' => 'required|string|min:6|same:new_password',
                'confirm_password' => 'required|string|min:6|same:new_password'
            ]);

            if ($validator->fails()) {
                $data = array();
                $data['status'] = 'failed';
                $data['data'] = $validator->errors();
                $data['msg'] = $this->validationErrorsToString($validator->errors());
                return response()->json([
                    'message'   => 'Validation',
                    'auth'      => false
                ], 201);
            }


            $user = Auth::guard('admin')->user();
            $user->password = bcrypt($request->get('new_password'));
            $user->save();


            return response()->json([
                'message'   => 'Success! Password has been successfully changed..',
                'auth'      => true
            ], 201);
        }
    }
}
