<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Prayer;
use Validator;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PrayerController extends Controller
{
    protected function upload_image($request, $item)
    {
        if ($request->hasFile('image')) {
            // dd('sdkjdfj');

            // if (file_exists($item->logo)) {
            // unlink(storage_path('app/public/' . $item->image));
            // }

            $image = $request->image;
            $filename = 'IMAGE_' . sprintf('%06d', $item->id) . '.' . $image->getClientOriginalExtension();

            $image->storeAs('public/prayer/image/', $filename);
            $item->image = 'prayer/image/' . $filename;

            $item->image .= '?v=' . time();
            $item->save();
        }
        if ($request->hasFile('file')) {
            // dd('sdkjdfj');

            // if (file_exists($item->logo)) {
            // unlink(storage_path('app/public/' . $item->image));
            // }

            $image = $request->file;
            $filename = 'FILE_' . sprintf('%06d', $item->id) . '.' . $image->getClientOriginalExtension();

            $image->storeAs('public/prayer/file/', $filename);
            $item->file = 'prayer/file/' . $filename;

            $item->file .= '?v=' . time();
            $item->save();
        }
    }

    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'  => ''
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $query = Prayer::with('user')->latest();
            if ($request->user_id) {
                $query->where('user_id', $request->user_id);
            }
            $lists = $query->get();

            if ($lists->isEmpty()) {
                $re = [
                    'status' => false,
                    'message'    => 'No record(s) found.'
                ];
            } else {
                $re = [
                    'status' => true,
                    'message'    => $lists->count() . " records found.",
                    'data'   => $lists
                ];
            }
        }
        return response()->json($re);
    }

    public function list()
    {
        $query = Prayer::latest();

        return DataTables::of($query)
            ->addIndexColumn()
            // ->addColumn('action', function ($post_id) {
            //     $action = "
            //     <a href='" . route('admin.post.edit', $post_id) . "' class='btn btn-sm btn-info action-btn' data-toggle='tooltip' title='Edit'><i class='icon-pencil1'></i></a>
            //     <a href='" . route('admin.post.destroy', $post_id) . "' class='btn btn-sm btn-danger action-btn delete-btn' data-toggle='tooltip' title='Remove'><i class='icon-delete'></i></a>
            // ";
            //     return $action;
            // })
            // ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required',
            'image'     => 'required',
            'caption'   => 'required',
            'type'      => 'required',
            'user_id'   => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->except(['_token', 'image', 'file']);
            $prayer = new Prayer($input);
            // dd($prayer);
            $prayer->save();

            $this->upload_image($request, $prayer);

            $re = [
                'status'    => true,
                'message'   => "Prayer created successfully.",
                'data'      => $prayer
            ];
        }
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Prayer  $prayer
     * @return \Illuminate\Http\Response
     */
    public function show(Prayer $prayer)
    {
        $list = Prayer::with('user')->findOrFail($prayer->id);
        $re = [
            'status' => true,
            'data'   => $list
        ];
        return response()->json($re);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Prayer  $prayer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Prayer $prayer)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required',
            'caption'   => 'required',
            'type'      => 'required',
            'user_id'   => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->except(['_token', '_method', 'image', 'file']);
            $prayer->fill($input);
            $prayer->save();

            $this->upload_image($request, $prayer);

            $re = [
                'status'    => true,
                'message'   => "Prayer updated successfully.",
                'data'      => $prayer
            ];
        }
        return response()->json($re);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Prayer  $prayer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prayer $prayer)
    {
        $prayer->delete();
        $re = [
            'status' => true,
            'message'    => "Prayer deleted successfully.",
        ];
        return response()->json($re);
    }
}
