<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Relation;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class RelationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $relations = Relation::pluck('name', 'id');

        return response()->json($relations);
    }


    public function list()
    {
        $query = Relation::latest();

        return DataTables::of($query)
            ->addIndexColumn()
            ->addColumn('action', function ($relation_id) {
                $action = "
                <a href='" . route('admin.relation.edit', $relation_id) . "' class='btn btn-sm btn-info action-btn' data-toggle='tooltip' title='Edit'><i class='icon-pencil1'></i></a>
                <a href='" . route('admin.relation.destroy', $relation_id) . "' class='btn btn-sm btn-danger action-btn delete-btn' data-toggle='tooltip' title='Remove'><i class='icon-delete'></i></a>
            ";
                return $action;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Relation  $relation
     * @return \Illuminate\Http\Response
     */
    public function show(Relation $relation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Relation  $relation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Relation $relation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Relation  $relation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Relation $relation)
    {
        //
    }
}
