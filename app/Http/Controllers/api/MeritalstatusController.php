<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Meritalstatus;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class MeritalstatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query =  Meritalstatus::query();

        if ($request->type == 'all') {
            $meritalstatuses = $query->pluck('name', 'id');
        } else {
            $meritalstatuses = $query->pluck('name', 'id');
        }

        return response()->json($meritalstatuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $query = Meritalstatus::latest();


        return DataTables::of($query)
            ->addIndexColumn()
            ->addColumn('action', function ($meritalstatus_id) {
                $action = "
                <a href='" . route('admin.meritalstatus.edit', $meritalstatus_id) . "' class='btn btn-sm btn-info action-btn' data-toggle='tooltip' title='Edit'><i class='icon-pencil1'></i></a>
                <a href='" . route('admin.meritalstatus.destroy', $meritalstatus_id) . "' class='btn btn-sm btn-danger action-btn delete-btn' data-toggle='tooltip' title='Remove'><i class='icon-delete'></i></a>
            ";
                return $action;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        //
    }
}
