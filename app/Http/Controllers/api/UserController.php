<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use App\Model\Connection;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{

    protected function upload_image($request, $item)
    {
        if ($request->hasFile('image')) {
            // dd('sdkjdfj');

            // if (file_exists($item->logo)) {
            // unlink(storage_path('app/public/' . $item->image));
            // }

            $image = $request->image;
            $filename = 'IMAGE_' . sprintf('%06d', $item->id) . '.' . $image->getClientOriginalExtension();

            $image->storeAs('public/user/', $filename);
            $item->image = 'user/' . $filename;

            $item->image .= '?v=' . time();
            $item->save();
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'site_id'  => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $query = User::with('site_user');
            if ($request->type) {
                $query->where('type', $request->type);
            }
            if ($request->name) {
                $query->where('user_name', $request->name);
            }
            $lists = $query->get();

            if ($lists->isEmpty()) {
                $re = [
                    'status' => false,
                    'message'    => 'No record(s) found.'
                ];
            } else {
                $re = [
                    'status' => true,
                    'message'    => $lists->count() . " records found.",
                    'data'   => $lists
                ];
            }
        }
        return response()->json($re);
    }

    public function search_user(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'site_id'  => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $query = User::with('site_user');
            if ($request->name) {



                if (!empty($request->name)) {

                    $query->where('user_name', 'LIKE', '%' . $request->name . '%');
                }

                // $query->where('user_name', $request->name);


            } else {
                $query->where('user_name', '');
            }
            $lists = $query->get();
            // if ($lists->isEmpty()) {
            //     $re = [
            //         'status' => false,
            //         'message'    => 'No record(s) found.'
            //     ];
            // } else {
            $re = [
                'status' => true,
                'message'    => $lists->count() . " records found.",
                'data'   => $lists
            ];
            // }
        }
        return response()->json($re);
    }


    public function list()
    {
        $query = User::latest();


        return DataTables::of($query)
            ->addIndexColumn()
            // ->addColumn('action', function ($user_id) {
            //     $action = "
            //     <a href='" . route('admin.user.edit', $user_id) . "' class='btn btn-sm btn-info action-btn' data-toggle='tooltip' title='Edit'><i class='icon-pencil1'></i></a>
            //     <a href='" . route('admin.user.destroy', $user_id) . "' class='btn btn-sm btn-danger action-btn delete-btn' data-toggle='tooltip' title='Remove'><i class='icon-delete'></i></a>
            // ";
            //     return $action;
            // })
            // ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);

        $validator = Validator::make($request->all(), [
            'user_name'  => 'required|unique:users',
            'mobile'  => 'required|unique:users'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $validator = Validator::make($request->all(), [
                'password'  => 'required|min:6',
                'fname'  => 'required',
                'role_id'  => 'required',
                'gotra_id'  => 'required',
                'city_id'  => 'required',
                'pincode'  => 'required',
                'address'  => 'required',
                // 'site_id'  => 'required',
                // 'site_id'  => 'required|array',
            ]);
            if ($validator->fails()) {
                $re = [
                    'status'    => false,
                    'message'   => 'Validations errors found.',
                    'errors'    => $validator->errors()
                ];
            } else {
                $input = $request->except(['_token', 'site_id']);
                $input['password'] = Hash::make($input['password']);
                // dd($input);
                $user = new User($input);
                // dd($user);
                $user->save();

                if ($request->hasFile('image')) {
                    $this->upload_image($request, $user);
                }

                $user->site_user()->attach($request->site_id);

                $re = [
                    'status' => true,
                    'message'    => "User created successfully.",
                    'data'   => $user
                ];
            }
        }
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $list = User::with('gotra', 'role', 'city', 'posts')->withCount('posts')->where('id', $user->id)->first();


        $auth_user = Auth::guard('api')->user();

        $requests = Connection::where('status', 'requested')->where('user_id', $auth_user->id)->pluck('connection_id');
        $followers = Connection::where('status', 'followed')->where('connection_id', $auth_user->id)->pluck('user_id');
        $followings = Connection::where('status', 'followed')->where('user_id', $auth_user->id)->pluck('connection_id');


        $auth_user_request = false;
        foreach ($requests as $key => $r) {

            if ($list->id == $r) {
                $auth_user_request = true;
            }
        }
        $list->auth_user_request = $auth_user_request;

        $auth_user_follower = false;
        foreach ($followers as $key => $f) {
            if ($list->id == $f) {
                $auth_user_follower = true;
            }
        }
        $list->auth_user_follower = $auth_user_follower;

        $auth_user_following = false;
        foreach ($followings as $key => $f) {
            if ($list->id == $f) {
                $auth_user_following = true;
            }
        }
        $list->auth_user_following = $auth_user_following;


        $follower = User::whereHas('follower', function ($q) use ($list) {
            $q->where('connection_id', $list->id)->where('status', 'Followed');
        })->get();
        // dd($follower);
        $list->followers_count = count($follower);
        $list->follower = $follower;

        $following = User::whereHas('following', function ($q) use ($list) {
            $q->where('user_id', $list->id)->where('status', 'Followed');
        })->get();
        $list->following_count = count($following);
        $list->following = $following;

        $re = [
            'status' => true,
            'data'   => $list
        ];


        return response()->json($re);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user)
    {
        $user = User::find($user);
        $validator = Validator::make($request->all(), [
            'user_name'  => 'required|unique:users,user_name,' . $user->id,
            'mobile'  => 'required|unique:users,mobile,' . $user->id
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $validator = Validator::make($request->all(), [
                'fname'  => 'required',
                'role_id'  => 'required',
                'gotra_id'  => 'required',
                'city_id'  => 'required',
                'pincode'  => 'required',
                'address'  => 'required',
                'site_id'  => 'array',
            ]);
            if ($validator->fails()) {
                $re = [
                    'status'    => false,
                    'message'   => 'Validations errors found.',
                    'errors'    => $validator->errors()
                ];
            } else {
                $input = $request->except(['_token', 'site_id']);
                // dd($input);
                $user->fill($input);
                // dd($user);
                $user->save();
                if ($request->site_id) {
                    $user->site_user()->attach($request->site_id);
                }
                if ($request->hasFile('image')) {
                    $this->upload_image($request, $user);
                }

                $re = [
                    'status' => true,
                    'message'    => "User updated successfully.",
                    'data'   => $user
                ];
            }
        }
        return response()->json($re);
    }

    public function updateDetail(Request $request, $user)
    {
        $user = User::find($user);
        $input = $request->except(['_token']);
        $user->fill($input);
        $user->save();

        $re = [
            'status' => true,
            'message'    => "User details updated successfully.",
            'data'   => $user
        ];

        return response()->json($re);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        $re = [
            'status' => true,
            'message'    => "User deleted successfully.",
        ];
        return response()->json($re);
    }
}
