<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\UserRelation;
use Illuminate\Http\Request;

class UserRelationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'user_id'      => 'required',
        ]);
        $query = UserRelation::latest();
        if ($request->post_id) {
            $query->where('user_id', $request->post_id);
        }
        if ($request->relative_id) {
            $query->where('relative_id', $request->relative_id);
        }
        if ($request->relation_id) {
            $query->where('relation_id', $request->relation_id);
        }
        $lists = $query->get();

        if ($lists->isEmpty()) {
            $re = [
                'status' => false,
                'message'    => 'No record(s) found.'
            ];
        } else {
            $re = [
                'status' => true,
                'message'    => $lists->count() . " records found.",
                'data'   => $lists
            ];
        }

        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'relation_id'   => 'required',
            'user_id'       => 'required',
            'relative_id'   => 'required',
        ]);
        $count = UserRelation::where('user_id', $request->user_id)->where('relation_id', $request->relation_id)->where('relative_id', $request->relative_id)->count();
        if (!$count) {
            $input = $request->except(['_token']);
            $userrelation = new UserRelation($input);
            $userrelation->save();

            $re = [
                'status'    => true,
                'message'   => "User relation created successfully.",
                'data'      => $userrelation
            ];
        } else {
            $re = [
                'status'    => true,
                'message'   => "User relation already exists.",
            ];
        }

        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\UserRelation  $userRelation
     * @return \Illuminate\Http\Response
     */
    public function show($relation)
    {
        $list = UserRelation::findOrFail($relation);

        $re = [
            'status' => true,
            'data'   => $list
        ];
        return response()->json($re);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\UserRelation  $userRelation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $relation)
    {
        $request->validate([
            'relation_id'   => 'required',
        ]);

        $userrelation = UserRelation::findOrFail($relation);
        $count = UserRelation::WhereNotIn('id', [$relation])
            ->where('user_id', $request->user_id ? $request->user_id : $userrelation->user_id)
            ->where('relation_id', $request->relation_id ? $request->relation_id : $userrelation->relation_id)
            ->where('relative_id', $request->relative_id ? $request->relative_id : $userrelation->relative_id)->count();

        if (!$count) {
            $input = $request->except(['_token', 'user_id']);
            $userrelation->fill($input);
            $userrelation->save();

            $re = [
                'status'    => true,
                'message'   => "User relation updated successfully.",
                'data'      => $userrelation
            ];
        } else {
            $re = [
                'status'    => true,
                'message'   => "User relation already exists.",
            ];
        }
        return response()->json($re);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\UserRelation  $userRelation
     * @return \Illuminate\Http\Response
     */
    public function destroy($relation)
    {
        $userrelation = UserRelation::findOrFail($relation);
        $userrelation->delete();
        $re = [
            'status' => true,
            'message'    => "User relation deleted successfully.",
        ];
        return response()->json($re);
    }
}
