<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\CommentPost;
use App\Model\Connection;
use App\Model\Event;
use App\Model\Notification;
use App\Model\Post;
use App\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class HomeController extends Controller
{
    public function posts(Request $request)
    {
        $request->validate([
            'type'       => 'required',
        ]);

        $user_id = Auth::guard('api')->user()->id;
        $connected_users = Connection::where('user_id', $user_id)->where('status', 'followed')->pluck('connection_id');
        // $connected_users = array_push($connected_users, $user_id);
        $connected_users[$connected_users->count()] = $user_id;
        // dd($connected_users);
        $query = Post::with('user', 'like_post', 'view_post', 'report_post', 'admin')->where('status', true)->where('type', $request->type);
        if ($request->type == 'post') {
            $lists = $query->whereIn('user_id', $connected_users)->where('privancy', '!=', 'onlyme')->latest()->get();
        } else {
            $lists = $query->latest()->get();
        }
        foreach ($lists as $key => $list) {
            if (count($list->report_post) > 10) {
                $list->status = 'false';
                $list->save();
            }
            $auth_post_like = false;
            foreach ($list->like_post as $key => $value) {
                if ($value->id == $user_id) {
                    $auth_post_like = true;
                }
            }
            $list->count_post_like = count($list->like_post);
            $list->auth_post_like = $auth_post_like;
            $comment = CommentPost::with('like_comment')->where('post_id', $list->id)->where('comment_id', null)->get();
            foreach ($comment as $i => $c) {
                $auth_comment_like = false;

                if ($c->user_id == $user_id) {
                    $auth_comment_like = true;
                }
                $c->count_comment_like = count($c->like_comment);
                $c->auth_comment_like = $auth_comment_like;
                foreach ($c->reply as $key => $cr) {
                    $auth_comment_reply_like = false;
                    if ($cr->user_id == $user_id) {
                        $auth_comment_reply_like = true;
                    }
                    $cr->count_comment_reply_like = count($cr->like_comment);
                    $cr->auth_comment_reply_like = $auth_comment_reply_like;
                }
            }
            $list->comments = $comment;
        }

        if ($lists->isEmpty()) {
            $re = [
                'status' => false,
                'message'    => 'No record(s) found.'
            ];
        } else {
            $re = [
                'status' => true,
                'message'    => $lists->count() . " records found.",
                'data'   => $lists
            ];
        }

        return response()->json($re);
    }

    public function notification(Request $request)
    {
        $user_id = Auth::guard('api')->user()->id;

        $lists = Notification::latest()->where('receiver_id', $user_id)->get();

        $auth_user = Auth::guard('api')->user();

        $requests = Connection::where('status', 'requested')->where('user_id', $auth_user->id)->pluck('connection_id');
        $followers = Connection::where('status', 'followed')->where('connection_id', $auth_user->id)->pluck('user_id');
        $followings = Connection::where('status', 'followed')->where('user_id', $auth_user->id)->pluck('connection_id');

        foreach ($lists as $key => $list) {
            $auth_user_request = false;
            foreach ($requests as $key => $r) {
                if ($list->sender_id == $r) {
                    $auth_user_request = true;
                }
            }
            $list->auth_user_request = $auth_user_request;

            $auth_user_follower = false;
            foreach ($followers as $key => $f) {
                if ($list->sender_id == $f) {
                    $auth_user_follower = true;
                }
            }
            $list->auth_user_follower = $auth_user_follower;

            $auth_user_following = false;
            foreach ($followings as $key => $f) {
                if ($list->sender_id == $f) {
                    $auth_user_following = true;
                }
            }
            $list->auth_user_following = $auth_user_following;
        }

        if ($lists->isEmpty()) {
            $re = [
                'status' => false,
                'message'    => 'No record(s) found.'
            ];
        } else {
            $re = [
                'status' => true,
                'message'    => $lists->count() . " records found.",
                'data'   => $lists
            ];
        }

        return response()->json($re);
    }
}
