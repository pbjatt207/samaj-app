<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\CommentPost;
use App\Model\Connection;
use App\Model\LikePost;
use App\Model\Notification;
use App\Model\Post;
use App\Model\ReportPost;
use App\Model\ViewPost;
use App\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use ImageKit\ImageKit;

class PostController extends Controller
{
    protected function upload_image($request, $item)
    {
        if ($request->hasFile('image')) {
            // dd('sdkjdfj');

            // if (file_exists($item->logo)) {
            // unlink(storage_path('app/public/' . $item->image));
            // }

            $image = $request->image;
            $filename = 'IMAGE_' . sprintf('%06d', $item->id) . '.' . $image->getClientOriginalExtension();

            $image->storeAs('public/post/', $filename);
            $item->image = 'post/' . $filename;

            $item->image .= '?v=' . time();
            $item->save();

            // $imageKit = new ImageKit(
            //     "public_xevlPQU9zeW0GxziB8oA4K6IoDo=",
            //     "private_CN263InlkfMFNjiof+6vcEeQs8A=",
            //     "https://ik.imagekit.io/1wav3mdsphm/event"
            // );
            // $imageKit->uploadFiles(array(
            //     "file" => $request->image, // required
            //     "fileName" => "your_file_name.jpg", // required
            // ));
        }
        if ($request->hasFile('file')) {
            // dd('sdkjdfj');

            // if (file_exists($item->logo)) {
            // unlink(storage_path('app/public/' . $item->image));
            // }

            $image = $request->file;
            $filename = 'FILE_' . sprintf('%06d', $item->id) . '.' . $image->getClientOriginalExtension();

            $image->storeAs('public/prayer/file/', $filename);
            $item->file = 'prayer/file/' . $filename;

            $item->file .= '?v=' . time();
            $item->save();
        }
    }

    public function index(Request $request)
    {
        $request->validate([
            'type'       => 'required',
        ]);

        $auth_id = Auth::guard('api')->user()->id;
        $connected_users = Connection::where('user_id', $auth_id)->where('status', 'followed')->pluck('connection_id');
        $query = Post::with('tag_post', 'mention_post', 'user', 'report_post', 'admin')->where('type', $request->type)->where('status', true);
        if ($request->user_id) {
            $query->where('user_id', $request->user_id);
        }
        if ($request->type == 'post') {
            $lists = $query->whereIn('user_id', $connected_users)->where('privancy', '!=', 'onlyme')->latest()->get();
        } else {
            $lists = $query->latest()->get();
        }

        foreach ($lists as $key => $list) {
            if (count($list->report_post) > 10) {
                $list->status = 'false';
                $list->save();
            }
            $auth_post_like = false;
            foreach ($list->like_post as $key => $value) {
                if ($value->id == $auth_id) {
                    $auth_post_like = true;
                }
            }
            $list->auth_post_like = $auth_post_like;
            $comment = CommentPost::with('like_comment')->where('post_id', $list->id)->where('comment_id', null)->get();
            foreach ($comment as $i => $c) {
                $auth_comment_like = false;
                if ($c->id == $auth_id) {
                    $auth_comment_like = true;
                }
                $c->auth_comment_like = $auth_comment_like;
                foreach ($c->reply as $key => $cr) {
                    $auth_comment_reply_like = false;
                    if ($cr->id == $auth_id) {
                        $auth_comment_reply_like = true;
                    }
                    $cr->auth_comment_reply_like = $auth_comment_reply_like;
                }
            }
            $list->comments = $comment;
        }

        if ($lists->isEmpty()) {
            $re = [
                'status' => false,
                'message'    => 'No record(s) found.'
            ];
        } else {
            $re = [
                'status' => true,
                'message'    => $lists->count() . " records found.",
                'data'   => $lists
            ];
        }

        return response()->json($re);
    }

    public function list()
    {
        $query = Post::latest();

        return DataTables::of($query)
            ->addIndexColumn()
            ->addColumn('image', function ($post) {
                $action = "<img src='" . url('storage/') . '/' . $post->image . "' width='100px' />";
                return $action;
            })
            ->addColumn('user_id', function ($post) {
                $user = User::find($post->user_id);
                // dd($user);
                $action = $user->fname . ' ' . $user->lname;
                return $action;
            })
            // ->addColumn('action', function ($post_id) {
            //     $action = "
            //     <a href='" . route('admin.post.edit', $post_id) . "' class='btn btn-sm btn-info action-btn' data-toggle='tooltip' title='Edit'><i class='icon-pencil1'></i></a>
            //     <a href='" . route('admin.post.destroy', $post_id) . "' class='btn btn-sm btn-danger action-btn delete-btn' data-toggle='tooltip' title='Remove'><i class='icon-delete'></i></a>
            // ";
            //     return $action;
            // })
            ->rawColumns(['user_id', 'image'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
            'caption'       => 'required',
            'user_id'       => 'required',
            'tag_id'           => 'array',
            'mention_id'       => 'array',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->except(['_token', 'image', 'tag_id', 'mention_id']);
            $post = new Post($input);
            $post->save();

            $this->upload_image($request, $post);
            if ($request->tag_id) {
                $post->tag_post()->sync($request->tag_id);
            }
            if ($request->mention_id) {
                $post->mention_post()->sync($request->mention_id);
            }

            $re = [
                'status'    => true,
                'message'   => "Post created successfully.",
                'data'      => $post
            ];
        }
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $user = Auth::guard('api')->user();
        $list = Post::with('tag_post', 'mention_post', 'like_post', 'comment', 'view_post', 'admin')->findOrFail($post->id);


        $auth_post_like = false;
        foreach ($list->like_post as $key => $value) {
            if ($value->id == $user->id) {
                $auth_post_like = true;
            }
        }
        $list->count_post_like = count($list->like_post);
        $list->auth_post_like   = $auth_post_like;

        $comment = CommentPost::with('like_comment')->where('post_id', $list->id)->where('comment_id', null)->get();
        foreach ($comment as $i => $c) {

            $auth_comment_like = false;
            foreach ($c->like_comment as $lc) {
                if ($lc->id == $user->id) {
                    $auth_comment_like = true;
                }
            }
            $c->count_comment_like = count($c->like_comment);
            $c->auth_comment_like = $auth_comment_like;
            foreach ($c->reply as $key => $cr) {

                $auth_comment_reply_like = false;
                foreach ($cr->like_comment as $rl) {
                    if ($rl->id == $user->id) {
                        $auth_comment_reply_like = true;
                    }
                }
                $cr->count_comment_reply_like = count($cr->like_comment);
                $cr->auth_comment_reply_like = $auth_comment_reply_like;
            }
        }
        $list->comments = $comment;

        if (ViewPost::where('user_id', $user->id)->where('post_id', $list->id)->exists()) {
            $message = "View already exists.";
        } else {
            $input = [
                'user_id' => $user->id,
                'post_id' => $list->id,
            ];
            $viewpost = new ViewPost($input);
            $viewpost->save();
        }

        $re = [
            'status' => true,
            'data'   => $list
        ];
        return response()->json($re);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $validator = Validator::make($request->all(), [
            'caption'       => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->except(['_token', 'image', 'user_id', 'tag_id', 'mention_id']);

            $post->fill($input);
            // dd($post);
            $post->save();

            $this->upload_image($request, $post);
            if ($request->tag_id) {
                $post->tag_post()->sync($request->tag_id);
            }
            if ($request->mention_id) {
                $post->mention_post()->sync($request->mention_id);
            }

            $re = [
                'status'    => true,
                'message'   => "Post created successfully.",
                'data'      => $post
            ];
        }
        return response()->json($re);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        $re = [
            'status' => true,
            'message'    => "Post deleted successfully.",
        ];
        return response()->json($re);
    }

    public function reaction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'post_id'       => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $query = LikePost::latest();
            if ($request->post_id) {
                $query->where('post_id', $request->post_id);
            }
            $lists = $query->get();


            $auth_user = Auth::guard('api')->user();

            $requests = Connection::where('status', 'requested')->where('user_id', $auth_user->id)->pluck('connection_id');
            $followers = Connection::where('status', 'followed')->where('connection_id', $auth_user->id)->pluck('user_id');
            $followings = Connection::where('status', 'followed')->where('user_id', $auth_user->id)->pluck('connection_id');

            foreach ($lists as $key => $list) {
                $auth_user_request = false;
                foreach ($requests as $key => $r) {
                    if ($list->user_id == $r) {
                        $auth_user_request = true;
                    }
                }
                $list->auth_user_request = $auth_user_request;

                $auth_user_follower = false;
                foreach ($followers as $key => $f) {
                    if ($list->user_id == $f) {
                        $auth_user_follower = true;
                    }
                }
                $list->auth_user_follower = $auth_user_follower;

                $auth_user_following = false;
                foreach ($followings as $key => $f) {
                    if ($list->user_id == $f) {
                        $auth_user_following = true;
                    }
                }
                $list->auth_user_following = $auth_user_following;
            }


            if ($lists->isEmpty()) {
                $re = [
                    'status' => false,
                    'message'    => 'No record(s) found.'
                ];
            } else {
                $re = [
                    'status' => true,
                    'message'    => $lists->count() . " records found.",
                    'data'   => $lists
                ];
            }
        }

        return response()->json($re);
    }

    public function reactionadd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'       => 'required',
            'post_id'       => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $auth_user = Auth::guard('api')->user();
            $input = $request->except(['_token']);

            if (LikePost::where('user_id', $request->user_id)->where('post_id', $request->post_id)->exists()) {
                $like = LikePost::where('user_id', $request->user_id)->where('post_id', $request->post_id)->first();
                Notification::where('type', 'like')->where('post_id', $request->post_id)->where('like_post_id', $like->id)->delete();
                $like->delete();
            } else {
                $likepost = new LikePost($input);
                $likepost->save();
                $post = Post::find($request->post_id);
                if ($request->user_id != $post->user_id) {
                    $obj = [
                        'sender_id' => $request->user_id,
                        'receiver_id' => $post->user_id,
                        'type' => 'like',
                        'post_id' => $request->post_id,
                        'like_post_id' => $likepost->id,
                    ];
                    $notification = new Notification($obj);
                    $notification->save();
                }
            }

            $re = [
                'status'    => true,
                'message'   => "Reaction changed successfully.",
            ];
        }
        return response()->json($re);
    }

    public function view(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'post_id'       => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $query = ViewPost::latest();
            if ($request->post_id) {
                $query->where('post_id', $request->post_id);
            }
            $lists = $query->get();

            if ($lists->isEmpty()) {
                $re = [
                    'status' => false,
                    'message'    => 'No record(s) found.'
                ];
            } else {
                $re = [
                    'status' => true,
                    'message'    => $lists->count() . " records found.",
                    'data'   => $lists
                ];
            }
        }

        return response()->json($re);
    }

    public function viewadd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'       => 'required',
            'post_id'       => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->except(['_token']);

            if (ViewPost::where('user_id', $request->user_id)->where('post_id', $request->post_id)->exists()) {
                $message = "View already exists.";
            } else {
                $viewpost = new ViewPost($input);
                $viewpost->save();
                $message = "View added successfully.";
            }

            $re = [
                'status'    => true,
                'message'   => $message,
            ];
        }
        return response()->json($re);
    }


    public function report(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'post_id'       => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $query = ReportPost::latest();
            if ($request->post_id) {
                $query->where('post_id', $request->post_id);
            }
            $lists = $query->get();

            if ($lists->isEmpty()) {
                $re = [
                    'status' => false,
                    'message'    => 'No record(s) found.'
                ];
            } else {
                $re = [
                    'status' => true,
                    'message'    => $lists->count() . " records found.",
                    'data'   => $lists
                ];
            }
        }

        return response()->json($re);
    }

    public function reportadd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'       => 'required',
            'post_id'       => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->except(['_token']);

            if (ReportPost::where('user_id', $request->user_id)->where('post_id', $request->post_id)->exists()) {
                $message = "Report already exists.";
            } else {
                $reportpost = new ReportPost($input);
                $reportpost->save();
                $message = "Report added successfully.";
            }

            $re = [
                'status'    => true,
                'message'   => $message,
            ];
        }
        return response()->json($re);
    }
}
