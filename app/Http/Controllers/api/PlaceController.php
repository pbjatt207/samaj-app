<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Place;
use Validator;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PlaceController extends Controller
{
    protected function upload_image($request, $item)
    {
        if ($request->hasFile('image')) {
            // dd('sdkjdfj');

            // if (file_exists($item->logo)) {
            // unlink(storage_path('app/public/' . $item->image));
            // }

            $image = $request->image;
            $filename = 'IMAGE_' . sprintf('%06d', $item->id) . '.' . $image->getClientOriginalExtension();

            $image->storeAs('public/place/', $filename);
            $item->image = 'place/' . $filename;

            $item->image .= '?v=' . time();
            $item->save();
        }
    }

    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'  => ''
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $query = Place::with('user', 'city')->latest();
            if ($request->user_id) {
                $query->where('user_id', $request->user_id);
            }
            $lists = $query->get();

            if ($lists->isEmpty()) {
                $re = [
                    'status' => false,
                    'message'    => 'No record(s) found.'
                ];
            } else {
                $re = [
                    'status' => true,
                    'message'    => $lists->count() . " records found.",
                    'data'   => $lists
                ];
            }
        }
        return response()->json($re);
    }

    public function list()
    {
        $query = Place::latest();

        return DataTables::of($query)

            ->addColumn('image', function ($post) {
                $action = "<img src='" . url('storage/') . '/' . $post->image . "' width='100px' />";
                return $action;
            })
            // ->addIndexColumn()
            // ->addColumn('action', function ($post_id) {
            //     $action = "
            //     <a href='" . route('admin.post.edit', $post_id) . "' class='btn btn-sm btn-info action-btn' data-toggle='tooltip' title='Edit'><i class='icon-pencil1'></i></a>
            //     <a href='" . route('admin.post.destroy', $post_id) . "' class='btn btn-sm btn-danger action-btn delete-btn' data-toggle='tooltip' title='Remove'><i class='icon-delete'></i></a>
            // ";
            //     return $action;
            // })
            ->rawColumns(['image'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'          => 'required',
            'user_id'       => 'required',
            'city_id'       => 'required',
            'type'          => 'required',
            'pincode'        => 'required',
            'address'       => 'required',
            'description'   => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->except(['_token', 'image']);
            $place = new Place($input);
            $place->save();

            $this->upload_image($request, $place);

            $re = [
                'status'    => true,
                'message'   => "Place created successfully.",
                'data'      => $place
            ];
        }
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function show(Place $place)
    {
        $list = Place::with('user', 'city')->findOrFail($place->id);
        $re = [
            'status' => true,
            'data'   => $list
        ];
        return response()->json($re);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Place $place)
    {
        $validator = Validator::make($request->all(), [
            'name'          => 'required',
            'user_id'       => 'required',
            'city_id'       => 'required',
            'type'          => 'required',
            'pincode'        => 'required',
            'address'       => 'required',
            'description'   => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->except(['_token', 'image', 'user_id']);

            $place->fill($input);
            // dd($place);
            $place->save();

            $this->upload_image($request, $place);


            $re = [
                'status'    => true,
                'message'   => "Place created successfully.",
                'data'      => $place
            ];
        }
        return response()->json($re);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function destroy(Place $place)
    {
        $place->delete();
        $re = [
            'status' => true,
            'message'    => "Place deleted successfully.",
        ];
        return response()->json($re);
    }
}
