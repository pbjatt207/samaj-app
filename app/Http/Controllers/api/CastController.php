<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Cast;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query =  Cast::query();

        if ($request->religion_id) {
            $query->where('religion_id', $request->religion_id);
        }

        $casts = $query->pluck('name', 'id');

        return response()->json($casts);
    }

    public function list()
    {
        $query = Cast::latest();


        return DataTables::of($query)
            ->addIndexColumn()
            ->addColumn('action', function ($cast_id) {
                $action = "
                <a href='" . route('admin.cast.edit', $cast_id) . "' class='btn btn-sm btn-info action-btn' data-toggle='tooltip' title='Edit'><i class='icon-pencil1'></i></a>
                <a href='" . route('admin.cast.destroy', $cast_id) . "' class='btn btn-sm btn-danger action-btn delete-btn' data-toggle='tooltip' title='Remove'><i class='icon-delete'></i></a>
            ";
                return $action;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function show(cast $cast)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function edit(cast $cast)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cast $cast)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function destroy(cast $cast)
    {
        //
    }
}
