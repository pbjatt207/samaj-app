<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Connection;
use App\Model\LikePost;
use App\Model\Notification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConnectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'user_id'   => 'required',
            'status'    => 'required',
        ]);

        $query = Connection::latest();
        if ($request->user_id) {
            $query->where('user_id', $request->user_id);
        }
        if ($request->status) {
            $query->where('status', $request->status);
        }
        $lists = $query->get();

        if ($lists->isEmpty()) {
            $re = [
                'status'    => false,
                'message'   => 'No record(s) found.'
            ];
        } else {
            $re = [
                'status'    => true,
                'message'   => $lists->count() . " records found.",
                'data'      => $lists
            ];
        }

        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id'       => 'required',
            'connection_id' => 'required',
            'status'        => 'required',
        ]);
        $check = Connection::where('user_id', $request->user_id)->where('connection_id', $request->connection_id)->first();
        if (!Connection::where('user_id', $request->user_id)->where('connection_id', $request->connection_id)->exists()) {
            $input = $request->except(['_token']);
            $connection = new Connection($input);
            $connection->save();

            if ($connection->status == 'followed') {
                $status = 'follow';
            } else if ($connection->status == 'requested') {
                $status = 'request';
            }
            $obj = [
                'sender_id' => $request->user_id,
                'receiver_id' => $request->connection_id,
                'connection_id' => $connection->id,
                'type' => $status
            ];
            $notification = new Notification($obj);
            $notification->save();

            $re = [
                'status'    => true,
                'message'   => "Connection created successfully.",
                'data'      => $connection
            ];
        } else {

            $re = [
                'status'    => false,
                'message'   => "Connection already " . $check->status . ".",
            ];
        }
        // dd($connection);

        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Connection  $connection
     * @return \Illuminate\Http\Response
     */
    public function show(Connection $connection)
    {
        $re = [
            'status' => true,
            'data'   => $connection
        ];
        return response()->json($re);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Connection  $connection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Connection $connection)
    {
        $request->validate([
            'status'        => 'required',
        ]);

        if (!Connection::WhereNotIn('id', [$connection->id])->where('user_id', $connection->user_id)->where('connection_id', $connection->connection_id)->exists()) {
            $input = $request->except(['_token']);
            $connection->fill($input);
            $connection->save();

            if ($connection->status == 'followed') {
                $status = 'follow';
            } else if ($connection->status == 'requested') {
                $status = 'request';
            }
            Notification::where('connection_id', $connection->id)->delete();
            $obj = [
                'sender_id' => $connection->user_id,
                'receiver_id' => $connection->connection_id,
                'connection_id' => $connection->id,
                'type' => $status
            ];
            $notification = new Notification($obj);
            $notification->save();

            $re = [
                'status'    => true,
                'message'   => "Connection updated successfully.",
                'data'      => $connection
            ];
        } else {

            $re = [
                'status'    => false,
                'message'   => "Connection already " . $connection->status . ".",
            ];
        }
        // dd($connection);

        return response()->json($re);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Connection  $connection
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
    }
    public function delete(Request $request)
    {
        $request->validate([
            'user_id'       => 'required',
            'status'        => 'required',
        ]);
        $auth_user = Auth::guard('api')->user();

        if ($request->status == 'follower') {
            $connection_fetch = Connection::where('user_id', $request->user_id)->where('connection_id', $auth_user->id)->where('status', 'followed')->first();
            Notification::where('type', 'follow')->where('connection_id', $connection_fetch->id)->delete();
            $connection_fetch->delete();
        }
        if ($request->status == 'following') {
            $connection_fetch = Connection::where('user_id', $auth_user->id)->where('connection_id', $request->user_id)->where('status', 'followed')->first();
            Notification::where('type', 'follow')->where('connection_id', $connection_fetch->id)->delete();
            $connection_fetch->delete();
        }
        if ($request->status == 'request_me') {
            $connection_fetch = Connection::where('user_id', $auth_user->id)->where('connection_id', $request->user_id)->where('status', 'requested')->first();
            Notification::where('type', 'request')->where('connection_id', $connection_fetch->id)->delete();
            $connection_fetch->delete();
        }
        if ($request->status == 'requested') {
            $connection_fetch = Connection::where('user_id', $request->user_id)->where('connection_id', $auth_user->id)->where('status', 'requested')->first();
            Notification::where('type', 'request')->where('connection_id', $connection_fetch->id)->delete();
            $connection_fetch->delete();
        }
        // $connection->delete();
        $re = [
            'status' => true,
            'message'    => "Connection deleted successfully.",
        ];
        return response()->json($re);
    }

    public function connection_list(Request $request)
    {
        $request->validate([
            'user_id'   => 'required',
            'type'    => 'required',
        ]);
        $auth_user = Auth::guard('api')->user();
        if ($request->type == 'suggested') {
            $user = User::with('site_user')->where('id', $request->user_id)->first();
            $sites = [];
            foreach ($user->site_user as $key => $su) {
                $sites[] = $su->id;
            }
            $connected = Connection::where('user_id', $user->id)->get();
            $connected_user = [];
            foreach ($connected as $key => $c) {
                $connected_user[] = $c->connection_id;
            }
            $lists = User::whereNotIn('id', [$request->user_id])->whereNotIn('id', $connected_user)->whereHas('site_user', function ($q) use ($sites) {
                $q->whereIn('site_id', [$sites]);
            })->get();
        } else {
            $query = Connection::latest()->with('user', 'connection');
            if ($request->type == 'following') {
                $query->where('user_id', $request->user_id)->where('status', 'Followed');
            }
            if ($request->type == 'follower') {
                $query->where('connection_id', $request->user_id)->where('status', 'Followed');
            }
            $lists = $query->get();
            $requests = Connection::where('status', 'requested')->where('user_id', $auth_user->id)->pluck('connection_id');
            $followers = Connection::where('status', 'followed')->where('connection_id', $auth_user->id)->pluck('user_id');
            $followings = Connection::where('status', 'followed')->where('user_id', $auth_user->id)->pluck('connection_id');
            // dd($followings);
            foreach ($lists as $list) {
                $auth_user_request = false;
                foreach ($requests as $key => $r) {

                    if ($request->type == 'follower') {
                        if ($list->user_id == $r) {
                            $auth_user_request = true;
                        }
                    } else {

                        if ($list->connection_id == $r) {
                            $auth_user_request = true;
                        }
                    }
                }
                $list->auth_user_request = $auth_user_request;

                $auth_user_follower = false;
                foreach ($followers as $key => $f) {
                    if ($request->type == 'follower') {
                        if ($list->user_id == $f) {
                            $auth_user_follower = true;
                        }
                    } else {

                        if ($list->connection_id == $f) {
                            $auth_user_follower = true;
                        }
                    }
                }
                $list->auth_user_follower = $auth_user_follower;

                $auth_user_following = false;
                foreach ($followings as $key => $f) {
                    if ($list->connection_id == $f) {
                        $auth_user_following = true;
                    }
                    if ($request->type == 'follower') {
                        if ($list->user_id == $f) {
                            $auth_user_following = true;
                        }
                    } else {

                        if ($list->connection_id == $f) {
                            $auth_user_following = true;
                        }
                    }
                }
                $list->auth_user_following = $auth_user_following;
            }
        }

        if ($lists->isEmpty()) {
            $re = [
                'status'    => false,
                'message'   => 'No record(s) found.'
            ];
        } else {
            $re = [
                'status'    => true,
                'message'   => $lists->count() . " records found.",
                'data'      => $lists
            ];
        }

        return response()->json($re);
    }
}
