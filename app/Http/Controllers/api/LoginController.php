<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\UserDevice;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'user_name'      => 'required',
            'password'      => 'required',
        ]);

        // Check if user name exists or not
        $user = User::where('user_name', request('user_name'))->first();
        if (!empty($user->id)) {
            if ($user->is_active == 'true') {
                if ($user->is_mobile == 'true') {
                    $credentials = $request->only('user_name', 'password');
                    $remember    = !empty($request->remember) ? true : false;

                    if (Auth::attempt($credentials, $remember)) {
                        $user = Auth::user();
                        if (UserDevice::where('user_id', $user->id)->exists()) {

                            $userlogin = UserDevice::where('user_id', $user->id)->first();
                            $input = [
                                'device_type'   => request('device_type'),
                                'device_id'     => request('device_id'),
                                'fcm_id'        => request('fcm_id')
                            ];

                            $userlogin->fill($input)->save();
                        }

                        $token = $user->createToken('samaj-app')->accessToken;
                        if ($user->is_email == 'true') {
                            $re = [
                                'status'    => true,
                                'email_verify' => true,
                                'message'   => 'Success!! Login successfully.',
                                'data'      => $user,
                                'token'     => $token,
                            ];
                        } else {
                            $re = [
                                'status'    => true,
                                'email_verify' => false,
                                'message'   => 'Success!! Login successfully.',
                                'data'      => $user,
                                'token'     => $token,
                            ];
                        }
                    } else {
                        $re = [
                            'status'    => false,
                            'message'   => 'Error!! Credentials not matched.',
                        ];
                    }
                } else {
                    $otp        = rand(100000, 999999);
                    // Send SMS
                    // $msg    = urlencode("Dear ...... user, " . $otp . " is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE. Regards .......");
                    // $apiUrl = str_replace(["[MESSAGE]", "[MOBILE]"], [$msg, $user->mobile], $setting->sms_api);
                    // $sms    = file_get_contents($apiUrl);

                    $user->otp = $otp;
                    $user->save();

                    $re = [
                        'status'    => false,
                        'message'   => 'Error!! Mobile number not verified. please verify.',
                        'data'      => $user,
                    ];
                }
            } else {
                $re = [
                    'status'    => false,
                    'message'   => 'Error!! Your account has been Blocked.',
                ];
            }
        } else {
            $re = [
                'status'    => false,
                'message'   => 'Error!! Credentials not matched.',
            ];
        }
        return response()->json($re);
    }

    public function sendotp(Request $request)
    {
        $request->validate([
            'user_name'      => 'required',
        ]);

        // Check if user name exists or not
        $user = User::where('user_name', request('user_name'))->first();
        if (!empty($user->id)) {
            $otp        = rand(100000, 999999);
            // Send SMS
            // $msg    = urlencode("Dear ...... user, " . $otp . " is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE. Regards .......");
            // $apiUrl = str_replace(["[MESSAGE]", "[MOBILE]"], [$msg, $user->mobile], $setting->sms_api);
            // $sms    = file_get_contents($apiUrl);

            $user->otp = $otp;
            $user->save();

            $re = [
                'status'    => true,
                'message'   => 'Success!! Otp send successfully.',
                'data'      => $user,
            ];
        } else {
            $re = [
                'status'    => false,
                'message'   => 'Error!! User name not exists.',
            ];
        }
        return response()->json($re);
    }

    public function verifyotp(Request $request)
    {
        $request->validate([
            'user_name'     => 'required',
            'otp'      => 'required',
        ]);

        // Check if user name exists or not
        $user = User::where('user_name', request('user_name'))->first();
        if (!empty($user->id)) {
            if ($user->otp == $request->otp) {
                $user->is_mobile = true;
                $user->save();

                $re = [
                    'status'    => true,
                    'message'   => 'Success!! Mobile otp verify successfully.',
                    'data'      => $user,
                ];
            } else {
                $re = [
                    'status'    => false,
                    'message'   => 'Error!! Otp not match.',
                    'data'      => $user,
                ];
            }
        } else {
            $re = [
                'status'    => false,
                'message'   => 'Error!! User name not exists.',
            ];
        }
        return response()->json($re);
    }

    public function logout(Request $request)
    {
        $user = auth()->user();
        $userlogin = UserDevice::where('user_id', $user->id)->first();
        $userlogin->device_id = '';
        $userlogin->fcm_id = '';
        $userlogin->save();
        $re = [
            'status'    => true,
            'message'   => 'Success! You are logout successfully.',
        ];

        return response()->json($re);
    }

    public function forgotpassword(Request $request)
    {
        $request->validate([
            'user_name'        => 'required',
            'new_password'     => 'required|string|min:6|same:new_password',
            'confirm_password' => 'required|string|min:6|same:new_password'
        ]);

        // Check if user name exists or not
        $user = User::where('user_name', request('user_name'))->first();
        if (!empty($user->id)) {
            $new_password = Hash::make($request->new_password);
            $user = $user->fill(['password' => $new_password])->save();

            $re = [
                'status'    => true,
                'message'   => 'Success! Password has been updated.',
            ];
        } else {
            $re = [
                'status'    => false,
                'message'   => 'Error!! User name not exists.',
            ];
        }

        return response()->json($re);
    }

    public function changepassword(Request $request)
    {
        $request->validate([
            'user_name'        => 'required',
            'old_password'     => 'required|string|min:6',
            'new_password'     => 'required|string|min:6|same:new_password',
            'confirm_password' => 'required|string|min:6|same:new_password'
        ]);

        // Check if user name exists or not
        $user = User::where('user_name', request('user_name'))->first();
        if (!empty($user->id)) {
            if (Hash::check(request('old_password'), $user->password)) {
                $new_password = Hash::make($request->new_password);
                $user = $user->fill(['password' => $new_password])->save();

                $re = [
                    'status'    => true,
                    'message'   => 'Success! Password has been updated.',
                ];
            } else {
                $re = [
                    'status'    => false,
                    'message'   => 'Error!! Old password not match.',
                ];
            }
        } else {
            $re = [
                'status'    => false,
                'message'   => 'Error!! User name not exists.',
            ];
        }

        return response()->json($re);
    }
}
