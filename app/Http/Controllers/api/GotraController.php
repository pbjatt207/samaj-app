<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Gotra;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class GotraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query =  Gotra::query();

        if ($request->cast_id) {
            $query->where('cast_id', $request->cast_id);
        }

        // if ($request->type == 'all') {
        //     $gotras = $query->pluck('name', 'id');
        // } else {
        //     $gotras = $query->pluck('name', 'id');
        // }
        
        if ($request->type == 'all') {
            $gotras = $query->select('name', 'id')->get();
        } else {
            $gotras = $query->select('name', 'id')->get();
        }

        return response()->json($gotras);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $query = Gotra::latest();


        return DataTables::of($query)
            ->addIndexColumn()
            ->addColumn('action', function ($gotra_id) {
                $action = "
                <a href='" . route('admin.gotra.edit', $gotra_id) . "' class='btn btn-sm btn-info action-btn' data-toggle='tooltip' title='Edit'><i class='icon-pencil1'></i></a>
                <a href='" . route('admin.gotra.destroy', $gotra_id) . "' class='btn btn-sm btn-danger action-btn delete-btn' data-toggle='tooltip' title='Remove'><i class='icon-delete'></i></a>
            ";
                return $action;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\gotra  $gotra
     * @return \Illuminate\Http\Response
     */
    public function show(gotra $gotra)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\gotra  $gotra
     * @return \Illuminate\Http\Response
     */
    public function edit(gotra $gotra)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\gotra  $gotra
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, gotra $gotra)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\gotra  $gotra
     * @return \Illuminate\Http\Response
     */
    public function destroy(gotra $gotra)
    {
        //
    }
}
