<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\Event;
use App\Model\LikeEvent;
use App\Model\Notification;
use App\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class EventController extends Controller
{
    protected function upload_image($request, $item)
    {
        if ($request->hasFile('image')) {
            // dd('sdkjdfj');

            // if (file_exists($item->logo)) {
            // unlink(storage_path('app/public/' . $item->image));
            // }

            $image = $request->image;
            $filename = 'IMAGE_' . sprintf('%06d', $item->id) . '.' . $image->getClientOriginalExtension();

            $image->storeAs('public/event/', $filename);
            $item->image = 'event/' . $filename;

            $item->image .= '?v=' . time();
            $item->save();
        }
    }

    public function index(Request $request)
    {
        $user_id = Auth::guard('api')->user()->id;
        $validator = Validator::make($request->all(), [
            'user_id'  => '',
            'type'     => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $query = Event::with('user', 'like_event')->withCount('like_event')->latest();
            if ($request->user_id) {
                $query->where('user_id', $request->user_id);
            }
            if ($request->type) {
                $query->where('type', $request->type);
            }
            $lists = $query->get();
            foreach ($lists as $key => $list) {
                $auth_event_like = false;
                foreach ($list->like_event as $key => $li) {
                    if ($li->id == $user_id) {
                        $auth_event_like = true;
                    }
                }
                $list->auth_event_like = $auth_event_like;
            }

            if ($lists->isEmpty()) {
                $re = [
                    'status' => false,
                    'message'    => 'No record(s) found.'
                ];
            } else {
                $re = [
                    'status' => true,
                    'message'    => $lists->count() . " records found.",
                    'data'   => $lists
                ];
            }
        }
        return response()->json($re);
    }

    public function list()
    {
        $query = Event::latest();

        return DataTables::of($query)
            ->addIndexColumn()
            ->addColumn('image', function ($post) {
                $action = "<img src='" . url('storage/') . '/' . $post->image . "' width='50px' />";
                return $action;
            })
            ->addColumn('date', function ($post) {
                if ($post->date_type == 'single') {
                    $action = $post->start_date;
                }
                if ($post->date_type == 'range') {
                    $action = $post->start_date . ' to ' . $post->end_date;
                }
                return $action;
            })
            ->addColumn('status', function ($event) {
                if ($event->status == 'true') {
                    $action = "<a href='" . route('admin.event.status', $event->id) . "' class='btn btn-sm btn-info action-btn' data-toggle='tooltip' title='Status'><i class='icon-eye-blocked'></i> Hide</a>";
                } else {
                    $action = "<a href='" . route('admin.event.status', $event->id) . "' class='btn btn-sm btn-info action-btn' data-toggle='tooltip' title='Status'><i class='icon-eye'></i> Show</a>";
                }
                return $action;
            })
            ->rawColumns(['date', 'image', 'status'])
            // ->addColumn('action', function ($post_id) {
            //     $action = "
            //     <a href='" . route('admin.post.edit', $post_id) . "' class='btn btn-sm btn-info action-btn' data-toggle='tooltip' title='Edit'><i class='icon-pencil1'></i></a>
            //     <a href='" . route('admin.post.destroy', $post_id) . "' class='btn btn-sm btn-danger action-btn delete-btn' data-toggle='tooltip' title='Remove'><i class='icon-delete'></i></a>
            // ";
            //     return $action;
            // })
            // ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            // 'start_date'  => 'required|date',    
            'user_id'  => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->except(['_token', 'image']);
            $event = new Event($input);
            $event->save();

            $this->upload_image($request, $event);

            $admin = Admin::find($request->user_id);
            $users = User::whereHas('site_user', function ($q) use ($admin) {
                $q->whereIn('site_id', [$admin->site_id]);
            })->pluck('id');
            foreach ($users as $key => $user) {
                $obj = [
                    'receiver_id' => $user,
                    'type' => $request->type,
                    'event_id' => $event->id
                ];
                $notification = new Notification($obj);
                $notification->save();
            }

            $re = [
                'status'    => true,
                'message'   => "Event created successfully.",
                'data'      => $event
            ];
        }
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        $user_id = Auth::guard('api')->user()->id;
        $list = Event::with('user', 'like_event')->withCount('like_event')->findOrFail($event->id);
        $auth_event_like = false;
        foreach ($list->like_event as $key => $li) {
            if ($li->id == $user_id) {
                $auth_event_like = true;
            }
        }
        $list->auth_event_like = $auth_event_like;

        $latests = Event::with('user', 'like_event')->withCount('like_event')->where('type', $event->type)->latest()->whereNotIn('id', [$event->id])->take(5)->get();
        foreach ($latests as $key => $latest) {
            $auth_event_like = false;
            foreach ($latest->like_event as $key => $li) {
                if ($li->id == $user_id) {
                    $auth_event_like = true;
                }
            }
            $latest->auth_event_like = $auth_event_like;
        }

        $list->latest = $latests;
        $re = [
            'status' => true,
            'data'   => $list
        ];
        return response()->json($re);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'start_date'  => 'required|date',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->except(['_token', 'image', 'user_id']);

            $event->fill($input);
            $event->save();

            $this->upload_image($request, $event);

            $re = [
                'status'    => true,
                'message'   => "Event created successfully.",
                'data'      => $event
            ];
        }
        return response()->json($re);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        Notification::where('type', $event->type)->where('event_id', $event->id)->delete();
        $event->delete();
        $re = [
            'status' => true,
            'message'    => "Event deleted successfully.",
        ];
        return response()->json($re);
    }


    public function reaction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'event_id'       => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $query = LikeEvent::latest();
            if ($request->event_id) {
                $query->where('event_id', $request->event_id);
            }
            $lists = $query->get();

            if ($lists->isEmpty()) {
                $re = [
                    'status' => false,
                    'message'    => 'No record(s) found.'
                ];
            } else {
                $re = [
                    'status' => true,
                    'message'    => $lists->count() . " records found.",
                    'data'   => $lists
                ];
            }
        }

        return response()->json($re);
    }

    public function reactionadd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'       => 'required',
            'event_id'       => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->except(['_token']);

            if (LikeEvent::where('user_id', $request->user_id)->where('event_id', $request->event_id)->exists()) {
                LikeEvent::where('user_id', $request->user_id)->where('event_id', $request->event_id)->delete();
            } else {
                $likeevent = new LikeEvent($input);
                $likeevent->save();
            }

            $re = [
                'status'    => true,
                'message'   => "Reaction changed successfully.",
            ];
        }
        return response()->json($re);
    }
}
