<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\CommentPost;
use App\Model\Connection;
use App\Model\LikeComment;
use App\Model\Notification;
use App\Model\Post;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'post_id'      => 'required',
        ]);
        $query = CommentPost::latest()->with('like_comment');
        if ($request->post_id) {
            $query->where('post_id', $request->post_id);
        }
        if ($request->comment_id) {
            $query->where('comment_id', $request->comment_id);
        }
        $lists = $query->get();

        if ($lists->isEmpty()) {
            $re = [
                'status' => false,
                'message'    => 'No record(s) found.'
            ];
        } else {
            $re = [
                'status' => true,
                'message'    => $lists->count() . " records found.",
                'data'   => $lists
            ];
        }

        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'comment'      => 'required',
            'user_id'      => 'required',
            'post_id'      => 'required',
        ]);

        $input = $request->except(['_token']);
        $comment = new CommentPost($input);
        $comment->save();

        $post = Post::find($request->post_id);
        if ($request->comment_id) {
            if ($request->user_id != $post->user_id) {
                $obj = [
                    'sender_id' => $request->user_id,
                    'receiver_id' => $post->user_id,
                    'type' => 'reply',
                    'post_id' => $request->post_id,
                    'comment_post_id' => $comment->id,
                ];
                $notification = new Notification($obj);
                $notification->save();
            }
        } else {
            if ($request->user_id != $post->user_id) {
                $obj = [
                    'sender_id' => $request->user_id,
                    'receiver_id' => $post->user_id,
                    'type' => 'comment',
                    'post_id' => $request->post_id,
                    'comment_post_id' => $comment->id,
                ];
                $notification = new Notification($obj);
                $notification->save();
            }
        }

        $re = [
            'status'    => true,
            'message'   => "Comment created successfully.",
            'data'      => $comment
        ];
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\CommentPost  $commentPost
     * @return \Illuminate\Http\Response
     */
    public function show($comment)
    {
        $list = CommentPost::findOrFail($comment);

        $re = [
            'status' => true,
            'data'   => $list
        ];
        return response()->json($re);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\CommentPost  $commentPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $comment)
    {
        $request->validate([
            'comment'      => 'required',
        ]);

        $input = $request->except(['_token', 'user_id', 'post_id']);
        $postcomment = CommentPost::findOrFail($comment);
        $postcomment->fill($input);
        $postcomment->save();

        $re = [
            'status'    => true,
            'message'   => "Comment updated successfully.",
            'data'      => $postcomment
        ];
        return response()->json($re);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\CommentPost  $commentPost
     * @return \Illuminate\Http\Response
     */
    public function destroy($comment)
    {
        $postcomment = CommentPost::findOrFail($comment);
        if ($postcomment->comment_id) {
            Notification::where('type', 'reply')->where('post_id', $postcomment->post_id)->where('comment_post_id', $postcomment->id)->delete();
        } else {
            Notification::where('type', 'comment')->where('post_id', $postcomment->post_id)->where('comment_post_id', $postcomment->id)->delete();
        }
        $postcomment->delete();
        $re = [
            'status' => true,
            'message'    => "Comment deleted successfully.",
        ];
        return response()->json($re);
    }


    public function reaction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment_post_id'       => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $query = LikeComment::latest();
            if ($request->comment_post_id) {
                $query->where('comment_post_id', $request->comment_post_id);
            }
            $lists = $query->get();

            $auth_user = Auth::guard('api')->user();

            $requests = Connection::where('status', 'requested')->where('user_id', $auth_user->id)->pluck('connection_id');
            $followers = Connection::where('status', 'followed')->where('connection_id', $auth_user->id)->pluck('user_id');
            $followings = Connection::where('status', 'followed')->where('user_id', $auth_user->id)->pluck('connection_id');

            foreach ($lists as $key => $list) {
                $auth_user_request = false;
                foreach ($requests as $key => $r) {
                    if ($list->user_id == $r) {
                        $auth_user_request = true;
                    }
                }
                $list->auth_user_request = $auth_user_request;

                $auth_user_follower = false;
                foreach ($followers as $key => $f) {
                    if ($list->user_id == $f) {
                        $auth_user_follower = true;
                    }
                }
                $list->auth_user_follower = $auth_user_follower;

                $auth_user_following = false;
                foreach ($followings as $key => $f) {
                    if ($list->user_id == $f) {
                        $auth_user_following = true;
                    }
                }
                $list->auth_user_following = $auth_user_following;
            }

            if ($lists->isEmpty()) {
                $re = [
                    'status' => false,
                    'message'    => 'No record(s) found.'
                ];
            } else {
                $re = [
                    'status' => true,
                    'message'    => $lists->count() . " records found.",
                    'data'   => $lists
                ];
            }
        }

        return response()->json($re);
    }

    public function reactionadd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'       => 'required',
            'comment_post_id'       => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->except(['_token']);

            if (LikeComment::where('user_id', $request->user_id)->where('comment_post_id', $request->comment_post_id)->exists()) {
                $commentlike = LikeComment::where('user_id', $request->user_id)->where('comment_post_id', $request->comment_post_id)->first();
                Notification::where('type', 'like')->where('comment_post_id', $request->comment_post_id)->where('like_comment_id', $commentlike->id)->delete();
                $commentlike->delete();
            } else {
                $likecomment = new LikeComment($input);
                $likecomment->save();
                $postcomment = CommentPost::find($request->comment_post_id);
                if ($request->user_id != $postcomment->user_id) {
                    $obj = [
                        'sender_id' => $request->user_id,
                        'receiver_id' => $postcomment->user_id,
                        'type' => 'like',
                        'comment_post_id' => $request->comment_post_id,
                        'like_comment_id' => $likecomment->id,
                    ];
                    $notification = new Notification($obj);
                    $notification->save();
                }
            }

            $re = [
                'status'    => true,
                'message'   => "Reaction changed successfully.",
            ];
        }
        return response()->json($re);
    }
}
