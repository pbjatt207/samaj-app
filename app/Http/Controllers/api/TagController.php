<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Tag;
use Validator;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Tag::latest();
        if ($request->s) {
            $query->where('s', 'LIKE', `%$request->s%`);
        }
        $lists = $query->get();

        if ($lists->isEmpty()) {
            $re = [
                'status' => false,
                'message'    => 'No record(s) found.'
            ];
        } else {
            $re = [
                'status' => true,
                'message'    => $lists->count() . " records found.",
                'data'   => $lists
            ];
        }

        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */



    public function list()
    {
        $query = Tag::latest();

        return DataTables::of($query)
            ->addIndexColumn()
            // ->addColumn('image', function ($post) {
            //     $action = "<img src='" . url('storage/') . '/' . $post->image . "' width='50px' />";
            //     return $action;
            // })
            // ->rawColumns(['date', 'image', 'status'])
            ->make(true);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required|regex:/^#\w+$/|unique:tags',
        ]);

        $input = $request->except(['_token']);
        $prayer = new Tag($input);
        // dd($prayer);
        $prayer->save();

        $re = [
            'status'    => true,
            'message'   => "Tag created successfully.",
            'data'      => $prayer
        ];
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        $list = $tag;
        $re = [
            'status' => true,
            'data'   => $list
        ];
        return response()->json($re);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        $request->validate([
            'name'      => 'required|regex:/^#\w+$/|unique:tags,name,' . $tag->id,
        ]);

        $input = $request->except(['_token', '_method']);
        $tag->fill($input);
        $tag->save();

        $re = [
            'status'    => true,
            'message'   => "Tag updated successfully.",
            'data'      => $tag
        ];

        return response()->json($re);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();
        $re = [
            'status' => true,
            'message'    => "Tag deleted successfully.",
        ];
        return response()->json($re);
    }
}
