<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $guarded = [];
    protected $with = ['post', 'sender', 'receiver', 'comment_post'];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function sender()
    {
        return $this->hasOne('App\User', 'id', 'sender_id');
    }
    public function receiver()
    {
        return $this->hasOne('App\User', 'id', 'receiver_id');
    }

    public function comment_post()
    {
        return $this->belongsTo(CommentPost::class);
    }
}
