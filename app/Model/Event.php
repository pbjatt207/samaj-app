<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = [];
    protected $with = ['city'];

    public function user()
    {
        return $this->belongsTo(Admin::class);
    }
    public function like_event()
    {
        return $this->belongsToMany(User::class, LikeEvent::class);
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
