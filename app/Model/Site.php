<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $guarded = [];
    protected $with = ['religions', 'casts'];
    public function religions()
    {
        return $this->belongsToMany(Religion::class);
    }

    public function casts()
    {
        return $this->belongsToMany(Cast::class);
    }
}
