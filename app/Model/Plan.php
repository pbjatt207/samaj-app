<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    
    protected $table 		= "plans";

	public function cat(){
		return $this->hasOne('App\Model\Category', 'id', 'parent');
	}

    
}
