<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [];
    protected $with = ['user'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
    public function tag_post()
    {
        return $this->belongsToMany(Tag::class, TagPost::class);
    }
    public function mention_post()
    {
        return $this->belongsToMany(User::class, MentionPost::class);
    }
    public function comment()
    {
        return $this->hasMany(CommentPost::class);
    }
    public function like_post()
    {
        return $this->belongsToMany(User::class, LikePost::class);
    }
    public function view_post()
    {
        return $this->belongsToMany(User::class, ViewPost::class);
    }
    public function report_post()
    {
        return $this->hasMany('App\Model\ReportPost', 'post_id', 'id');
    }
}
