<?php

namespace App\model;

use App\Model\Country;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $guarded = [];
    protected $with = ['country'];
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
