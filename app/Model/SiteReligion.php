<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SiteReligion extends Model
{
    protected $guarded = [];
    protected $table   = "religion_site";
}
