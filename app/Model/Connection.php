<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Connection extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function connection()
    {
        return $this->hasOne('App\User', 'id', 'connection_id');
    }
}
