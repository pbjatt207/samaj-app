<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{


    protected $table        = "category";
    const CREATED_AT        = 'created_at';
    const UPDATED_AT        = 'updated_at';


    public function products()
    {
        return $this->hasMany('App\Model\Product', 'category_id', 'id');
    }
    public function blogs()
    {
        return $this->hasMany('App\Model\Blog', 'parent', 'id');
    }
    public function faq()
    {
        return $this->hasMany('App\Model\Faq', 'cid', 'id');
    }
}
