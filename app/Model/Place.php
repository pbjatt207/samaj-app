<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
