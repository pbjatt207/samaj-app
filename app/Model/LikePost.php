<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LikePost extends Model
{
    protected $guarded = [];
    protected $with = ['user'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
