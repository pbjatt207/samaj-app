<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SiteCast extends Model
{
    protected $guarded = [];

    protected $table   = "cast_site";
}
