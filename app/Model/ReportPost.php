<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReportPost extends Model
{
    protected $guarded = [];
    protected $with = ['user', 'post'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
