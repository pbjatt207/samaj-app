<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class CommentPost extends Model
{
    protected $guarded = [];
    protected $with = ['reply', 'user', 'like_comment'];

    public function reply()
    {
        return $this->hasMany('App\Model\CommentPost', 'comment_id', 'id');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function like_comment()
    {
        return $this->belongsToMany(User::class, LikeComment::class);
    }
}
