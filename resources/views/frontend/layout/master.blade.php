<?php
$setting = App\Model\Setting::findOrFail(1);
$service = App\Model\Product::where('is_front', 'Y')->latest()->paginate(15);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>{{ $setting->site_title }}</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ url('imgs/'.$setting->favicon) }}" rel="icon">
  <link href="{{ url('imgs/'.$setting->favicon) }}" rel="apple-touch-icon">
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->

  {{Html::style('vendor/bootstrap/css/bootstrap.min.css')}}
  {{Html::style('vendor/icofont/icofont.min.css')}}
  {{Html::style('vendor/boxicons/css/boxicons.min.css')}}
  {{Html::style('vendor/animate.css/animate.min.css')}}
  {{Html::style('vendor/remixicon/remixicon.css')}}
  {{Html::style('vendor/owl.carousel/assets/owl.carousel.min.css')}}
  {{Html::style('vendor/venobox/venobox.css')}}
  <!--{{Html::style('vendor/aos/aos.css')}}-->
  {{Html::style('css/style.css')}}
  {{Html::style('css/bootstrap-pincode-input.css')}}

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>

<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top topbar-inner-pages">
    <div class="container d-flex align-items-center">
      <div class="contact-info mr-auto">
        <ul>
          <li><i class="icofont-envelope"></i><a href="mailto:{{ $setting->email }}">{{ $setting->email }}</a></li>
          <li><i class="icofont-phone"></i><a href="tel:{{ $setting->mobile }}">{{ $setting->mobile }}</a></li>
          <!-- <li><i class="icofont-clock-time icofont-flip-horizontal"></i> Mon-Fri 9am - 5pm</li> -->
        </ul>
      </div>
      <div class="cta mr-2">
        <!--<a href="{{ url('/apply-form-stage-1') }}" class="scrollto">E-mitra Apply</a>-->
      </div>
      <div class="cta">
        <a href="{{ url('/agent-panel') }}" class="scrollto">Login</a>
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="{{ url('/') }}" class="scrollto">

          @if(!empty($setting->logo))
          <img src="{{ url('imgs/'.$setting->logo) }}" alt="logo">
          @else
          {{ $setting->site_title }}
          @endif
        </a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="#header" class="logo mr-auto scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="{{ url('/')}}">Home</a></li>
          <li><a href="{{ url('/about')}}">About</a></li>
          <li class="drop-down"><a href="#">Services</a>
            <ul>
              @foreach( $service as $item )
              <li><a href="{{ url('service/'.$item->slug) }}">{{ $item->title }}</a></li>
              @endforeach
            </ul>
          </li>
          <!--<li><a href="{{ url('/blog')}}">Blog</a></li>-->
          <!--<li><a href="{{ url('/ourteam')}}">Team</a></li>-->

          <li><a href="{{ url('/contact') }}">Contact</a></li>
          <li class="d-xs-block d-sm-block d-md-block d-lg-none d-xl-none"><a href="{{ url('/contact') }}">Login</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->
  @show


  @yield('contant')



  @section('footer')

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-5 col-md-6 footer-info">
            <h3>{{ $setting->site_title }}</h3>
            <p>{{ $setting->about }}</p>
            <div class="social-links mt-3">
              @if($setting->facebook_url!='')
              <a href="{{ $setting->facebook_url }}" class="facebook" target="_blank"><i class="bx bxl-facebook"></i></a>
              @else
              @endif
              @if($setting->insta_url!='')
              <a href="{{ $setting->insta_url }}" class="instagram" target="_blank"><i class="bx bxl-instagram"></i></a>
              @else
              @endif
              @if($setting->youtube_url!='')
              <a href="{{ $setting->youtube_url }}" class="google-plus" target="_blank"><i class="bx bxl-youtube"></i></a>
              @else
              @endif
              @if($setting->twitter_url!='')
              <a href="{{ $setting->twitter_url }}" class="google-plus" target="_blank"><i class="bx bxl-twitter"></i></a>
              @else
              @endif
              @if($setting->pinterest_url!='')
              <a href="{{ $setting->pinterest_url }}" class="google-plus" target="_blank"><i class="bx bxl-pinterest"></i></a>
              @else
              @endif
            </div>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="{{ url('/') }}">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="{{ url('/about') }}">About us</a></li>
              <!-- <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li> -->
              <!--<li><i class="bx bx-chevron-right"></i> <a href="{{ url('/blog')}}">Blog</a></li>-->
              <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div>



          <div class="col-lg-4 col-md-6 footer-contact">
            <h4>Contact Us</h4>
            <p>{{ $setting->address }}<br><br>
              <strong>Phone:</strong> {{ $setting->mobile }}<br>
              <strong>Email:</strong><a href="mailto:{{ $setting->email }}"> {{ $setting->email }}</a><br>
            </p>

          </div>



        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="copyright">
            &copy; Copyright <strong><span>Suncity</span></strong>. All Rights Reserved
          </div>
        </div>
        <div class="col-sm-6">
          <div class="copyright">
            Designed by <a href="https://suncitygroup.org/" target="_blank">Suncitygroup.org</a>
          </div>
        </div>
      </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <!-- <div id="preloader"></div> -->

  <!-- Vendor JS Files -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="vendor/php-email-form/validate.js"></script>
  <script src="js/validation.js"></script>
  <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="vendor/venobox/venobox.min.js"></script>
  <script src="vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="vendor/aos/aos.js"></script>
  <script src="js/bootstrap-pincode-input.js"></script>
  <script src="js/ajax.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $("#district").attr('checked', false);
      if (!$("#district").attr('checked')) {
        $(".e_dist").attr('disabled', "true");
      }
      $("#district").click(function() {
        $(".e_dist").attr('disabled', !this.checked);
      });

      $("#tehsil").attr('checked', false);
      if (!$("#tehsil").attr('checked')) {
        $(".e_teh").attr('disabled', "true");
      }
      $("#tehsil").click(function() {
        $(".e_teh").attr('disabled', !this.checked);
      });

      $("#panchayat").attr('checked', false);
      if (!$("#panchayat").attr('checked')) {
        $(".e_panchayat").attr('disabled', "true");
      }
      $("#panchayat").click(function() {
        $(".e_panchayat").attr('disabled', !this.checked);
      });

      $("#gram").attr('checked', false);
      if (!$("#gram").attr('checked')) {
        $(".e_gram").attr('disabled', "true");
      }
      $("#gram").click(function() {
        $(".e_gram").attr('disabled', !this.checked);
      });

      $("#village").attr('checked', false);
      if (!$("#village").attr('checked')) {
        $(".e_village").attr('disabled', "true");
      }
      $("#village").click(function() {
        $(".e_village").attr('disabled', !this.checked);
      });

      if (document.getElementById("testName").checked) {
        document.getElementById('testNameHidden').enable = true;
      } else {
        document.getElementById('testName').disabled = false;
      }
    });
  </script>
  <script>
    jQuery(document).ready(function($) {
      $('.hero').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: !0,
        cssEase: 'linear',
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 8000,
        draggable: false,
        arrows: false,
        responsive: [{
            breakpoint: 1024,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: true
            }
          },
          {
            breakpoint: 768,
            settings: {
              draggable: true,
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              draggable: true,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              draggable: true,
              slidesToScroll: 1
            }
          }

        ]
      });
    });
  </script>
  <script type="text/javascript">
    $('.pincode-input2').pincodeInput({
      hidedigits: false,
      inputs: 6,
      complete: function(value, e, errorElement) {


        // $(errorElement).html("I'm sorry, but the code not correct");
      }
    });
  </script>
  <script type="text/javascript">
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#lb').addClass('d-none');
          $('#profile-img-tag').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    $("#profile-img").change(function() {
      readURL(this);
    });

    $(':radio').change(function(event) {
      var id = $(this).data('id');
      $('#' + id).addClass('none').siblings().removeClass('none');
    });
  </script>
  <script type="text/javascript">
    function printForm() {
      let content = $('body').html();

      $('body').html($('#print_area').html());

      window.print();

      $('body').html(content);
    }
  </script>
  {{ Html::script('js/ajax.js') }}
</body>

</html>