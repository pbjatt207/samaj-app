@extends('frontend.layout.master')

@section('title','Homepage')

@section('contant')
<main id="main">
	<section id="breadcrumbs" class="breadcrumbs">
		<!-- MultiStep Form -->
		
		                <!-- <h2><strong>Sign Up Your User Account</strong></h2>
		                <p>Fill all form field to go to next step</p> -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 mx-0 py-5">
                	<ul id="progressbar" class="text-center">
                        <li class="active" id="account"><strong class="mt-4">Basic Detail</strong></li>
                        <li class="active" id="document"><strong class="mt-4">Document</strong></li>
                        <li id="account"><strong class="mt-4">Bank Deatil</strong></li>
                        <li id="confirm"><strong class="mt-4">Finish</strong></li>                       
                        
                    </ul>
                    <h2 class="text-center mb-5"><strong>Apply Form</strong></h2>
                    
                    	{{ Form::open(['method' => 'POST', 'files' => true, 'class' => 'applyform', 'id' => 'msform']) }}
                        <fieldset>
                            
                                <div class="row">
									<div class="col-12">
										<h6 class="font-weight-bold mt-4">Document :-</h6>
										
										<div class="row">
											<div class="col-4">
												<label>
													<span class="float-left">Photo*</span>						
												</label>
												<span class="float-right">-</span>
											</div>
											<div class="col-8 form-group">
												<input type="file" name="kiosk_owner_photo"  id="profile-img" required>
											</div>
										</div>
										<div class="row">
											<div class="col-4">
												<label>
													<span class="float-left">Aadhaar Card*</span>						
												</label>
												<span class="float-right">-</span>
											</div>
											<div class="col-8 form-group">
												<input type="file" name="aadhaar_card_att"  id="profile-img" required>
											</div>
										</div>
										<div class="row">
											<div class="col-4">
												<label>
													<span class="float-left">Pan Card*</span>						
												</label>
												<span class="float-right">-</span>
											</div>
											<div class="col-8 form-group">
												<input type="file" name="pan_card"  id="profile-img" required>
											</div>
										</div>
										<div class="row">
											<div class="col-4">
												<label>
													<span class="float-left">10th Marksheet*</span>						
												</label>
												<span class="float-right">-</span>
											</div>
											<div class="col-8 form-group">
												<input type="file" name="10th_marksheet"  id="profile-img" required>
											</div>
										</div>
										<div class="row">
											<div class="col-4">
												<label>
													<span class="float-left">Bank Account Detail*</span>						
												</label>
												<span class="float-right">-</span>
											</div>
											<div class="col-8 form-group">
												<input type="file" name="bank_acc_detail"  id="profile-img" required>
											</div>
										</div>
										<div class="row">
											<div class="col-4">
												<label>
													<span class="float-left">Police Verification*</span>						
												</label>
												<span class="float-right">-</span>
											</div>
											<div class="col-8 form-group">
												<input type="file" name="police_ver_att"  id="profile-img" required>
											</div>
										</div>
										<div class="row">
											<div class="col-4">
												<label>
													<span class="float-left">Jan Aadhaar Card*</span>						
												</label>
												<span class="float-right">-</span>
											</div>
											<div class="col-8 form-group">
												<input type="file" name="jan_aadhaar_card"  id="profile-img" required>
											</div>
										</div>										
										
								<input type="hidden" name="random_t" value="{{ $request->token }}">
                             <!-- <input type="button" name="previous" class="previous action-button-previous" value="Previous" />  -->
                             <!-- <input type="submit" name="next" class="next action-button" value="Next Step" /> -->
                             <button type="submit" class="next action-button">Next Step</button>
                        </fieldset>
                        {{ Form::close() }}
                </div>
            </div>
        </div>
		            
	</section>
</main>
@stop