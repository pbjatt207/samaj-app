@extends('frontend.layout.master')

@section('title','Homepage')

@section('contant')
<main id="main">
	<section id="breadcrumbs" class="breadcrumbs">




		<!-- MultiStep Form -->
		
		                <!-- <h2><strong>Sign Up Your User Account</strong></h2>
		                <p>Fill all form field to go to next step</p> -->
		                <div class="container">
		                <div class="row">
		                    <div class="col-md-12 mx-0 py-5">
		                    	<ul id="progressbar" class="text-center">
	                                <li class="active" id="account"><strong class="mt-4">Basic Detail</strong></li>
			                        <li class="active" id="document"><strong class="mt-4">Document</strong></li>
			                        <li class="active" id="account"><strong class="mt-4">Bank Deatil</strong></li>
			                        <li id="confirm"><strong class="mt-4">Finish</strong></li>
	                                
	                            </ul>
	                            <h2 class="text-center mb-5"><strong>Apply Form</strong></h2>
		                        
		                        	{{ Form::open(['method' => 'POST', 'files' => true, 'class' => 'applyform', 'id' => 'msform']) }}
		                            <fieldset>
		                                
		                                    <div class="row">
												<div class="col-12">
													<h6 class="font-weight-bold mt-4">Kiosk Bank a/c Detail :-</h6>
													<div class="row">
														<div class="col-4"></div>
														<div class="col-4 text-center">A/C Number</div>
														<div class="col-4 text-center">IFSC</div>
													</div>
													<div class="row">
														<div class="col-4">
															<label>
																<span class="float-left">Bank a/c Number*</span>						
															</label>
															<span class="float-right">-</span>								
														</div>
														<div class="col-8">
															<div class="row">
																<div class="col-6 form-group">
																	<input type="number" name="account_number" required>
																</div>
																<div class="col-6 form-group">
																	<input type="text" name="ifsc" required>
																</div>
															</div>	
														</div>							
													</div>
													<div class="row">
														<div class="col-4">
															<label>
																<span class="float-left">Bank Name*</span>						
															</label>
															<span class="float-right">-</span>
														</div>
														<div class="col-8 form-group">
															<input type="text" name="bank_name" required>
														</div>
													</div>
													<div class="row">
														<div class="col-4">
															<label>
																<span class="float-left">Branch Name*</span>						
															</label>
															<span class="float-right">-</span>
														</div>
														<div class="col-8 form-group">
															<input type="text" name="branch_name" class="" required>								
														</div>
													</div>
													<div class="row">
														<div class="col-4">
															<label>
																<span class="float-left">Account Holder Name*</span>						
															</label>
															<span class="float-right">-</span>
														</div>
														<div class="col-8 form-group">
															<input type="text" name="account_holder_name" class="mb-1" required>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-12">
													<h6 class="font-weight-bold mt-4">If this kiosk had already user ID on e-mitra portal please provide as:-</h6>
													<div class="row">
														<div class="col-4">
															<label>
																<span class="float-left">Kiosk Id*</span>						
															</label>
															<span class="float-right">-</span>
														</div>
														<div class="col-8 form-group">
															<input type="text" name="kiosk_id" required>
														</div>
													</div>
													<div class="row">
														<div class="col-4">
															<label>
																<span class="float-left">LSP Name*</span>						
															</label>
															<span class="float-right">-</span>
														</div>
														<div class="col-8 form-group">
															<input type="text" name="lsp_name" class="" required>								
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-12">
													<div class="mt-4 mb-2">
														<span class="font-weight-bold mt-4">Level</span> (Please define the level of kiosk if the kiosk having more than one level, mention all levels)* :-
													</div>
													<table width="100%" class="level-form-table">
														<tr class="text-center">
															<th></th>
															<th>Level</th>
															<th>Detail (Name)</th>
															<th>Private Location</th>
															<th>IT Center <br><span style="font-weight: normal;font-size: 10px;">(Rajsthan Sampark /<br> Government Building)</span></th>
														</tr>
														<tr>
															<td class="text-center">
																<input type="checkbox" id="district">
															</td>
															<td class="text-left">
																<label class="mb-0">
																	<span class="float-left">District (Head Quarter)*</span>						
																</label>
																<span class="float-right pr-2">-</span>
															</td class="text-center">
															<td>
																<input type="text" name="kl_dist_detail" class="e_dist">
															</td class="text-center">
															<td class="text-center">
																<input type="checkbox" name="kl_dist_pl" value="Y" class="e_dist">
															</td>
															<td class="text-center">
																<input type="checkbox" name="kl_dist_it" value="Y" class="e_dist">
															</td>
														</tr>

														<tr>
															<td class="text-center">
																<input type="checkbox"  id="tehsil">
															</td>
															<td class="text-left">
																<label class="mb-0">
																	<span class="float-left">Tehsil*</span>						
																</label>
																<span class="float-right pr-2">-</span>
															</td class="text-center">
															<td>
																<input type="text" name="kl_tehsil_detail" class="e_teh">
															</td class="text-center">
															<td class="text-center">
																<input type="checkbox" name="kl_tehsil_pl" value="Y" class="e_teh">
															</td>
															<td class="text-center">
																<input type="checkbox" name="kl_tehsil_it" value="Y" class="e_teh">
															</td>
														</tr>
														<tr>
															<td class="text-center">
																<input type="checkbox" id="panchayat">
															</td>
															<td class="text-left">
																<label class="mb-0">
																	<span class="float-left">Panchayat Samiti / Block*</span>						
																</label>
																<span class="float-right pr-2">-</span>
															</td class="text-center">
															<td>
																<input type="text" name="kl_panchayat_detail" class="e_panchayat">
															</td class="text-center">
															<td class="text-center">
																<input type="checkbox" name="kl_panchayat_pl" value="Y" class="e_panchayat">
															</td>
															<td class="text-center">
																<input type="checkbox" name="kl_panchayat_it" value="Y" class="e_panchayat">
															</td>
														</tr>
														<tr>
															<td class="text-center">
																<input type="checkbox" id="gram" >
															</td>
															<td class="text-left">
																<label class="mb-0">
																	<span class="float-left">Gram Panchayat*</span>						
																</label>
																<span class="float-right pr-2">-</span>
															</td class="text-center">
															<td>
																<input  type="text" name="kl_gram_detail" class="e_gram">
															</td class="text-center">
															<td class="text-center">
																<input type="checkbox" name="kl_panchayat_pl" value="Y" class="e_gram">
															</td>
															<td class="text-center">
																<input type="checkbox" name="kl_panchayat_it" value="Y" class="e_gram">
															</td>
														</tr>
														<tr>
															<td class="text-center">
																<input type="checkbox" id="village">
															</td>
															<td class="text-left">
																<label class="mb-0">
																	<span class="float-left">Village*</span>						
																</label>
																<span class="float-right pr-2">-</span>
															</td class="text-center">
															<td>
																<input type="text" name="kl_village_detail" class="e_village">
															</td class="text-center">
															<td class="text-center">
																<input type="checkbox" name="kl_village_pl" value="Y" class="e_village">
															</td>
															<td class="text-center">
																<input type="checkbox" name="kl_village_it" value="Y" class="e_village">
															</td>
														</tr>
													</table>
												</div>
											</div>
											<div class="row">
												<div class="col-12">
													<h6 class="font-weight-bold mt-4">If this kiosk had already user ID on e-mitra portal please provide as:-</h6>
													<div class="row">
														<div class="col-5">
															<div class="row">
																<div class="col-1 mr-0 pr-0">1.</div>
																<div class="col-10 ml-0 pl-0">
																	<label>
																		<span class="float-left">ID Proof / Address Proof*</span>						
																	</label>
																	<span class="float-right">-</span>
																</div>
																<div class="col-1">
																	<input type="checkbox" name="id_proof" value="N">
																</div>
															</div>
														</div>
														
														<div class="col-7"></div>
													</div>
													<div class="row">
														<div class="col-5">
															<div class="row">
																<div class="col-1 mr-0 pr-0">2.</div>
																<div class="col-10 ml-0 pl-0">
																	<label>
																		<span class="float-left">Police Verification Report*</span>						
																	</label>
																	<span class="float-right">-</span>
																</div>
																<div class="col-1">
																	<input type="checkbox" name="police_verification" value="N">
																</div>
															</div>
														</div>
														
														<div class="col-7"></div>
													</div>
													<div class="row">
														<div class="col-5">
															<div class="row">
																<div class="col-1 mr-0 pr-0">3.</div>
																<div class="col-10 ml-0 pl-0">
																	<label>
																		<span class="float-left">Educational Qualification*</span>						
																	</label>
																	<span class="float-right">-</span>
																</div>
																<div class="col-1">
																	<input type="checkbox" name="educational_qualification" value="N">
																</div>
															</div>
														</div>
														
														<div class="col-7"></div>
													</div>
													<div class="row">
														<div class="col-5">
															<div class="row">
																<div class="col-1 mr-0 pr-0">4.</div>
																<div class="col-10 ml-0 pl-0">
																	<label>
																		<span class="float-left">Bank a/c Detail*</span>						
																	</label>
																	<span class="float-right">-</span>
																</div>
																<div class="col-1">
																	<input type="checkbox" name="bank_account_detail" value="N">
																</div>
															</div>
														</div>
														
														<div class="col-7"></div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-12">
													<div class="row">
														<div class="col-2">
															<label>
																<span class="float-left">Longitude</span>						
															</label>
															<!-- <span class="float-right">-</span> -->
														</div>
														<div class="col-4">
															<input type="text" name="longitude">
														</div>
														<div class="col-6"></div>
													</div>
													<div class="row">
														<div class="col-2">
															<label>
																<span class="float-left">Latitude</span>						
															</label>
															<!-- <span class="float-right">-</span> -->
														</div>
														<div class="col-4">
															<input type="text" name="latitude">
														</div>
														<div class="col-6"></div>
													</div>
													<div>Above mention check list are checked are correct in my knowledge.</div>						
												</div>
											</div>
											<div class="row">
												<div class="col-6">
													<label class="py-5 input-file">
														Signature Kiosk Owner
														<input type="file" name="sign_kiosk_owner" class="d-none">

													</label>
												</div>
												<div class="col-6 text-right ">
													<label class="py-5 text-center input-file">
														Signature and Seal<br>
														LSP
														<input type="file" name="sign_lsp" class="d-none">
													</label>
												</div>
											</div>
											<div class="row">
												<div class="col-6">
													Date : 
												</div>
											</div>
											<input type="hidden" name="random_t" value="{{ $request->token }}">
		                                 <!-- <input type="button" name="previous" class="previous action-button-previous" value="Previous" />  -->
		                                 <!-- <input type="submit" name="next" class="next action-button" value="Next Step" /> -->
		                                 <button type="submit" class="next action-button">Next Step</button>
		                            </fieldset>
		                            {{ Form::close() }}
		                    </div>
		                </div>
		            </div>
		            
	</section>
</main>
@stop