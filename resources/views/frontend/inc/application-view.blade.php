<style type="text/css">
	@media print{
		.applyform{
			padding: 15px !important;
			font-size: 10px;
		}
		.wrapper{
			max-width: 960px;
			
		}

		.print-btn{
			display: none;
		}
	}
</style>
<div class="applyform" id="print_area">				
<h2 class="text-center mb-5"><strong>Application Form</strong></h2>
												<div class="row">
													<div class="col-10">
														<h6 class="font-weight-bold">Basic Details :-</h6>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">District</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8">
																{{$form->district}}
															</div>
															<div class="col-4">
																<label>
																	<span class="float-left">LSP</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8">
																{{$form->lsp}}
															</div>
														</div>
													</div>
													<div class="col-2">
														<div class="row">
															<div class="col-12">
																
																<img src="{{ url('imgs/e-mitra_applied/'.$form->kiosk_owner_photo) }}">
																
															</div>
														</div>
													</div>
																		
												</div>
												<div class="row">
													<div class="col-12">
														<h6 class="font-weight-bold">Kiosk Owner Detail :-</h6>
														<div class="row">
															<div class="col-3">
																<label>
																	<span class="float-left">Kiosk Owner Type</span>						
																</label>
																<span class="float-right">-</span>								
															</div>
															<div class="col-9">
																{{$form->kiosk_owner_type ?? 'N/A'}}
																
															</div>							
														</div>
														<div class="row">
															<div class="col-3">
																<label>
																	<span class="float-left">Kiosk Owner Name</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-9">
																{{$form->kiosk_owner_name}}
																
															</div>
														</div>
														<div class="row">
															<div class="col-3">
																<label>
																	<span class="float-left">Permanent Address</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-9">
																
																{{$form->permanent_address}}
															</div>
														</div>
														<div class="row">
															<div class="col-3">
																<label>
																	<span class="float-left">Owner Phone Numbers</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-9">
																{{$form->owner_phone_number}}

															</div>
														</div>
														<div class="row">
															<div class="col-3">
																<label>
																	<span class="float-left">Mobile</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-9">
																{{$form->owner_phone_mobile}}
															</div>
														</div>
														<div class="row">
															<div class="col-3">
																<label>
																	<span class="float-left">Owner E-mail ID</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-9">
																{{$form->owner_email}}
															</div>
														</div>
													</div>
													
												</div>
												<div class="row">
													<div class="col-12">
														<h6 class="font-weight-bold mt-4">Kiosk Owner Detail :-</h6>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Kiosk Center Location</span>						
																</label>
																<span class="float-right">-</span>								
															</div>
															<div class="col-8">
																{{$form->kiosk_enter_location}}
															</div>							
														</div>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Kiosk Center Name (English)</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8">
																{{$form->kiosk_center_name}}
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Kiosk Contact (Hindi)</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8">
																{{$form->kiosk_contact}}								
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Kiosk Contact Person's Name</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8">
																{{$form->kiosk_contact_person}}
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Kiosk Phone Numbers</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8">
																{{$form->kiosk_phone_number}}
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Mobile</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8">
																{{$form->kiosk_phone_mobile}}
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Kiosk E-mail ID</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8">
																{{$form->kiosk_email_id}}
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Police Station (Thana)</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8">
																{{$form->police_station}}
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Kiosk Address</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8">
																{{$form->kiosk_address}}
															</div>
														</div>
														@if($form->kiosk_enter_location == 'urban')
														<div class="row">
															<div class="col-4">
																<label class="">
																	<span class="float-left">Municipal Town</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8">
																{{$form->municipal_town}}
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label class="">
																	<span class="float-left">Ward Number</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8">
																{{$form->ward_number}}
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label class="">
																	<span class="float-left">PIN Code</span>						
																</label>
																<span class="float-right">-</span>
																
															</div>
															<div class="col-8">
																{{$form->pincode}}
																
															</div>
														</div>
														@else
														<div class="row">
															<div class="col-4">
																<label class="">
																	<span class="float-left">Panchayat Samiti</span>						
																</label>
																<span class="float-right">-</span>
																
															</div>
															<div class="col-8">
																{{$form->panchayat_samiti ?? 'N/A'}}
															</div>
														</div>
														<div class="row">
															<div class="col-4 text-left">
																<label class="">
																	<span class="float-left">Gram Panchayat</span>						
																</label>
																<span class="float-right">-</span>
																
															</div>
															<div class="col-8">
																{{$form->gram_panchayat ?? 'N/A'}}
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label class="">
																	<span class="float-left">Village</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8">
																{{$form->village}}
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label class="">
																	<span class="float-left">PIN Code</span>						
																</label>
																<span class="float-right">-</span>
																
															</div>
															<div class="col-8">
																{{$form->pincode}}
																
															</div>
														</div>
														@endif
													<h6 class="font-weight-bold mt-4">Kiosk Bank a/c Detail :-</h6>
													
													<div class="row">
														<div class="col-4">
															<label>
																<span class="float-left">A/C Number</span>						
															</label>
															<span class="float-right">-</span>
														</div>
														<div class="col-8">
															{{$form->account_number}}
														</div>
													</div>
													<div class="row">
														<div class="col-4">
															<label>
																<span class="float-left">IFSC</span>						
															</label>
															<span class="float-right">-</span>
														</div>
														<div class="col-8">
															{{$form->ifsc ?? 'N/A'}}							
														</div>
													</div>
													<div class="row">
														<div class="col-4">
															<label>
																<span class="float-left">Bank Name</span>						
															</label>
															<span class="float-right">-</span>
														</div>
														<div class="col-8">
															{{$form->bank_name ?? 'N/A'}}
														</div>
													</div>
													<div class="row">
														<div class="col-4">
															<label>
																<span class="float-left">Branch Name</span>						
															</label>
															<span class="float-right">-</span>
														</div>
														<div class="col-8">
															{{$form->branch_name ?? 'N/A'}}							
														</div>
													</div>
													<div class="row">
														<div class="col-4">
															<label>
																<span class="float-left">Account Holder Name</span>						
															</label>
															<span class="float-right">-</span>
														</div>
														<div class="col-8">
															{{$form->account_holder_name ?? 'N/A'}}
														</div>
													</div>
												</div>
											</div>
												
											
											<div class="row">
												<div class="col-12">
													<h6 class="font-weight-bold mt-4">If this kiosk had already user ID on e-mitra portal please provide as:-</h6>
													<div class="row">
														<div class="col-4">
															<label>
																<span class="float-left">Kiosk Id</span>						
															</label>
															<span class="float-right">-</span>
														</div>
														<div class="col-8">
															{{$form->kiosk_id ?? 'N/A'}}
														</div>
													</div>
													<div class="row">
														<div class="col-4">
															<label>
																<span class="float-left">LSP Name</span>						
															</label>
															<span class="float-right">-</span>
														</div>
														<div class="col-8">
															{{$form->lsp_name ?? 'N/A'}}								
														</div>
													</div>
												</div>
												
												 
											</div>

											<div class="row">
												<div class="col-12">
													<div class="mt-4 mb-2">
														<span class="font-weight-bold mt-4">Level</span> (Please define the level of kiosk if the kiosk having more than one level, mention all levels) :-
													</div>
													<table width="100%" class="level-form-table">
														<tr class="text-center">
															
															<th>Level</th>
															<th>Detail (Name)</th>
															<th>Private Location</th>
															<th>IT Center <br><span style="font-weight: normal;font-size: 10px;">(Rajsthan Sampark /<br> Government Building)</span></th>
														</tr>
														<tr>
															<td> District </td>															
															<td> {{ $form->kl_dist_detail  ?? 'N/A' }} </td>													
															<td class="text-center">
																	@if($form->kl_dist_pl == 'Y')
																	Yes
																	@else
																	No
																	@endif
															</td>														
															<td class="text-center">
																@if($form->kl_dist_it == 'Y')
																	Yes
																	@else
																	No
																	@endif
															</td>
														</tr>
														<tr>
															<td> Tehsil </td>															
															<td> {{ $form->kl_tehsil_detail  ?? 'N/A' }} </td>													
															<td class="text-center">
																	@if($form->kl_tehsil_pl == 'Y')
																	Yes
																	@else
																	No
																	@endif
															</td>														
															<td class="text-center">
																@if($form->kl_tehsil_it == 'Y')
																	Yes
																	@else
																	No
																	@endif
															</td>
														</tr>
														<tr>
															<td> Panchayat Samiti </td>															
															<td> {{ $form->kl_panchayat_detail ?? 'N/A' }} </td>													
															<td class="text-center"> 
																	@if($form->kl_panchayat_pl == 'Y')
																	Yes
																	@else
																	No
																	@endif
															</td>														
															<td class="text-center">
																@if($form->kl_panchayat_it == 'Y')
																	Yes
																	@else
																	No
																	@endif
															</td>
														</tr>
														<tr>
															<td> Gram Panchayat </td>															
															<td> {{ $form->kl_gram_detail  ?? 'N/A' }} </td>													
															<td class="text-center">
																@if($form->kl_gram_pl == 'Y')
																	Yes
																	@else
																	No
																	@endif
															</td>														
															<td class="text-center">
																@if($form->kl_gram_it == 'Y')
																	Yes
																	@else
																	No
																	@endif
															</td>
														</tr>
														<tr>
															<td> Village </td>															
															<td> {{ $form->kl_village_detail  ?? 'N/A' }} </td>													
															<td class="text-center">
																	@if($form->kl_village_pl == 'Y')
																	Yes
																	@else
																	No
																	@endif
															</td>														
															<td class="text-center">
																@if($form->kl_village_it == 'Y')
																	Yes
																	@else
																	No
																	@endif
															</td>
														</tr>
													</table>
												</div>
											</div>
											
											<div class="row">
												<div class="col-12">
													<h6 class="font-weight-bold mt-4">If this kiosk had already user ID on e-mitra portal please provide as:-</h6>
													<div class="row">
														<div class="col-5">
															<div class="row">
																<div class="col-1 mr-0 pr-0">1.</div>
																<div class="col-10 ml-0 pl-0">
																	<label>
																		<span class="float-left">ID Proof / Address Proof</span>						
																	</label>
																	<span class="float-right">-</span>
																</div>
																<div class="col-1">																	
																	@if($form->id_proof == 'Y')
																	Yes
																	@else
																	No
																	@endif
																</div>
															</div>
														</div>
														
														<div class="col-7"></div>
													</div>
													<div class="row">
														<div class="col-5">
															<div class="row">
																<div class="col-1 mr-0 pr-0">2.</div>
																<div class="col-10 ml-0 pl-0">
																	<label>
																		<span class="float-left">Police Verification Report</span>						
																	</label>
																	<span class="float-right">-</span>
																</div>
																<div class="col-1">
																	@if($form->police_verification == 'Y')
																	Yes
																	@else
																	No
																	@endif
																</div>
															</div>
														</div>
														
														<div class="col-7"></div>
													</div>
													<div class="row">
														<div class="col-5">
															<div class="row">
																<div class="col-1 mr-0 pr-0">3.</div>
																<div class="col-10 ml-0 pl-0">
																	<label>
																		<span class="float-left">Educational Qualification</span>						
																	</label>
																	<span class="float-right">-</span>
																</div>
																<div class="col-1">
																	@if($form->educational_qualification == 'Y')
																	Yes
																	@else
																	No
																	@endif
																</div>
															</div>
														</div>
														
														<div class="col-7"></div>
													</div>
													<div class="row">
														<div class="col-5">
															<div class="row">
																<div class="col-1 mr-0 pr-0">4.</div>
																<div class="col-10 ml-0 pl-0">
																	<label>
																		<span class="float-left">Bank a/c Detail</span>						
																	</label>
																	<span class="float-right">-</span>
																</div>
																<div class="col-1">
																	@if($form->bank_account_detail == 'Y')
																	Yes
																	@else
																	No
																	@endif
																</div>
															</div>
														</div>
														
														<div class="col-7"></div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-12">
													<div class="row">
														<div class="col-2">
															<label>
																<span class="float-left">Longitude</span>						
															</label>
															<!-- <span class="float-right">-</span> -->
														</div>
														<div class="col-4">
															{{$form->longitude}}
														</div>
														<div class="col-6"></div>
													</div>
													<div class="row">
														<div class="col-2">
															<label>
																<span class="float-left">Latitude</span>						
															</label>
															<!-- <span class="float-right">-</span> -->
														</div>
														<div class="col-4">
															{{$form->latitude}}
														</div>
														<div class="col-6"></div>
													</div>
													<div>Above mention check list are checked are correct in my knowledge.</div>						
												</div>
											</div>
											<div class="row">
												<div class="col-6">
													<label class="py-5">
													<img src="{{ url('imgs/e-mitra_applied/'.$form->sign_kiosk_owner) }}" style="width: 150px;height: 50px;"><br>
														Signature Kiosk Owner
														
													</label>
												</div>
												<div class="col-6 text-right ">
													<label class="py-5 text-center">
													<img src="{{ url('imgs/e-mitra_applied/'.$form->sign_lsp) }}" style="width: 150px;height: 50px;"><br>
														Signature and Seal<br>
														LSP
														
													</label>
												</div>
											</div>

												</div>
												<button class="btn btn-dark print-btn" onclick="printForm(this)">Print</button>