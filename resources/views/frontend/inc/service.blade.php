@extends('frontend.layout.master')

@section('title','Service')

@section('contant')
<!-- Document Wrapper
    ============================================= -->
<div id="wrapper">
  <!-- banner 
    ============================================= -->
  <section class="banner dark" >
    <div id="menu-parallax">
      <div class="bcg background41"
                data-center="background-position: 50% 0px;"
                data-bottom-top="background-position: 50% 100px;"
                data-top-bottom="background-position: 50% -100px;"
                data-anchor-target="#menu-parallax"
              >
        <div class="bg-transparent">
          <div class="banner-content">
            <div class="container" >
              <div class="slider-content  "> <i class="icon-home-ico"></i>
                <h1>Menu</h1>
                <p>Your Taste is Our Goal</p>
                <ol class="breadcrumb">
                  <li><a href="index01.html">Home</a></li>
                  <li>Menu Gird Three Column</li>
                </ol>
              </div>
            </div>
          </div>
          <!-- End Banner content -->
        </div>
        <!-- End bg trnsparent -->
      </div>
    </div>
    <!-- Service parallax -->
  </section>
  <div id="content">
    <div class="menu_grid our-menu text-center padding-b-70">
	@if(!$products->isEmpty())
      <div class="menu-bar dark">
        <ul id="menu-fillter" class="clearfix">
		<li><a href="javascript:;" data-filter=".menu" @if(empty($cslug)) class="current" @endif>All</a></li>
		@foreach($categories as $c)
		  <li><a href="javascript:;" data-filter=".{{ $c->id }}" @if(!empty($cslug) && $cslug == $c->slug) class="current" @endif >{{ strtoupper($c->title) }}</a></li>
		@endforeach
         
        </ul>
	  </div>
	  
      <div class="container mt60" >
        <div id="menu-items" class="masonry-content menu-type dark clearfix" data-defaultfilter="{{ $cslug ? '.'.$cslug : '.menu' }}">
		@foreach($products as $p)
          <article class="menu-item col-md-4 col-sm-6 col-xs-12 {{ $p->category_id ? $p->category_id : ''}} menu">
			  
            <div class="overlay_content overlay-menu">
              <div class="overlay_item"> <span class="label">best seller</span> <img src="{{ url('imgs/menu/desert/desert1.jpg')}}" alt="">
                <div class="overlay">
                  <div class="icons">
                    <h3>{{ $p->title }}</h3>
                    <h3> ₹ {{ $p->regular_price }}</h3>
                    <fieldset class="rating">
                      <span class="active"><i class="fa fa-star"></i></span> <span class="active"><i class="fa fa-star"></i></span> <span class="active"><i class="fa fa-star"></i></span> <span class="active"><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span>
                    </fieldset>
                    <div class="button"> <a class="btn btn-gold" href="#"><i class="fa fa-shopping-cart"></i></a> <a class="btn btn-gold" href="menu_single.html"><i class="fa fa-link"></i></a> </div>
                    <a class="close-overlay hidden">x</a> </div>
                </div>
              </div>
            </div>
		  </article>
		  @endforeach
        </div>
	  </div>
	  @endif
      <a href="#" class="btn btn-gold mt30">View more</a> </div>
  </div>
@stop