@extends('frontend.layout.master')
@section('title','Blog')
@section('contant')
 <main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">
        <ol>
          <li><a href="{{ url('/')}}">Home</a></li>
          <li>Services</li>
        </ol>
      </div>
    </section><!-- End Breadcrumbs -->
    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 entries">
            <article class="entry entry-single">
              <h2 class="entry-title"> {{ $slug->title }}  </h2>
              @if($slug->image!='')
              <img src="{{ url('imgs/service/'.$slug->image) }}" width="100%">
              @endif
              <p>{!! $slug->description !!}</p>
			</article>
          </div>
          <div class="col-lg-4">
            <div class="sidebar" >
              <div style="position: -webkit-sticky; position: sticky; top: 0;">
            	<h3 class="sidebar-title" style>Other E-Mitra Service</h3>
	              <div class="sidebar-item recent-posts">
	              	@foreach( $data1['data'] as $sr )
	                <div class="post-item clearfix">
	                @if($sr['image']!='')
	                  <img src="{{ url('imgs/service/'.$sr['image'])}}" alt="{{ $sr['title'] }}">
	                @endif
	                @if($sr['image']!='')
	                  <h4><a href="{{ url('service/'.$sr['slug'])}}">{{ $sr['title'] }}</a></h4>
	                @else
	                <h4 style="margin-left:10px;"><a href="{{ url('service/'.$sr['slug'])}}">{{ $sr['title'] }}</a></h4>
	                @endif
	                </div>
	                @endforeach
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- End Blog Section -->
  </main><!-- End #main -->
@stop