@extends('frontend.layout.master')

@section('title','Homepage')

@section('contant')
<main id="main">
	<section id="breadcrumbs" class="breadcrumbs">




		<!-- MultiStep Form -->
		
		                <!-- <h2><strong>Sign Up Your User Account</strong></h2>
		                <p>Fill all form field to go to next step</p> -->
		                <div class="container">
		                <div class="row">
		                    <div class="col-md-12 mx-0 py-5">
		                    	<ul id="progressbar" class="text-center">
	                                <li class="active" id="personal"><strong class="mt-4">Basic Detail</strong></li>
	                                <li id="document"><strong class="mt-4">Document</strong></li>
	                                <li id="account"><strong class="mt-4">Bank Deatil</strong></li>
	                                <li id="confirm"><strong class="mt-4">Finish</strong></li>
	                                
	                            </ul>
	                            <h2 class="text-center mb-5"><strong>Apply Form</strong></h2>
		                        
		                        	{{ Form::open(['method' => 'POST', 'files' => true, 'class' => 'applyform', 'id' => 'msform']) }}
		                        	
		                            <!-- progressbar -->
		                             <!-- fieldsets -->
		                             
		                            <fieldset>
		                            	
		                                	<label>All field are mandatory as mark*</label>				
												<div class="row">
													<div class="col-12">
														<h6 class="font-weight-bold">Basic Details :-</h6>
														<div class="row py-4">
															<div class="col-3">
																<label>
																	<span class="float-left">District*</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-9 form-group">
																<input type="text" name="district" required>
															</div>
															<div class="col-3">
																<label>
																	<span class="float-left">LSP*</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-9 form-group">
																<input type="text" name="lsp" class="" required>
															</div>
															<div class="col-3">
																<label>
																	<span class="float-left">Sso ID*</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-9 form-group">
																<input type="text" name="sso_id" class="" required>
															</div>
														</div>
													</div>
													<!-- <div class="col-3">
														<div class="row mx-3">
															<div class="col-12 form-group" >
																<label class="border p-4 text-center input-file" style="position: relative;width: 100%;height: 150px;">
																	<span id="lb">(Click Here) The Latest Photo of Kiosk Owner</span>
																	<input type="file" name="kiosk_owner_photo" class="d-none" id="profile-img" required>
																	
																	<img src="" id="profile-img-tag" width="100%" class="owner_photo" />
																</label>
																
															</div>
														</div>
													</div> -->
																		
												</div>
												<div class="row">
													<div class="col-12">
														<h6 class="font-weight-bold">Kiosk Owner Detail :-</h6>
														<div class="row">
															<div class="col-3">
																<label>
																	<span class="float-left">Kiosk Owner Type*</span>						
																</label>
																<span class="float-right">-</span>								
															</div>
															<div class="col-9">
																<div class="row">
																	<div class="col-3">
																		<input type="checkbox" name="kiosk_owner_type" value="individual">
																		<label>Individual</label>
																	</div>
																	<div class="col-3">
																		<input type="checkbox" name="kiosk_owner_type" value="company">
																		<label>Company</label>
																	</div>
																	<div class="col-6"></div>
																</div>	
															</div>							
														</div>
														<div class="row">
															<div class="col-3">
																<label>
																	<span class="float-left">Kiosk Owner Name*</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-9 form-group">
																<input type="text" name="kiosk_owner_name" required>
															</div>
														</div>
														<div class="row">
															<div class="col-3">
																<label>
																	<span class="float-left">Permanent Address*</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-9 form-group">
																<input type="text" name="permanent_address" class="mb-1" required>
																<input type="text" name="" class="mb-2">
															</div>
														</div>
														<div class="row">
															<div class="col-3">
																<label>
																	<span class="float-left">Owner Phone Numbers*</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-9 form-group">
																<input type="number" name="owner_phone_number" class="mb-1 mobile">
															</div>
														</div>
														<div class="row">
															<div class="col-3">
																<label>
																	<span class="float-left">Mobile*</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-9 form-group">
																<input type="number" name="owner_phone_mobile" class="mb-1 mobile" >
															</div>
														</div>
														<div class="row">
															<div class="col-3">
																<label>
																	<span class="float-left">Owner E-mail ID*</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-9 form-group">
																<input type="email" name="owner_email" class="mb-1" required>
															</div>
														</div>
													</div>
													
												</div>
												<div class="row">
													<div class="col-12">
														<h6 class="font-weight-bold mt-4">Kiosk Owner Detail :-</h6>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Kiosk Center Location*</span>						
																</label>
																<span class="float-right">-</span>								
															</div>
															<div class="col-8">
																<div class="row">
																	<div class="col-3">
																		<input type="radio" name="kiosk_enter_location" value="Urban" id="e1" data-id="urban" checked>
																		<label>Urban</label>
																	</div>
																	<div class="col-3">
																		<input type="radio" name="kiosk_enter_location" value="Rural" data-id="rural">
																		<label>Rural</label>
																	</div>
																	<div class="col-6"></div>
																</div>	
															</div>							
														</div>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Kiosk Center Name (English)*</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8 form-group">
																<input type="text" name="kiosk_center_name" required>
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Kiosk Contact (Hindi)*</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8 form-group">
																<input type="text" name="kiosk_contact" class="" required>								
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Kiosk Contact Person's Name*</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8 form-group">
																<input type="text" name="kiosk_contact_person" class="mb-1" required>
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Kiosk Phone Numbers*</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8 form-group">
																<input type="number" name="kiosk_phone_number" class="mb-1 mobile" >
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Mobile*</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8 form-group">
																<input type="number" name="kiosk_phone_mobile" class="mb-1 mobile">
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Kiosk E-mail ID*</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8 form-group">
																<input type="email" name="kiosk_email_id" class="mb-1" required>
															</div>
														</div>
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Police Station (Thana)*</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8 form-group">
																<input type="text" name="police_station" class="mb-1" required="">
															</div>
														</div>
														        
														<div class="row">
															<div class="col-4">
																<label>
																	<span class="float-left">Kiosk Address*</span>						
																</label>
																<span class="float-right">-</span>
															</div>
															<div class="col-8 form-group">
																<input type="text" name="kiosk_address" class="mb-1" required>
															</div>
														</div>
														<div id="urban"  class="none">
																<div class="row">
															<div class="col-2"></div>
															<div class="col-2 text-left">
																<label class="">
																	<span class="">Panchayat Samiti*</span>						
																</label>
																
															</div>
															<div class="col-8 form-group">
																<input type="text" name="panchayat_samiti" class="mb-1">
															</div>
														</div>
														<div class="row">
															<div class="col-2"></div>
															<div class="col-2 text-left">
																<label class="">
																	<span class="">Gram Panchayat*</span>						
																</label>
																
															</div>
															<div class="col-8 form-group">
																<input type="text" name="gram_panchayat" class="mb-1">
															</div>
														</div>
														<div class="row">
															<div class="col-2"></div>
															<div class="col-2 text-left">
																<label class="">
																	<span class="">Village*</span>						
																</label>
																
															</div>
															<div class="col-2 form-group">
																<input type="text" name="village" class="mb-1">
															</div>
															<div class="col-2">
																PIN Code*
															</div>
															<div class="col-4 form-group">
																<input type="text" name="pincode" class="pincode-input2"  >

															</div>
														</div>
													</div>
													
														<div id="rural">
														<div class="row">
															<div class="col-2"></div>
															<div class="col-2 text-left">
																<label class="">
																	<span class="">Municipal Town*</span>						
																</label>
																
															</div>
															<div class="col-8 form-group">
																<input type="text" name="municipal_town" class="mb-1">
															</div>
														</div>
														<div class="row">
															<div class="col-2"></div>
															<div class="col-2 text-left">
																<label class="">
																	<span class="">Ward Number*</span>						
																</label>
																
															</div>
															<div class="col-2 form-group">
																<input type="text" name="ward_number" class="mb-1">
															</div>
															<div class="col-2">
																PIN Code*
															</div>
															<div class="col-4 form-group">
																<!-- <input type="text" name="pincode" class="mb-1" style="background: url({{url('imgs/pincode-bg.png')}}) center center; background-size: cover;letter-spacing: 50px;padding: 0px 21px;" maxlength="6" > -->
																	<!-- <div style="width:200px"> -->
                        <input type="text" name="pincode" class="pincode-input2"  >
                    <!-- </div> -->
															</div>
														</div>
													</div>
													</div>
													
												</div>
												 <!-- <input type="submit" name="next" class="next action-button" value="Next Step" /> -->
												 <button type="submit" class="next action-button">Next Step</button>


		                            </fieldset>
		                            
		                            
		                            {{ Form::close() }}
		                    </div>

		                </div>
		            </div>

	</section>
</main>
@stop