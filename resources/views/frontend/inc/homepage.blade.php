@extends('frontend.layout.master')

@section('title','Homepage')

@section('contant')

  <section id="hero" class="d-flex justify-cntent-center align-items-center">
    <div id="heroCarousel" class="container carousel carousel-fade" data-ride="carousel">

      <!-- Slide 1 -->
      
      @foreach($slider as $key => $item)
      <div class="carousel-item {{$key == 0 ? 'active' : '' }}">
        <div class="carousel-container">
          <h2 class="animate__animated animate__fadeInDown">{{ $item->title }}</h2>
          <p class="animate__animated animate__fadeInUp">{{ $item->excerpt }}</p>
          <!--<a href="{{ url('/about') }}" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>-->
        </div>
      </div>
      @endforeach

      

      <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon bx bx-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>

      <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon bx bx-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>

    </div>
  </section><!-- End Hero -->


    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">
        <div class="section-title">
          <h2 class="my-5">About Us</h2>
        <div class="row content">
          <div class="col-lg-6 col-sm-5">
            <img src="{{ url('imgs/about/'.$about->image) }}" alt="e-mitra" class="w-100">
          </div>
          <div class="col-lg-6 col-sm-7" id="about_description">
            
             {!! $about->description !!}
            
            <a href="{{ url('/about') }}" class="btn-learn-more">Learn More</a>
          </div>
        </div>

      </div>
    </section><!-- End About Us Section -->
    <section id="services" class="services">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Services</h2>
          <p>Our Online Services giving you all the recharge services and other services also in a single platform by which you can work easily and gracefully.</p>
        </div>

        <div class="row">
          @foreach($service as $item)
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="row icon-box">
            @if(!empty($item->image))
              <div class="col-md-3">                
                <img src="{{ url('imgs/service/'.$item->image) }}" width="100%" alt="{{ $item->title }}">
              </div>
            @endif
              <div class="col-md-9">
                <h4><a href="{{ url('service/'.$item->slug) }}">{{ $item->title }}</a></h4>
                <p>{{ $item->excerpt }}</p>
              </div>
              
            </div>
          </div>
          @endforeach          
        </div>

      </div>
    </section><!-- End Services Section -->

   

    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Frequently Asked Questions</h2>
        </div>

        <div class="faq-list">
          <ul>
            @foreach( $faq as $item )
            <li data-aos="fade-up" data-aos-delay="200">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-{{ $item->id }}" class="collapsed">{{ $item->question }}<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-{{ $item->id }}" class="collapse" data-parent=".faq-list">
                <p>
                 {!! $item->answer !!}
                </p>
              </div>
            </li>
            @endforeach
          </ul>
        </div>

      </div>
    </section><!-- End Frequently Asked Questions Section -->

    @stop