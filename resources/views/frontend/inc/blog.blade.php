@extends('frontend.layout.master')

@section('title','blog')

@section('contant')
 <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="{{ url('/') }}">Home</a></li>
          <li>Blog</li>
        </ol>
        <h2>Blog</h2>

      </div>
    </section><!-- End Breadcrumbs -->

     <section id="services" class="services">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Blog</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div class="row">
          @foreach($blog as $item)
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="row icon-box">
              <!-- <i class="icofont-computer"></i>-->
              <div class="col-md-3">                
                <img src="{{ url('imgs/blog/'.$item->image) }}" width="100%">
              </div>
              <div class="col-md-9">
                <h4><a href="{{ url('blog/'.$item->slug) }}">{{ $item->title }}</a></h4>
                <p>{{ $item->excerpt }}</p>
              </div>
              
            </div>
          </div>
          @endforeach          
        </div>

      </div>
    </section>

  </main><!-- End #main -->
@stop