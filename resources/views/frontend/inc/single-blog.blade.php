@extends('frontend.layout.master')

@section('title','Blog')

@section('contant')
 <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="{{ url('/')}}">Home</a></li>
          <li><a href="{{ url('/blog')}}">Blogs</a></li>
        </ol>
        <h2>{{ $slug->title }}</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container">

        <div class="row">

          <div class="col-lg-8 entries">

            <article class="entry entry-single">

              <div class="entry-img">
                <img src="{{ url('img/blog-1.jpg')}}" alt="" class="img-fluid">
              </div>

              <h2 class="entry-title">
                <a href="#">{{ $slug->title }}</a>
              </h2>
              @if($slug->image!='')
              <img src="{{ url('imgs/blog/'.$slug->image) }}" width="100%">
              @else
              <img src="{{ url('imgs/dummy.jpg') }}">
              @endif
              <p>{!! $slug->description !!}</p>
			</article>


              <!-- End blog comments -->

          </div><!-- End blog entries list -->

          <div class="col-lg-4">

            <div class="sidebar" >

              

              
            <div style="position: -webkit-sticky; position: sticky; top: 0;">
            	<h3 class="sidebar-title">Recent Posts</h3>
	              <div class="sidebar-item recent-posts">
	              	@foreach( $data1['data'] as $sr )
	                <div class="post-item clearfix">
	                @if($sr['image']!='')
	                  <img src="{{ url('imgs/blog/'.$sr['image'])}}" alt="{{ $sr['title'] }}">
	                  @else
	                  <img src="{{ url('imgs/dummy.jpg') }}" alt=" {{ $sr['title'] }}">
	                  @endif
	                  <h4><a href="{{ url('blog/'.$sr['slug'])}}">{{ $sr['title'] }}</a></h4>
	                  <time datetime="2020-01-01">{{ $sr['created_at'] }}</time>
	                </div>
	                @endforeach
                

                

              </div>
            </div>
              <!-- End sidebar recent posts-->

              

            </div><!-- End sidebar -->

          </div><!-- End blog sidebar -->

        </div>

      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->
@stop