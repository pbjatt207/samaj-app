@extends('admin.layouts.master')

@section('title', 'Dasboard | Agent Panel')

@section('content')

<div class="container-fluid">
    <div class="row dash-block-group">
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <div class="master-icon">
                        <i class="icon-person"></i>
                    </div>
                    <h3>View Agents</h3>
                </div>
                <a href="{{ route('admin.agent.index') }}" class="btn btn-block btn-dark">
                    Enter <i class="icon-long-arrow-right"></i>
                </a>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <div class="master-icon">
                        <i class="icon-person_add"></i>
                    </div>
                    <h3>Add Agent</h3>
                </div>
                <a href="{{ route('admin.agent.create') }}" class="btn btn-block btn-dark">
                    Enter <i class="icon-long-arrow-right"></i>
                </a>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <div class="master-icon">
                        <i class="icon-person"></i>
                    </div>
                    <h3>View Suppliers</h3>
                </div>
                <a href="{{ route('admin.supplier.index') }}" class="btn btn-block btn-dark">
                    Enter <i class="icon-long-arrow-right"></i>
                </a>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <div class="master-icon">
                        <i class="icon-person_add"></i>
                    </div>
                    <h3>Add Supplier</h3>
                </div>
                <a href="{{ route('admin.supplier.create') }}" class="btn btn-block btn-dark">
                    Enter <i class="icon-long-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>
@endsection