@extends('admin.layouts.form', [
'page_title' => 'Add Agents',
'b_items' => [ route('admin.account.master') => 'Account Master' ],
'errors' => $errors
])

@section('title', 'Add Agents')

@section('card_title', 'Create New Agent Account')

@section('form')
{!! Form::open(['url' => route('admin.agent.store')]) !!}
@include('admin.pages.inventory.agent.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Create', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/agent.js') }}" type="module" defer></script>
<script src="{{ url('admin/js/chunks/location.js') }}" type="module" defer></script>
@endsection