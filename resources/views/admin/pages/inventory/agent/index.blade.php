@extends('admin.layouts.lists', [
'page_title' => 'View Cities',
'b_items' => [ route('admin.account.master') => 'Account Master'],
'errors' => $errors
])

@section('title', 'View Cities')
@section('card_title', 'List All Cities')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.city.index'), 'columns' => ['Sr. No.', 'Name', 'Email',
'Mobile', 'Sale', '' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/city.js') }}" type="module" defer></script>
@endsection