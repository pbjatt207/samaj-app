@include('admin.components.inputs', [ 'fields' => [
[
'label' => Form::label('name', 'Name *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Enter Name', 'required' =>
'required'])
],
[
'label' => Form::label('mobile', 'Mobile No. *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::tel('mobile', '', ['class' => 'form-control', 'placeholder' => 'Enter Mobile No.', 'required' =>
'required'])
],
[
'label' => Form::label('email', 'Email Address *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'Enter Email Address',
'required' => 'required'])
],
[
'label' => Form::label('password', 'Password *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::password('password', ['class' => 'form-control', 'placeholder' => 'Enter Password', 'autocomplete' =>
'new-password'])
],
[
'label' => Form::label('address', 'Address *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::textarea('address', '', ['class' => 'form-control', 'rows' => 5, 'placeholder' => 'Enter Address',
'required' => 'required'])
]]
])

<div class="row">
    <div class="offset-sm-2 col-sm-10">
        <div class="row">
            <div class="col-sm-3 form-group">
                {!! Form::select('country_id', $countries, null, ['placeholder' => 'Select Country', 'class' =>
                'form-control country', 'required' => 'required' ])
                !!}
            </div>
            <div class="col-sm-3 form-group">
                {!! Form::select('state_id', $states, null, ['placeholder' => 'Select State', 'class' =>
                'form-control state', 'required' => 'required']) !!}
            </div>
            <div class="col-sm-3 form-group">
                {!! Form::select('city_id', $cities, null, ['placeholder' => 'Select City', 'class' => 'form-control
                city', 'required' => 'required' ])
                !!}
            </div>
            <div class="col-sm-3 form-group">
                {!! Form::text('pincode', '', ['placeholder' => 'Enter Pincode', 'class' => 'form-control', 'required'
                => 'required']) !!}
            </div>
        </div>
    </div>
</div>