@extends('admin.layouts.lists', [
'page_title' => 'Agent '.$agent->name,
'b_items' => [
route('admin.account.master') => 'Account Master',
route('admin.agent.index') => 'Agent',
],
'errors' => $errors
])

@section('title', 'View Sales')
@section('card_title', 'List All Agents')

@section('card_table')

@if(!$sales->isEmpty())
<table class="table table-bordered">
    <thead>
        <tr>
            <th>Sr No.</th>
            <th>Item Name</th>
            <th>Qty</th>
            <th>Name</th>
            <th>Mobile No.</th>
            <th>Address</th>
            <th>Remarks</th>
            <th>Date &amp; Time</th>
        </tr>
    </thead>
    <tbody>
        @foreach($sales as $index => $sale)
        <tr>
            <td>{{ $index + $sales->firstItem() }}.</td>
            <td>{{ $sale->item_name }}</td>
            <td>{{ $sale->qty }}</td>
            <td>{{ $sale->name }}</td>
            <td>{{ $sale->mobile }}</td>
            <td>{{ $sale->address }}</td>
            <td>{{ $sale->remarks }}</td>
            <td>{{ date('F d, Y h:i A', strtotime($sale->created_at)) }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@else
No record(s) found.
@endif
@endsection

@section('pagination')
{{ $sales->links() }}
@endsection

@section('extra_script')
@endsection