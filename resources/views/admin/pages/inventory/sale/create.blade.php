@extends('admin.layouts.master')

@section('title', 'Sale')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-8 col-lg-9">
            <div class="card">
                <div class="card-header">
                    Select Items
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach($items as $item)
                        <div class="col-sm-3">
                            <div class="bg-light d-flex rounded" style="overflow: hidden">
                                <div class="w-25">
                                    <img src="{{ \Storage::url($item->image) }}" alt="" class="p_image w-100">
                                </div>
                                <div class="w-75 p-2">
                                    <h6 class="mt-0">{{ $item->name }}</h6>
                                    <div>₹{{ $item->sale_price }}</div>
                                </div>
                            </div>
                            <div class="py-4 cart-btn-group text-center" data-item_id="{{ $item->id }}"
                                data-type="sale">
                                <button class="btn btn-outline-secondary btn-sm add_to_cart_btn mx-auto"
                                    data-item="{{ json_encode($item) }}">
                                    <i class="icon-plus2"></i> Add
                                </button>
                                <div class="input-group input-group-sm input-sppiner mx-auto" style="display: none;">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-outline-secondary minus-btn" type="button">
                                            <i class="icon-minus2"></i>
                                        </button>
                                    </div>
                                    <input type="number" class="form-control" value="0" min="0">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary plus-btn" type="button">
                                            <i class="icon-plus2"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-lg-3">
            <div class="card">
                <div class="card-header">
                    Cart Items
                </div>
                <div class="card-body">
                    {{ Form::open( ['url' => route('admin.sale.store')] ) }}
                    <input type="hidden" name="cart_items" id="cart_items_input" value="">
                    <div id="cartCaculations"></div>
                    <hr>
                    <div class="form-group">
                        {{ Form::select('agent_id', $agents, null, ['class' => 'form-control', 'placeholder' => 'Select Customer / Agent', 'required' => 'required']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::select('payment_mode', $payment_modes, 'Cash', ['class' => 'form-control', 'required' => 'required']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::text('txn_number', '', ['placeholder' => 'Enter Txn No. or Cheque No.', 'class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('discount', 'Discount (INR)') }}
                        {{ Form::number('discount', 0, ['placeholder' => 'Enter Discount Amount', 'class' => 'form-control']) }}
                    </div>
                    <button type="submit" class="btn btn-primary btn-block submit-btn" disabled>
                        <i class="icon-check1"></i>
                        Save &amp; Submit
                    </button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/cart.js') }}" type="module" defer></script>
@endsection