@extends('agent.layouts.lists', [
'page_title' => 'View Sales',
'b_items' => [route('agent.report.master') => 'Report Master'],
'errors' => $errors
])

@section('title', 'View Sales')
@section('card_title', 'List All Sales')

@section('card_table')
@include('agent.components.datatable', [
'action' => route('api.sale.index'),
'columns' => [
'Sr. No.',
'Supplier Name',
'PO Number',
'Total Amount',
'Total Items',
''
]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/sale.js') }}" type="module" defer></script>
@endsection