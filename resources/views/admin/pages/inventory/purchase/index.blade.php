@extends('admin.layouts.lists', [
'page_title' => 'View Purchases',
'b_items' => [route('admin.report.master') => 'Report Master'],
'errors' => $errors
])

@section('title', 'View Purchases')
@section('card_title', 'List All Purchases')

@section('card_table')
@include('admin.components.datatable', [
'action' => route('api.purchase.index'),
'columns' => [
'Sr. No.',
'Supplier Name',
'PO Number',
'Total Amount',
'Total Items',
''
]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/purchase.js') }}" type="module" defer></script>
@endsection