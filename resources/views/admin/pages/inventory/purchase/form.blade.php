@include('admin.components.inputs', [
'fields' => [
[
'label' => Form::label('name', 'Name *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Enter Name', 'required' =>
'required'])
],
[
'label' => Form::label('purchase_price', 'Purchase Price *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::number('purchase_price', '', ['class' => 'form-control', 'placeholder' => 'Enter Purchase Price',
'required' =>
'required'])
],
[
'label' => Form::label('margin', 'Margin (%) *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::number('margin', '', ['class' => 'form-control', 'placeholder' => 'Enter Margin (%)',
'required' => 'required'])
],
[
'label' => Form::label('sale_price', 'Sale Price *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::number('sale_price', '', [ 'class' => 'form-control', 'placeholder' => 'Enter Sale Price', 'required'
=> 'required' ])
],
[
'label' => Form::label('gst_rate', 'GST Rate *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::select('gst_rate', $gst_rates, null, ['class' => 'form-control', 'required' => 'required' ])
],
[
'label' => Form::label('description', 'Description', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::textarea('description', '', [
'class' => 'form-control',
'rows' => 8,
'placeholder' => 'Enter Description' ])
],
[
'label' => Form::label('image', 'Upload Image *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::file('image', [
'accept' => 'image/*',
'rows' => 8,
'placeholder' => 'Enter Description' ])
]
]
])