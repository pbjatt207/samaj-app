@extends('admin.layouts.lists', [
'page_title' => 'View Suppliers',
'b_items' => [ route('admin.account.master') => 'Account Master'],
'errors' => $errors
])

@section('title', 'View Suppliers')
@section('card_title', 'List All Suppliers')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.supplier.index'), 'columns' => ['Sr. No.', 'Name',
'Email', 'Mobile', '' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/supplier.js') }}" type="module" defer></script>
@endsection