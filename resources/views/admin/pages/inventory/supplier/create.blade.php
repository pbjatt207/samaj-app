@extends('admin.layouts.form', [
'page_title' => 'Add Suppliers',
'b_items' => [ route('admin.account.master') => 'Account Master' ],
'errors' => $errors
])

@section('title', 'Add Suppliers')

@section('card_title', 'Create New Supplier Account')

@section('form')
{!! Form::open(['url' => route('admin.supplier.store')]) !!}
@include('admin.pages.inventory.supplier.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Create', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/supplier.js') }}" type="module" defer></script>
<script src="{{ url('admin/js/chunks/location.js') }}" type="module" defer></script>
@endsection