@extends('admin.layouts.form', [
'page_title' => 'Edit Suppliers',
'b_items' => [ route('admin.account.master') => 'Account Master'],
'errors' => $errors
])

@section('title', 'Edit Suppliers')

@section('card_title', 'Update Supplier Info')

@section('form')
{!! Form::open(['method' => 'PUT', 'url' => route('admin.supplier.update', $supplier->id)]) !!}
@include('admin.pages.inventory.supplier.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Update Details', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/supplier.js') }}" type="module" defer></script>
@endsection