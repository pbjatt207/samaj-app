@extends('admin.layouts.form', [
'page_title' => 'Add Item',
'b_items' => [ route('admin.item.master') => 'Item Master' ],
'errors' => $errors
])

@section('title', 'Add Items')

@section('card_title', 'Create New Item')

@section('form')
{!! Form::open(['url' => route('admin.item.store'), 'files' => true]) !!}
@include('admin.pages.inventory.item.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Create', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/item.js') }}" type="module" defer></script>
@endsection