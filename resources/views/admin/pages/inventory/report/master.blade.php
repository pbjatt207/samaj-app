@extends('admin.layouts.master')

@section('title', 'Report Master | Inventory')

@section('content')
<div class="container-fluid">
    <div class="row dash-block-group">
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <div class="master-icon">
                        <i class="icon-tag2"></i>
                    </div>
                    <h3>Purchases</h3>
                </div>
                <a href="{{ route('admin.purchase.index') }}" class="btn btn-block btn-dark">
                    Enter <i class="icon-long-arrow-right"></i>
                </a>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <div class="master-icon">
                        <i class="icon-cart"></i>
                    </div>
                    <h3>Sales</h3>
                </div>
                <a href="{{ route('admin.sale.index') }}" class="btn btn-block btn-dark">
                    Enter <i class="icon-long-arrow-right"></i>
                </a>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <div class="master-icon">
                        <i class="icon-database2"></i>
                    </div>
                    <h3>Stock</h3>
                </div>
                <a href="{{ route('admin.report.stock') }}" class="btn btn-block btn-dark">
                    Enter <i class="icon-long-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>
@endsection