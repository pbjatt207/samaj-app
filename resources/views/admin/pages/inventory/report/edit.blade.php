@extends('admin.layouts.form', [
'page_title' => 'Edit Items',
'b_items' => [ route('admin.item.master') => 'Account Master'],
'errors' => $errors
])

@section('title', 'Edit Items')

@section('card_title', 'Update Item Info')

@section('form')
{!! Form::open(['method' => 'PUT', 'url' => route('admin.item.update', $item->id), 'files' => true]) !!}
@include('admin.pages.inventory.item.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Update Details', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/item.js') }}" type="module" defer></script>
@endsection