@extends('admin.layouts.lists', [
'page_title' => 'Stock Report',
'b_items' => [ route('admin.report.master') => 'Report Master'],
'errors' => $errors
])

@section('title', 'Stock Report')
@section('card_title', 'Stock Report')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.stock.index'), 'columns' => ['Sr. No.', 'Name', 'Image',
'Purchase Price', 'Stock Qty']])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/stock.js') }}" type="module" defer></script>
@endsection