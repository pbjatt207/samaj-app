@extends('admin.layouts.lists', [
'page_title' => 'View Items',
'b_items' => [ route('admin.item.master') => 'Item Master'],
'errors' => $errors
])

@section('title', 'View Items')
@section('card_title', 'List All Items')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.item.index'), 'columns' => ['Sr. No.', 'Name', 'Image',
'Purchase Price', 'Sale Price', '']])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/item.js') }}" type="module" defer></script>
@endsection