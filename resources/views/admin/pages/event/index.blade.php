@extends('admin.layouts.lists', [
'page_title' => 'View Events',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>'', 'title'=>'','icon'=>'']
])

@section('title', 'View Events')
@section('card_title', 'List All Events')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.event.list'), 'columns' => ['Sr. No.', 'Name','Image','Date','Description','' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/event.js') }}" type="module" defer></script>
@endsection