@extends('admin.layouts.form', [
'page_title' => 'Add City',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.city.index'), 'title'=>'View Cities','icon'=>'icon-file-text2']
])

@section('title', 'Add Agents')

@section('card_title', 'Create New City')

@section('form')
{!! Form::open(['url' => route('admin.city.store')]) !!}
@include('admin.pages.city.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Create', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/city.js') }}" type="module" defer></script>
<script src="{{ url('admin/js/chunks/location.js') }}" type="module" defer></script>
@endsection