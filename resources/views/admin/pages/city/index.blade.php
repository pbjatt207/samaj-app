@extends('admin.layouts.lists', [
'page_title' => 'View Cities',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>route('admin.city.create'), 'title'=>'Add City','icon'=>'icon-plus']
])

@section('title', 'View Cities')
@section('card_title', 'List All Cities')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.city.list'), 'columns' => ['Sr. No.', 'Name', '' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/city.js') }}" type="module" defer></script>
@endsection