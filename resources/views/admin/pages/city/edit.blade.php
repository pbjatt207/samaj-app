@extends('admin.layouts.form', [
'page_title' => 'Edit City',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.city.index'), 'title'=>'View Cities','icon'=>'icon-file-text2']
])

@section('title', 'Edit City')

@section('card_title', 'Update City Info')

@section('form')
{!! Form::open(['method' => 'PUT', 'url' => route('admin.city.update', $city->id)]) !!}
@include('admin.pages.city.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Update Details', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/city.js') }}" type="module" defer></script>
@endsection