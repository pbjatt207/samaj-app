@extends('admin.layouts.form', [
'page_title' => 'Edit Merital Status',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.meritalstatus.index'), 'title'=>'View Merital Status','icon'=>'icon-file-text2']
])

@section('title', 'Edit Meritalstatus')

@section('card_title', 'Update Merital Status Info')

@section('form')
{!! Form::open(['method' => 'PUT', 'url' => route('admin.meritalstatus.update', $meritalstatus->id)]) !!}
@include('admin.pages.meritalstatus.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Update Details', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/meritalstatus.js') }}" type="module" defer></script>
@endsection