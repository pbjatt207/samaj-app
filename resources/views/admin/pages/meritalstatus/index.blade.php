@extends('admin.layouts.lists', [
'page_title' => 'View Merital Status',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>route('admin.meritalstatus.create'), 'title'=>'Add Merital Status','icon'=>'icon-plus']
])

@section('title', 'View Merital Status')
@section('card_title', 'List All Merital Status')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.meritalstatus.list'), 'columns' => ['Sr. No.', 'Name', '' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/meritalstatus.js') }}" type="module" defer></script>
@endsection