@extends('admin.layouts.lists', [
'page_title' => 'View Posts',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>'', 'title'=>'','icon'=>'']
])

@section('title', 'View Posts')
@section('card_title', 'List All Posts')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.post.list'), 'columns' => ['Sr. No.', 'Image','User','Caption' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/post.js') }}" type="module" defer></script>
@endsection