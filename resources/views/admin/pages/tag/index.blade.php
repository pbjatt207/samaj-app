@extends('admin.layouts.lists', [
'page_title' => 'View Tags',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>'', 'title'=>'','icon'=>'']
])

@section('title', 'View Tags')
@section('card_title', 'List All Tags')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.tag.list'), 'columns' => ['Sr. No.', 'Name','Description']])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/tag.js') }}" type="module" defer></script>
@endsection