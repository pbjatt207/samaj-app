@extends('admin.layouts.lists', [
'page_title' => 'View Feelings',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>route('admin.feeling.create'), 'title'=>'Add Feeling','icon'=>'icon-plus']
])

@section('title', 'View Feelings')
@section('card_title', 'List All Feelings')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.feeling.list'), 'columns' => ['Sr. No.', 'Name', '' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/feeling.js') }}" type="module" defer></script>
@endsection