@extends('admin.layouts.form', [
'page_title' => 'Edit Feeling',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.feeling.index'), 'title'=>'View Feelings','icon'=>'icon-file-text2']
])

@section('title', 'Edit Feeling')

@section('card_title', 'Update Feeling Info')

@section('form')
{!! Form::open(['method' => 'PUT', 'url' => route('admin.feeling.update', $feeling->id)]) !!}
@include('admin.pages.feeling.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Update Details', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/feeling.js') }}" type="module" defer></script>
@endsection