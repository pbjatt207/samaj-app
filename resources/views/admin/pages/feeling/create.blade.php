@extends('admin.layouts.form', [
'page_title' => 'Add Feeling',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.feeling.index'), 'title'=>'View Feelings','icon'=>'icon-file-text2']
])

@section('title', 'Add Feeling')

@section('card_title', 'Create New Feeling')

@section('form')
{!! Form::open(['url' => route('admin.feeling.store')]) !!}
@include('admin.pages.feeling.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Create', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/feeling.js') }}" type="module" defer></script>
<script src="{{ url('admin/js/chunks/location.js') }}" type="module" defer></script>
@endsection