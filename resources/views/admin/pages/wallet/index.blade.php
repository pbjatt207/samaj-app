@extends('admin.layouts.master')

@section('title', 'Wallet | Admin Panel')

@section('content')

@foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has($msg))
        <div class="alert alert-{{ $msg }}">{{ Session::get($msg) }}</div>
    @endif
@endforeach

<div
    class="dash-block"
>
    <div
        class="card"
    >
        <div class="card-header">
            <span class="float-right">(₹{{ number_format($total_amount, 2) }})</span>
            Wallet Summary
        </div>
        <div class="card-body">
            @if($wallets->isEmpty())
                No record(s) found.
            @else
            <div
                class="row"
            >
                <div 
                    class="col-sm-4 col-lg-3 form-group"
                >
                    <label for="searchByAgent">Search by Agent</label>
                    <select 
                        name="search[agent_id]"
                        class="select2 form-control"
                        data-placeholder="Search by Agent"
                        data-allowClear="true"
                        id="searchByAgent"
                    >
                        <option value="">Search by Agent</option>
                        @foreach($agents as $index => $agent)
                        <option value="{{ $agent->id }}" @if(@request('search')['agent_id'] == $agent->id) selected @endif>{{ "{$agent->name} ({$agent->mobile})" }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div
                class="table-responsive"
            >
                <table
                    class="table table-bordered table-striped table-hover"
                >
                    <thead>
                        <tr>
                            <th>Sr. No.</th>
                            <th>Agent</th>
                            <th>Date</th>
                            <th>Remarks</th>
                            <th>Credit</th>
                            <th>Debit</th>
                            <th>Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $balance = 0; @endphp
                        @foreach($wallets as $index => $w)
                            @php 
                                if($w->type == 'credit') $balance += $w->amount;
                                if($w->type == 'debit') $balance -= $w->amount;
                            @endphp
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>
                                <div><strong>{{ $w->agent->name }}</strong></div>
                                <div>({{ $w->agent->mobile }})</div>
                            </td>
                            <td>{{ date('d M Y', strtotime($w->created_at)) }}</td>
                            <td>
                                <div>{{ $w->remarks }}</div>
                                <div><strong>Txn ID: </strong>{{ $w->txn_number }}</div>
                            </td>
                            <td>₹{{ $w->type == 'credit' ? $w->amount : 0 }}</td>
                            <td>₹{{ $w->type == 'debit' ? $w->amount : 0 }}</td>
                            <td>₹{{ $balance }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection

@section('footer_script')
<script>
    $(function () {
        $(document).on('change', '#searchByAgent', function () {
            window.location = `{{ route('admin.wallet.index') }}?&${encodeURI("search[agent_id]")}=${this.value}`;
        });
    })
</script>
@endsection