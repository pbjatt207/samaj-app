@include('admin.components.inputs', [ 'fields' => [
[
'label' => Form::label('name', 'Name *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Enter Name', 'required' =>
'required'])
],[
'label' => Form::label('version', 'Version *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::text('version', '', ['class' => 'form-control', 'placeholder' => 'Enter Version', 'required' =>
'required'])
],[
'label' => Form::label('package_name', 'Package Name *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::text('package_name', '', ['class' => 'form-control', 'placeholder' => 'Enter Package Name', 'required' =>
'required'])
]
]
])

<div class="row">
    <div class="offset-sm-2 col-sm-10">
        <div class="row">
            <div class="col-sm-3 form-group">
                {!! Form::select('religion_id[]', $religions, null, ['placeholder' => 'Select Religion', 'class' =>
                'form-control religion multiple-select', 'required' => 'required','multiple' ])
                !!}
            </div>
            <div class="col-sm-3 form-group">
                {!! Form::select('cast_id[]', $casts, null, ['placeholder' => 'Select Cast', 'class' =>
                'form-control cast multiple-select', 'multiple']) !!}
            </div>
            <div class="col-sm-3 form-group">
                {!! Form::file('logo', ['class' => 'form-control', 'placeholder' => 'Logo']) !!}
            </div>
        </div>
    </div>
</div>