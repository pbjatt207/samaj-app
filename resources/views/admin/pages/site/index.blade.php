@extends('admin.layouts.lists', [
'page_title' => 'View Site',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>route('admin.site.create'), 'title'=>'Add Site','icon'=>'icon-plus']
])

@section('title', 'View Site')
@section('card_title', 'List All Site')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.site.list'), 'columns' => ['Sr. No.', 'Name','version','logo' ,'' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/site.js') }}" type="module" defer></script>
@endsection