@extends('admin.layouts.form', [
'page_title' => 'Add Country',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.country.index'), 'title'=>'View Countries','icon'=>'icon-file-text2']
])

@section('title', 'Add Agents')

@section('card_title', 'Create New Country')

@section('form')
{!! Form::open(['url' => route('admin.country.store')]) !!}
@include('admin.pages.country.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Create', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/country.js') }}" type="module" defer></script>
<script src="{{ url('admin/js/chunks/location.js') }}" type="module" defer></script>
@endsection