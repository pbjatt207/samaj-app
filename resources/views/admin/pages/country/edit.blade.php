@extends('admin.layouts.form', [
'page_title' => 'Edit Country',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.country.index'), 'title'=>'View Countries','icon'=>'icon-file-text2']
])

@section('title', 'Edit Country')

@section('card_title', 'Update Country Info')

@section('form')
{!! Form::open(['method' => 'PUT', 'url' => route('admin.country.update', $country->id)]) !!}
@include('admin.pages.country.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Update Details', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/country.js') }}" type="module" defer></script>
@endsection