@include('admin.components.inputs', [ 'fields' => [
[
'label' => Form::label('name', 'Name *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Enter Name', 'required' =>
'required'])
],
[
'label' => Form::label('sortname', 'Sort Name *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::text('sortname', '', ['class' => 'form-control', 'placeholder' => 'Enter Sort Name', 'required' =>
'required'])
],
[
'label' => Form::label('phonecode', 'Phone Code *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::text('phonecode', '', ['class' => 'form-control', 'placeholder' => 'Enter Phone Code', 'required' =>
'required'])
]
]])