@extends('admin.layouts.lists', [
'page_title' => 'View Countries',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>route('admin.country.create'), 'title'=>'Add Country','icon'=>'icon-plus']
])

@section('title', 'View Countries')
@section('card_title', 'List All Countries')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.country.list'), 'columns' => ['Sr. No.', 'Name','Sort Name','Phone Code', '' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/country.js') }}" type="module" defer></script>
@endsection