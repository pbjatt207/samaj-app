@extends('admin.layouts.lists', [
'page_title' => 'View Places',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>'', 'title'=>'','icon'=>'']
])

@section('title', 'View Places')
@section('card_title', 'List All Places')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.place.list'), 'columns' => ['Sr. No.', 'Name','Image','Description' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/place.js') }}" type="module" defer></script>
@endsection