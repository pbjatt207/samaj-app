@include('admin.components.inputs', [ 'fields' => [
[
'label' => Form::label('name', 'Name *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Enter Name', 'required' =>
'required'])
]]
])

<div class="row">
    <div class="offset-sm-2 col-sm-10">
        <div class="row">
            <div class="col-sm-3 form-group">
                {!! Form::select('country_id', $countries, null, ['placeholder' => 'Select Country', 'class' =>
                'form-control country', 'required' => 'required' ])
                !!}
            </div>
        </div>
    </div>
</div>