@extends('admin.layouts.form', [
'page_title' => 'Add State',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.state.index'), 'title'=>'View States','icon'=>'icon-file-text2']
])

@section('title', 'Add Agents')

@section('card_title', 'Create New State')

@section('form')
{!! Form::open(['url' => route('admin.state.store')]) !!}
@include('admin.pages.state.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Create', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/state.js') }}" type="module" defer></script>
<script src="{{ url('admin/js/chunks/location.js') }}" type="module" defer></script>
@endsection