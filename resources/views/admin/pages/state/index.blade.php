@extends('admin.layouts.lists', [
'page_title' => 'View States',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>route('admin.state.create'), 'title'=>'Add State','icon'=>'icon-plus']
])

@section('title', 'View States')
@section('card_title', 'List All Cities')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.state.list'), 'columns' => ['Sr. No.', 'Name', '' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/state.js') }}" type="module" defer></script>
@endsection