@extends('admin.layouts.form', [
'page_title' => 'Edit State',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.state.index'), 'title'=>'View States','icon'=>'icon-file-text2']
])

@section('title', 'Edit State')

@section('card_title', 'Update State Info')

@section('form')
{!! Form::open(['method' => 'PUT', 'url' => route('admin.state.update', $state->id)]) !!}
@include('admin.pages.state.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Update Details', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/state.js') }}" type="module" defer></script>
@endsection