@extends('admin.layouts.lists', [
'page_title' => 'View Languages',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>route('admin.language.create'), 'title'=>'Add Language','icon'=>'icon-plus']
])

@section('title', 'View Languages')
@section('card_title', 'List All Languages')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.language.list'), 'columns' => ['Sr. No.', 'Name', '' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/language.js') }}" type="module" defer></script>
@endsection