@extends('admin.layouts.form', [
'page_title' => 'Edit Language',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.language.index'), 'title'=>'View Languages','icon'=>'icon-file-text2']
])

@section('title', 'Edit Language')

@section('card_title', 'Update Language Info')

@section('form')
{!! Form::open(['method' => 'PUT', 'url' => route('admin.language.update', $language->id)]) !!}
@include('admin.pages.language.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Update Details', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/language.js') }}" type="module" defer></script>
@endsection