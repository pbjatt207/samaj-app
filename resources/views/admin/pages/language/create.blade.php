@extends('admin.layouts.form', [
'page_title' => 'Add Language',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.language.index'), 'title'=>'View Languages','icon'=>'icon-file-text2']
])

@section('title', 'Add Language')

@section('card_title', 'Create New Language')

@section('form')
{!! Form::open(['url' => route('admin.language.store')]) !!}
@include('admin.pages.language.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Create', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/language.js') }}" type="module" defer></script>
@endsection