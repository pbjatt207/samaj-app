@extends('admin.layouts.form', [
'page_title' => 'Edit Site',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.site.index'), 'title'=>'View Site','icon'=>'icon-file-text2']
])

@section('title', 'Edit Site')

@section('card_title', 'Update Site Info')

@section('form')
{!! Form::open(['method' => 'PUT', 'url' => route('admin.site.update', $site->id),'files'=> 'true']) !!}
@include('admin.pages.site.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Update Details', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/site.js') }}" type="module" defer></script>
@endsection