@extends('admin.layouts.form', [
'page_title' => 'Add Site',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.site.index'), 'title'=>'View Site','icon'=>'icon-file-text2']
])

@section('title', 'Add Site')

@section('card_title', 'Create New Site')

@section('form')
{!! Form::open(['url' => route('admin.site.store'),'files'=> 'true']) !!}
@include('admin.pages.site.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Create', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/site.js') }}" type="module" defer></script>
<script src="{{ url('admin/js/chunks/castdetail.js') }}" type="module" defer></script>
@endsection