@extends('admin.layouts.lists', [
'page_title' => 'View Users',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>'', 'title'=>'','icon'=>'']
])

@section('title', 'View Users')
@section('card_title', 'List All Users')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.user.list'), 'columns' => ['Sr. No.', 'Name','Mobile','Email' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/user.js') }}" type="module" defer></script>
@endsection