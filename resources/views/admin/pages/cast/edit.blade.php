@extends('admin.layouts.form', [
'page_title' => 'Edit Cast',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.cast.index'), 'title'=>'View Casts','icon'=>'icon-file-text2']
])

@section('title', 'Edit Cast')

@section('card_title', 'Update Cast Info')

@section('form')
{!! Form::open(['method' => 'PUT', 'url' => route('admin.cast.update', $cast->id)]) !!}
@include('admin.pages.cast.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Update Details', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/cast.js') }}" type="module" defer></script>
@endsection