@extends('admin.layouts.form', [
'page_title' => 'Add Cast',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.cast.index'), 'title'=>'View Casts','icon'=>'icon-file-text2']
])

@section('title', 'Add Agents')

@section('card_title', 'Create New Cast')

@section('form')
{!! Form::open(['url' => route('admin.cast.store')]) !!}
@include('admin.pages.cast.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Create', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/cast.js') }}" type="module" defer></script>
<script src="{{ url('admin/js/chunks/castdetail.js') }}" type="module" defer></script>
@endsection