@extends('admin.layouts.lists', [
'page_title' => 'View Casts',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>route('admin.cast.create'), 'title'=>'Add Cast','icon'=>'icon-plus']
])

@section('title', 'View Casts')
@section('card_title', 'List All Cities')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.cast.list'), 'columns' => ['Sr. No.', 'Name', '' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/cast.js') }}" type="module" defer></script>
@endsection