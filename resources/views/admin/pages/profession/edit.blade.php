@extends('admin.layouts.form', [
'page_title' => 'Edit Profession',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.profession.index'), 'title'=>'View Professions','icon'=>'icon-file-text2']
])

@section('title', 'Edit Profession')

@section('card_title', 'Update Profession Info')

@section('form')
{!! Form::open(['method' => 'PUT', 'url' => route('admin.profession.update', $profession->id)]) !!}
@include('admin.pages.profession.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Update Details', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/profession.js') }}" type="module" defer></script>
@endsection