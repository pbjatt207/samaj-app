@extends('admin.layouts.lists', [
'page_title' => 'View Professions',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>route('admin.profession.create'), 'title'=>'Add Profession','icon'=>'icon-plus']
])

@section('title', 'View Professions')
@section('card_title', 'List All Professions')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.profession.list'), 'columns' => ['Sr. No.', 'Name', '' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/profession.js') }}" type="module" defer></script>
@endsection