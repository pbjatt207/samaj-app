@extends('admin.layouts.form', [
'page_title' => 'Add Profession',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.profession.index'), 'title'=>'View Professions','icon'=>'icon-file-text2']
])

@section('title', 'Add Profession')

@section('card_title', 'Create New Profession')

@section('form')
{!! Form::open(['url' => route('admin.profession.store')]) !!}
@include('admin.pages.profession.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Create', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/profession.js') }}" type="module" defer></script>
@endsection