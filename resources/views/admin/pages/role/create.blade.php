@extends('admin.layouts.form', [
'page_title' => 'Add Role',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.role.index'), 'title'=>'View Roles','icon'=>'icon-file-text2']
])

@section('title', 'Add Role')

@section('card_title', 'Create New Role')

@section('form')
{!! Form::open(['url' => route('admin.role.store')]) !!}
@include('admin.pages.role.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Create', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/role.js') }}" type="module" defer></script>
@endsection