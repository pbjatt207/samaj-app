@extends('admin.layouts.lists', [
'page_title' => 'View Roles',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>route('admin.role.create'), 'title'=>'Add Role','icon'=>'icon-plus']
])

@section('title', 'View Roles')
@section('card_title', 'List All Roles')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.role.list'), 'columns' => ['Sr. No.', 'Name', '' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/role.js') }}" type="module" defer></script>
@endsection