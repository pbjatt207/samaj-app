@extends('admin.layouts.lists', [
'page_title' => 'View Religions',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>route('admin.religion.create'), 'title'=>'Add Religion','icon'=>'icon-plus']
])

@section('title', 'View Religions')
@section('card_title', 'List All Religions')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.religion.list'), 'columns' => ['Sr. No.', 'Name', '' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/religion.js') }}" type="module" defer></script>
@endsection