@extends('admin.layouts.form', [
'page_title' => 'Edit Religion',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.religion.index'), 'title'=>'View Religions','icon'=>'icon-file-text2']
])

@section('title', 'Edit Religion')

@section('card_title', 'Update Religion Info')

@section('form')
{!! Form::open(['method' => 'PUT', 'url' => route('admin.religion.update', $religion->id)]) !!}
@include('admin.pages.religion.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Update Details', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/religion.js') }}" type="module" defer></script>
@endsection