@extends('admin.layouts.form', [
'page_title' => 'Add Religion',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.religion.index'), 'title'=>'View Religions','icon'=>'icon-file-text2']
])

@section('title', 'Add Religion')

@section('card_title', 'Create New Religion')

@section('form')
{!! Form::open(['url' => route('admin.religion.store')]) !!}
@include('admin.pages.religion.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Create', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/religion.js') }}" type="module" defer></script>
@endsection