@extends('admin.layouts.lists', [
'page_title' => 'View Prayers',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>'', 'title'=>'','icon'=>'']
])

@section('title', 'View Prayers')
@section('card_title', 'List All Prayers')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.prayer.list'), 'columns' => ['Sr. No.', 'Name','Image','Description' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/prayer.js') }}" type="module" defer></script>
@endsection