@extends('admin.layouts.form', [
'page_title' => 'Add Gotra',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.gotra.index'), 'title'=>'View Gotra','icon'=>'icon-file-text2']
])

@section('title', 'Add Gotra')

@section('card_title', 'Create New Gotra')

@section('form')
{!! Form::open(['url' => route('admin.gotra.store')]) !!}
@include('admin.pages.gotra.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Create', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/gotra.js') }}" type="module" defer></script>
<script src="{{ url('admin/js/chunks/castdetail.js') }}" type="module" defer></script>
@endsection