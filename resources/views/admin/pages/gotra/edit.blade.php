@extends('admin.layouts.form', [
'page_title' => 'Edit Gotra',
'b_items' => [],
'errors' => $errors,
'add_page' => ['link' =>route('admin.gotra.index'), 'title'=>'View Gotra','icon'=>'icon-file-text2']
])

@section('title', 'Edit Gotra')

@section('card_title', 'Update Gotra Info')

@section('form')
{!! Form::open(['method' => 'PUT', 'url' => route('admin.gotra.update', $gotra->id)]) !!}
@include('admin.pages.gotra.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Update Details', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/gotra.js') }}" type="module" defer></script>
@endsection