@extends('admin.layouts.lists', [
'page_title' => 'View Gotra',
'b_items' => [ ],
'errors' => $errors,
'add_page' => ['link' =>route('admin.gotra.create'), 'title'=>'Add Gotra','icon'=>'icon-plus']
])

@section('title', 'View Gotra')
@section('card_title', 'List All Gotra')

@section('card_table')
@include('admin.components.datatable', ['action' => route('api.gotra.list'), 'columns' => ['Sr. No.', 'Name', '' ]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/gotra.js') }}" type="module" defer></script>
@endsection