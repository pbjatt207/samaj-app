@extends('admin.layouts.master')

@section('content')

<div class="container-fluid">
    <h1>{{ $page_title }}</h1>
    @include('admin.components.breadcrumbs_li', compact('page_title', 'b_items'))

    <div class="card">
        <div class="card-header">
            @yield('card_title')
        </div>
        <div class="card-body">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ol>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ol>
            </div>
            @endif

            @yield('form')
        </div>
    </div>
</div>
@endsection