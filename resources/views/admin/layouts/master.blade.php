@if(Auth::guard('admin')->check())
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ url('admin/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ url('admin/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/agent/style.css') }}">
    <link rel="stylesheet" href="{{ url('admin/css/select2.css') }}">
    <link rel="stylesheet" href="{{ url('icomoon/style.css') }}">

    <!-- Datatables -->
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
</head>

<body class="bg-light">
    <div id="wrapper" class="d-flex vh-100">
        <div class='menu-sidebar text-white'>
            <div class="p-3 text-center">
                <strong>SAMAJ APP</strong>
            </div>
            <div class="text-center profile-img">
                <div>
                    <img src="{{ url('imgs/admin.jpg') }}" alt="" class="rounded-circle">
                </div>
                <div class="my-1">
                    {{ auth()->guard('admin')->user()->name }}
                </div>
            </div>
            <nav>
                <ul>
                    <li class="{{ \Str::is('admin.dashboard', request()->route()->getName()) ? 'active' : '' }}">
                        <a href="{{ route('admin.dashboard') }}">
                            <i class="icon-home nav-icon"></i>
                            <span class="nav-text">Dashboard</span>
                        </a>
                    </li>
                    @php
                    $current = request()->route()->getName();
                    $active_condition = \Str::is('admin.agent*', $current) ||
                    \Str::is('admin.supplier*', $current) ||
                    \Str::is('admin.account*', $current) ||
                    \Str::is('admin.item*', $current) ||
                    \Str::is('admin.purchase*', $current) ||
                    \Str::is('admin.sale*', $current) ||
                    \Str::is('admin.report*', $current)
                    @endphp
                    <li class="has-dropdown">
                        <a href="#">
                            <i class="icon-suitcase nav-icon"></i>
                            <span class="nav-text">Master</span>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ route('admin.country.index') }}">Country</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.state.index') }}">State</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.city.index') }}">City</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.language.index') }}">Language</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.religion.index') }}">Religion</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.cast.index') }}">Cast</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.gotra.index') }}">Gotra</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.profession.index') }}">Profession</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.meritalstatus.index') }}">Meritalstatus</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.role.index') }}">Role</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.feeling.index') }}">Feeling</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-dropdown">
                        <a href="#" id="site">
                            <i class="icon-stack nav-icon"></i>
                            <span class="nav-text">Sites</span>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ route('admin.site.create') }}">Add Site</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.site.index') }}">List Sites</a>
                            </li>
                        </ul>
                    </li>
                    <!-- <li class="has-dropdown">
                        <a href="#">
                            <i class="icon-clipboard2 nav-icon"></i>
                            <span class="nav-text">Report</span>
                        </a>
                        <ul>
                            <li>
                                <a href="#">Transfer Report</a>
                            </li>
                            <li>
                                <a href="#">Daily Report</a>
                            </li>
                            <li>
                                <a href="#">Monthly Report</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-dropdown">
                        <a href="#">
                            <i class="icon-live_help nav-icon"></i>
                            <span class="nav-text">Guidelines</span>
                        </a>
                        <ul>
                            <li>
                                <a href="#">Money Transfer</a>
                            </li>
                            <li>
                                <a href="#">Wallet</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-dropdown">
                        <a href="#">
                            <i class="icon-headphones2 nav-icon"></i>
                            <span class="nav-text">Support</span>
                        </a>
                        <ul>
                            <li>
                                <a href="#">Country</a>
                            </li>
                            <li>
                                <a href="#">State</a>
                            </li>
                            <li>
                                <a href="#">City</a>
                            </li>
                        </ul>
                    </li> -->
                    <li class="">
                        <a href="{{ route('admin.user.index')}}">
                            <i class="icon-user nav-icon"></i>
                            <span class="nav-text">Users</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('admin.post.index')}}">
                            <i class="icon-credit-card nav-icon"></i>
                            <span class="nav-text">Posts</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('admin.place.index')}}">
                            <i class="icon-office nav-icon"></i>
                            <span class="nav-text">Places</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('admin.event.index')}}">
                            <i class="icon-pacman nav-icon"></i>
                            <span class="nav-text">Events</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('admin.tag.index')}}">
                            <i class="icon-price-tags nav-icon"></i>
                            <span class="nav-text">Tags</span>
                        </a>
                    </li>
                    <li class="{{ \Str::is('admin.change_password.*', request()->route()->getName()) ? 'active' : '' }}">
                        <a href="{{ route('admin.adminchange.password')}}">
                            <i class="icon-account_balance_wallet nav-icon"></i>
                            <span class="nav-text">Change Password</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class='flex-fill'>
            <div class="panel-head">
                <!-- <a href="{{ route('admin.dashboard') }}"><i class="icon-bell-o"></i></a> -->
                <a href="{{ route('admin.logout') }}"><i class="icon-logout"></i></a>
            </div>
            <div class="container-fluid py-4">
                @yield('content')
            </div>
        </div>
    </div>

    <script src="{{ url('admin/js/select2.min.js') }}" defer></script>
    <script src="{{ url('admin/js/jquery-3.3.1.min.js') }}" defer></script>
    <script src="{{ url('admin/js/select2.js') }}" defer></script>
    <script src="{{ url('js/sweetalert.min.js') }}" type="module" defer></script>
    <script src="{{ url('js/agent/script.js') }}" type="module" defer></script>

    <script>
        $(document).ready(function() {
            $('.multiple-select').select2();
        });
    </script>
    @yield('footer_script')
</body>

</html>
@else
@include('admin.login')
@endif