@extends('admin.layouts.master')

@section('content')
{!! Form::open(['method' => 'DELETE', 'id' => 'deleteForm']) !!}
{!! Form::hidden('action', 'delete') !!}
{!! Form::close() !!}
<div class="container-fluid">
    <h1>{{ $page_title }}</h1>
    @include('admin.components.breadcrumbs_li', compact('page_title', 'b_items'))

    <div class="card">
        <div class="card-header">
            @yield('card_title')
        </div>
        <div class="card-body">
            @if (session()->has('success'))
            <div class="alert alert-success">
                {!! session()->get('success')!!}
            </div>
            @endif
            <div class="table-responsive">
                @yield('card_table')
            </div>
            @yield('pagination')
        </div>
    </div>
</div>
@endsection
@section('footer_script')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" defer></script>
@yield('extra_script')
@endsection