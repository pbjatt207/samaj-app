<div class="row breadcrumb">
    <div class="col-lg-8">
        <nav aria-label="breadcrumb p-0 m-0">
            <ol class="breadcrumb p-0 m-0">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="icon-home"></i> Dashboard</a>
                </li>
                @if(!empty($b_items) && is_array($b_items))
                @foreach($b_items as $link => $item)
                <li class="breadcrumb-item"><a href="{{ $link }}">{{ $item }}</a></li>
                @endforeach
                @endif
                <li class="breadcrumb-item active" aria-current="page">{{ $page_title }}</li>
            </ol>
        </nav>
    </div>
    <div class="col-lg-4">
        <div class="breadcrumb p-0 m-0 auto-ml float-right">
            <a href="{{ $add_page['link'] }}"> <i class="{{ $add_page['icon'] }}"></i> {{ $add_page['title'] }}</a>
        </div>
    </div>
</div>