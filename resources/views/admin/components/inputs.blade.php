@foreach($fields as $field)
<div class="form-group row">
    {!! $field['label'] !!}
    <div class="col-sm-10">
        {{ $field['input'] }}
    </div>
</div>
@endforeach