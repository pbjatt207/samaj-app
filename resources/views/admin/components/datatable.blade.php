@if(!empty($columns))
<table class="table table-sm table-bordered table-striped data-table" data-ajax_url="{{ $action }}">
    <thead>
        <tr>
            @foreach($columns as $col)
            <th>{!! $col !!}</th>
            @endforeach
        </tr>
    </thead>
    <tbody> </tbody>
</table>
@endif