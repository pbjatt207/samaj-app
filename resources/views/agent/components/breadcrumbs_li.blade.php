<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="icon-home"></i> Dashboard</a>
        </li>
        @if(!empty($b_items) && is_array($b_items))
        @foreach($b_items as $link => $item)
        <li class="breadcrumb-item"><a href="{{ $link }}">{{ $item }}</a></li>
        @endforeach
        @endif
        <li class="breadcrumb-item active" aria-current="page">{{ $page_title }}</li>
    </ol>
</nav>