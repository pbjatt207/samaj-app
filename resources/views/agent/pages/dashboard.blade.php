@extends('agent.layouts.master')

@section('title', 'Dasboard | Agent Panel')

@section('content')

<div class="container-fluid">
    <div class="row dash-block-group">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h3>Wallet</h3>
                    <div>₹ 697.74</div>
                    <div class="progress my-3">
                        <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div>
                        <i class="icon-arrow-up"></i> 10% Since last month
                    </div>
                    <img src="imgs/wallet.png" class="dash-img">
                </div>
                <a href="#" class="btn btn-block btn-dark">View Details <i class="icon-long-arrow-right"></i></a>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h3>Earning</h3>
                    <div>₹ Comming Soon</div>
                    <div class="progress my-3">
                        <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div>
                        <i class="icon-arrow-up"></i> 10% Since last month
                    </div>
                    <img src="imgs/earning.png" class="dash-img">
                </div>
                <a href="#" class="btn btn-block btn-dark">View Details <i class="icon-long-arrow-right"></i></a>
            </div>
        </div>
        <!--<div class="col-sm-4">-->
        <!--    <div class="card">-->
        <!--        <div class="card-body">-->
        <!--            <h3>AePS Wallet</h3>-->
        <!--            <div>₹ 100.00</div>-->
        <!--            <div class="progress my-3">-->
        <!--                <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>-->
        <!--            </div>-->
        <!--            <div>-->
        <!--                ₹ 0.00 Today AePS withdrawal-->
        <!--            </div>-->
        <!--            <img src="imgs/wallet2.png" class="dash-img">-->
        <!--        </div>-->
        <!--        <a href="#" class="btn btn-block btn-dark">View Details <i class="icon-long-arrow-right"></i></a>-->
        <!--    </div>-->
        <!--</div>-->
    </div>
    <div class="row mt-5">
        <div class="col-sm-12">
            <div class="card card-danger">
                <div class="card-header">
                    <a href="#" class="btn btn-primary btn-sm float-right">View All <i class="icon-angle-right"></i></a>
                    Service
                </div>
                <div class="card-body">
                    <div class="row dash-table">
                        <div class="col-4">
                            <div class="text-center box-link">
                                <div>Electricity Bill Pay</div>
                                <div class="my-2"><img src="imgs/002-idea.svg" alt="" class="img-circle"></div>
                                <div class="subheading">Electricity Bill Pay</div>
                                <a href="#"></a>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="text-center box-link">
                                <div>Water Bill Pay</div>
                                <div class="my-2"><img src="imgs/001-money.svg" alt="" class="img-circle"></div>
                                <div class="subheading">Water Bill Pay </div>
                                <a href="#"></a>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="text-center box-link">
                                <div>Utility Recharge</div>
                                <div class="my-2"><img src="imgs/003-fingerprint.svg" alt="" class="img-circle"></div>
                                <div class="subheading">Utility Recharge </div>
                                <a href="#"></a>
                            </div>
                        </div>
                        
                    </div>
                    <!--<hr>-->
                    <!--<div class="row dash-table">-->
                    <!--    <div class="col-4">-->
                    <!--        <div class="text-center box-link">-->
                    <!--            <div>Insurance Premium</div>-->
                    <!--            <div class="my-2"><img src="imgs/005-family.svg" alt="" class="img-circle"></div>-->
                    <!--            <div class="subheading">Pay Premium</div>-->
                    <!--            <a href="#"></a>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--    <div class="col-4">-->
                    <!--        <div class="text-center box-link">-->
                    <!--            <div>Apply PAN</div>-->
                    <!--            <div class="my-2"><img src="imgs/credit-card.svg" alt="" class="img-circle"></div>-->
                    <!--            <div class="subheading">New/Correction</div>-->
                    <!--            <a href="#"></a>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--    <div class="col-4">-->
                    <!--        <div class="text-center box-link">-->
                    <!--            <div>TAX-ITR</div>-->
                    <!--            <div class="my-2"><img src="imgs/004-calculation.svg" alt="" class="img-circle"></div>-->
                    <!--            <div class="subheading">Apply Tax Return</div>-->
                    <!--            <a href="#"></a>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</div>-->
                </div>
            </div>
        </div>
        <!--<div class="col-sm-6">-->
        <!--    <div class="card">-->
        <!--        <div class="card-header">-->
        <!--            <a href="#" class="btn btn-primary btn-sm float-right">View All <i class="icon-angle-right"></i></a>-->
        <!--            Fund Control-->
        <!--        </div>-->
        <!--        <div class="card-body">-->
        <!--            <div class="row dash-table">-->
        <!--                <div class="col-4">-->
        <!--                    <div class="text-center box-link">-->
        <!--                        <div>Add Money</div>-->
        <!--                        <div class="my-2"><img src="imgs/002-add.svg" alt="" class="img-circle"></div>-->
        <!--                        <div class="subheading">Wallet Refill</div>-->
        <!--                        <a href="#"></a>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--                <div class="col-4">-->
        <!--                    <div class="text-center box-link">-->
        <!--                        <div>Send Money</div>-->
        <!--                        <div class="my-2"><img src="imgs/003-direct.svg" alt="" class="img-circle"></div>-->
        <!--                        <div class="subheading">Transfer</div>-->
        <!--                        <a href="#"></a>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--                <div class="col-4">-->
        <!--                    <div class="text-center box-link">-->
        <!--                        <div>AePS</div>-->
        <!--                        <div class="my-2"><img src="imgs/005-gear.svg" alt="" class="img-circle"></div>-->
        <!--                        <div class="subheading">Bank Transfer</div>-->
        <!--                        <a href="#"></a>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--            <hr>-->
        <!--            <div class="row dash-table">-->
        <!--                <div class="col-4">-->
        <!--                    <div class="text-center box-link">-->
        <!--                        <div>Gift Coupon</div>-->
        <!--                        <div class="my-2"><img src="imgs/001-gift-box.svg" alt="" class="img-circle"></div>-->
        <!--                        <div class="subheading">Offer/Coupon</div>-->
        <!--                        <a href="#"></a>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--                <div class="col-4">-->
        <!--                    <div class="text-center box-link">-->
        <!--                        <div>Setting</div>-->
        <!--                        <div class="my-2"><img src="imgs/004-bank.svg" alt="" class="img-circle"></div>-->
        <!--                        <div class="subheading">Bank Info</div>-->
        <!--                        <a href="#"></a>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--                <div class="col-4">-->
        <!--                    <div class="text-center box-link">-->
        <!--                        <div>Report</div>-->
        <!--                        <div class="my-2"><img src="imgs/006-bank-statement.svg" alt="" class="img-circle"></div>-->
        <!--                        <div class="subheading">Transaction</div>-->
        <!--                        <a href="#"></a>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->
    </div>
</div>

@endsection