@extends('agent.layouts.master')

@section('title', 'Change Password | Agent Panel')

@section('content')

<div class="container-fluid">
    <section
        class="d-flex align-self-center vh-100"
    >
        
        <div
            class="LoginBox"
        >
            <div
                class="card"
            >
                <div
                    class="card-body"
                >
                    <div
                        class="mb-3 text-center"
                    >
                        <!--<img src="{{ url('imgs/logo.png') }}" alt="SN Power Tech" class="mw-100">-->
                    </div>
                    <div id="divSuccessMsg"></div>
                    <form 
                        action="{{ route('agent.agentchange.password') }}" 
                        method="post"
                        id="ChangePassword"
                        class="ajax-form"
                    >
                        @csrf
                        <div
                            class="form-group"
                        >
                            <label for="mobile_no">Mobile</label>
                            <input 
                                type="tel"
                                minlength="10"
                                maxlength="10"
                                class="form-control"
                                placeholder="Enter Your Mobile Number"
                                name="mobile"
                                id="mobile_no"
                                value='{{ auth()->guard('agent')->user()->mobile }}'
                                
                                required
                                disabled
                            >
                            <input 
                                type="hidden"
                                minlength="10"
                                maxlength="10"
                                class="form-control"
                                placeholder="Enter Your Mobile Number"
                                name="mobile"
                                id="mobile_no"
                                value='{{ auth()->guard('agent')->user()->mobile }}'
                                
                                required
                            >
                            
                        </div>
                        <button
                            type="submit"
                            class="btn btn-primary btn-block"
                        >Send OTP</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection