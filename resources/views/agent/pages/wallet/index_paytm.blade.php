@extends('agent.layouts.master')

@section('title', 'Wallet | Agent Panel')

@section('content')

@foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has($msg))
        <div class="alert alert-{{ $msg }}">{{ Session::get($msg) }}</div>
    @endif
@endforeach

<div
    class="row dash-block"
>
    <div
        class="col-sm-4 col-lg-3"
    >
        <div
            class="card"
        >
            <div class="card-header">
                Add Amount
            </div>
            <div class="card-body">
                <form action="{{ url('agent-panel/payment') }}" class="form-image-upload" method="POST" enctype="multipart/form-data">


                {!! csrf_field() !!}
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                
                    <!--<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />-->
                    <!--<input type="hidden" name="udf5" id="udf5" value="BOLT_KIT_PHP7">-->
                    <!--<input type="hidden" name="surl" id="surl" value="{{ route('agent.wallet.store') }}">-->

                    <!--<input type="hidden" name="key" id="key" value="{{ $key }}">-->
                    <!--<input type="hidden" name="salt" id="salt" value="{{ $salt1 }}">-->

                    <!--<input type="hidden" name="txnid" id="txnid" value="{{ 'TXN_' . uniqid() }}">-->
                    
                    <!--<input type="hidden" name="productinfo" id="pinfo" value="p01">-->

                    <!--<input type="hidden" id="hash" name="hash" placeholder="Hash" value="" required />-->

                    <div
                        class="form-group"
                    >
                        
                        <label for="fname">Name</label>
                        <input 
                            type="text"
                            name="firstname"
                            id="fname"
                            value="{{ auth()->guard('agent')->user()->name }}"
                            min="1"
                            class="form-control"
                            required
                            readonly
                        >
                    </div>
                    <div
                        class="form-group"
                    >
                        <label for="amount">Email</label>
                        <input 
                            type="text"
                            name="email"
                            id="email"
                            value="{{ auth()->guard('agent')->user()->email }}"
                            min="1"
                            class="form-control"
                            required
                            readonly
                        >
                    </div>
                    <div
                        class="form-group"
                    >
                        <label for="amount">Mobile No.</label>
                        <input 
                            type="text"
                            name="phone"
                            id="mobile"
                            value="9660257608"
                            min="1"
                            class="form-control"
                            required
                            readonly
                        >
                    </div>
                    <div
                        class="form-group"
                    >
                        <label for="amount">Amount</label>
                        <input 
                            type="number"
                            name="amount"
                            id="amount"
                            placeholder="Enter Amount"
                            min="1"
                            class="form-control"
                            required
                        >
                    </div>
                    <button
                        type="submit"
                        class="btn btn-primary btn-block"
                        
                        id="submit_btn"
                        
                    >Pay Now</button>
                    <!-- <button
                        type="submit"
                        class="btn btn-primary btn-block"
                        id="submit_btn"
                        disabled
                    >Pay Now</button> -->
                </form>
            </div>
        </div>
    </div>
    <div
        class="col-sm-8 col-lg-9"
    >
        <div
            class="card"
        >
            <div class="card-header">
                <span class="float-right">(₹{{ number_format($total_amount, 2) }})</span>
                Wallet Summary
            </div>
            <div class="card-body">
                @if($wallets->isEmpty())
                    No record(s) found.
                @else
                <div
                    class="table-responsive"
                >
                    <table
                        class="table table-bordered table-striped table-hover"
                    >
                        <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Date</th>
                                <th>Remarks</th>
                                <th>Credit</th>
                                <th>Debit</th>
                                <th>Balance</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $balance = 0; @endphp
                            @foreach($wallets as $index => $w)
                                @php 
                                    if($w->type == 'credit') $balance += $w->amount;
                                    if($w->type == 'debit') $balance -= $w->amount;
                                @endphp
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ date('d M Y', strtotime($w->created_at)) }}</td>
                                <td>
                                    <div>{{ $w->remarks }}</div>
                                    <div><strong>Txn ID: </strong>{{ $w->txn_number }}</div>
                                </td>
                                <td>₹{{ $w->type == 'credit' ? $w->amount : 0 }}</td>
                                <td>₹{{ $w->type == 'debit' ? $w->amount : 0 }}</td>
                                <td>₹{{ $balance }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>
    @foreach($links as $link)
    <!-- <div
        class="col-lg-2 col-sm-3 col-6"
    >
        <div
            class="card"
        >
            <div
                class="card-body"
            >
                <h4 class="card-title">{{ $link['title'] }}</h4>
                <a href="{{ route($link['name']) }}"></a>
            </div>
        </div>
    </div> -->
    @endforeach
</div>
@endsection

@section('footer_script')

@endsection