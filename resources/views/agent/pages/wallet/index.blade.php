@extends('agent.layouts.master')

@section('title', 'Wallet | Agent Panel')

@section('content')

@foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has($msg))
        <div class="alert alert-{{ $msg }}">{{ Session::get($msg) }}</div>
    @endif
@endforeach

<div
    class="row dash-block"
>
    <div
        class="col-sm-4 col-lg-3"
    >
        <div
            class="card"
        >
            <div class="card-header">
                Add Amount
            </div>
            <div class="card-body">
                <form action="#" method="post" id="payment_form">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <input type="hidden" name="udf5" id="udf5" value="BOLT_KIT_PHP7">
                    <input type="hidden" name="surl" id="surl" value="{{ route('agent.wallet.store') }}">

                    <input type="hidden" name="key" id="key" value="{{ $key }}">
                    <input type="hidden" name="salt" id="salt" value="{{ $salt1 }}">

                    <input type="hidden" name="txnid" id="txnid" value="{{ 'TXN_' . uniqid() }}">
                    
                    <input type="hidden" name="productinfo" id="pinfo" value="p01">

                    <input type="hidden" id="hash" name="hash" placeholder="Hash" value="" required />

                    <div
                        class="form-group"
                    >
                        <label for="fname">Name</label>
                        <input 
                            type="text"
                            name="firstname"
                            id="fname"
                            value="{{ auth()->guard('agent')->user()->name }}"
                            min="1"
                            class="form-control"
                            required
                            readonly
                        >
                    </div>
                    <div
                        class="form-group"
                    >
                        <label for="amount">Email</label>
                        <input 
                            type="text"
                            name="email"
                            id="email"
                            value="{{ auth()->guard('agent')->user()->email }}"
                            min="1"
                            class="form-control"
                            required
                            readonly
                        >
                    </div>
                    <div
                        class="form-group"
                    >
                        <label for="amount">Mobile No.</label>
                        <input 
                            type="text"
                            name="phone"
                            id="mobile"
                            value="{{ auth()->guard('agent')->user()->mobile }}"
                            min="1"
                            class="form-control"
                            required
                            readonly
                        >
                    </div>
                    <div
                        class="form-group"
                    >
                        <label for="amount">Amount</label>
                        <input 
                            type="number"
                            name="amount"
                            id="amount"
                            placeholder="Enter Amount"
                            min="1"
                            class="form-control"
                            required
                        >
                    </div>
                    <button
                        type="submit"
                        class="btn btn-primary btn-block"
                        onclick="onScriptLoad(); return false;"
                        id="submit_btn"
                        disabled
                    >Pay Now</button>
                    <!-- <button
                        type="submit"
                        class="btn btn-primary btn-block"
                        id="submit_btn"
                        disabled
                    >Pay Now</button> -->
                </form>
            </div>
        </div>
    </div>
    <div
        class="col-sm-8 col-lg-9"
    >
        <div
            class="card"
        >
            <div class="card-header">
                <span class="float-right">(₹{{ number_format($total_amount, 2) }})</span>
                Wallet Summary
            </div>
            <div class="card-body">
                @if($wallets->isEmpty())
                    No record(s) found.
                @else
                <div
                    class="table-responsive"
                >
                    <table
                        class="table table-bordered table-striped table-hover"
                    >
                        <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Date</th>
                                <th>Remarks</th>
                                <th>Credit</th>
                                <th>Debit</th>
                                <th>Balance</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $balance = 0; @endphp
                            @foreach($wallets as $index => $w)
                                @php 
                                    if($w->type == 'credit') $balance += $w->amount;
                                    if($w->type == 'debit') $balance -= $w->amount;
                                @endphp
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ date('d M Y', strtotime($w->created_at)) }}</td>
                                <td>
                                    <div>{{ $w->remarks }}</div>
                                    <div><strong>Txn ID: </strong>{{ $w->txn_number }}</div>
                                </td>
                                <td>₹{{ $w->type == 'credit' ? $w->amount : 0 }}</td>
                                <td>₹{{ $w->type == 'debit' ? $w->amount : 0 }}</td>
                                <td>₹{{ $balance }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>
    @foreach($links as $link)
    <!-- <div
        class="col-lg-2 col-sm-3 col-6"
    >
        <div
            class="card"
        >
            <div
                class="card-body"
            >
                <h4 class="card-title">{{ $link['title'] }}</h4>
                <a href="{{ route($link['name']) }}"></a>
            </div>
        </div>
    </div> -->
    @endforeach
</div>
@endsection

@section('footer_script')
<script 
    id="bolt"
    src="https://checkout-static.citruspay.com/bolt/run/bolt.min.js"
    bolt-color="e34524"
    bolt-logo="{{ url('imgs/logo.png') }}"
></script>
<script type="text/javascript">
$('#payment_form').bind('keyup blur', function() {
    $('#submit_btn').attr('disabled', 'disabled');
    
	$.ajax({
          url: '{{ route("agent.wallet.create_key") }}',
          type: 'post',
          data: JSON.stringify({ 
            _token: $(this).find('[name=_token]').val(),
            key: $('#key').val(),
			salt: $('#salt').val(),
			txnid: $('#txnid').val(),
			amount: $('#amount').val(),
		    pinfo: $('#pinfo').val(),
            fname: $('#fname').val(),
			email: $('#email').val(),
			mobile: $('#mobile').val(),
			udf5: $('#udf5').val()
          }),
		  contentType: "application/json",
          dataType: 'json',
          success: function(json) {
            if (json['error']) {
			 $('#alertinfo').html('<i class="fa fa-info-circle"></i>'+json['error']);
            }
			else if (json['success']) {	
				$('#hash').val(json['success']);
                $('#submit_btn').removeAttr('disabled');
            }
          }
        }); 
});
</script>
<script type="text/javascript">
function launchBOLT()
{
	bolt.launch({
	key: $('#key').val(),
	txnid: $('#txnid').val(), 
	hash: $('#hash').val(),
	amount: $('#amount').val(),
	firstname: $('#fname').val(),
	email: $('#email').val(),
	phone: $('#mobile').val(),
	productinfo: $('#pinfo').val(),
	udf5: $('#udf5').val(),
	surl : $('#surl').val(),
	furl: $('#surl').val(),
	mode: 'dropout'	
},{ responseHandler: function(BOLT){
	console.log( BOLT.response.txnStatus );		
	if(BOLT.response.txnStatus != 'CANCEL')
	{
		//Salt is passd here for demo purpose only. For practical use keep salt at server side only.
		var fr = '<form action=\"'+$('#surl').val()+'\" method=\"post\">' +
		'<input type=\"hidden\" name=\"key\" value=\"'+BOLT.response.key+'\" />' +
		'<input type=\"hidden\" name=\"salt\" value=\"'+$('#salt').val()+'\" />' +
		'<input type=\"hidden\" name=\"txnid\" value=\"'+BOLT.response.txnid+'\" />' +
		'<input type=\"hidden\" name=\"amount\" value=\"'+BOLT.response.amount+'\" />' +
		'<input type=\"hidden\" name=\"productinfo\" value=\"'+BOLT.response.productinfo+'\" />' +
		'<input type=\"hidden\" name=\"firstname\" value=\"'+BOLT.response.firstname+'\" />' +
		'<input type=\"hidden\" name=\"email\" value=\"'+BOLT.response.email+'\" />' +
		'<input type=\"hidden\" name=\"udf5\" value=\"'+BOLT.response.udf5+'\" />' +
		'<input type=\"hidden\" name=\"mihpayid\" value=\"'+BOLT.response.mihpayid+'\" />' +
		'<input type=\"hidden\" name=\"status\" value=\"'+BOLT.response.status+'\" />' +
		'<input type=\"hidden\" name=\"hash\" value=\"'+BOLT.response.hash+'\" />' +
		'</form>';
		var form = jQuery(fr);
		jQuery('body').append(form);								
		form.submit();
	}
},
	catchException: function(BOLT){
 		alert( BOLT.message );
	}
});
}
</script>
<script type="application/html" crossorigin="anonymous" src="{HOST}/merchantpgpui/checkoutjs/merchants/{MID}.js" onload="onScriptLoad();"> </script>
<script>
  function onScriptLoad(){
      var config = {
        "root": "",
        "flow": "DEFAULT",
        "data": {
        "orderId": $('#txnid').val(), /* update order id */
        "token": "", /* update token value */
        "tokenType": "TXN_TOKEN",
        "amount": $('#amount').val() /* update amount */
        },
        "handler": {
          "notifyMerchant": function(eventName,data){
            console.log("notifyMerchant handler function called");
            console.log("eventName => ",eventName);
            console.log("data => ",data);
          } 
        }
      };

      if(window.Paytm && window.Paytm.CheckoutJS){
          window.Paytm.CheckoutJS.onLoad(function excecuteAfterCompleteLoad() {
              // initialze configuration using init method 
              window.Paytm.CheckoutJS.init(config).then(function onSuccess() {
                  // after successfully updating configuration, invoke JS Checkout
                  window.Paytm.CheckoutJS.invoke();
              }).catch(function onError(error){
                  console.log("error => ",error);
              });
          });
      } 
  }
</script>
@endsection