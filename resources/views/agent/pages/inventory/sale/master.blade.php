@extends('admin.layouts.master')

@section('title', 'Item Master | Inventory')

@section('content')
<div class="container-fluid">
    <div class="row dash-block-group">
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <div class="master-icon">
                        <i class="icon-person"></i>
                    </div>
                    <h3>View Items</h3>
                </div>
                <a href="{{ route('admin.item.index') }}" class="btn btn-block btn-dark">
                    Enter <i class="icon-long-arrow-right"></i>
                </a>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <div class="master-icon">
                        <i class="icon-person_add"></i>
                    </div>
                    <h3>Add Item</h3>
                </div>
                <a href="{{ route('admin.item.create') }}" class="btn btn-block btn-dark">
                    Enter <i class="icon-long-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>
@endsection