@include('admin.components.inputs', [
'fields' => [
[
'label' => Form::label('name', 'Customer Name *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Enter Name', 'required' =>
'required'])
],
[
'label' => Form::label('mobile', 'Customer Mobile No. *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::tel('mobile', '', ['class' => 'form-control', 'placeholder' => 'Enter Mobile No.',
'required' =>
'required'])
],
[
'label' => Form::label('address', 'Customer Address *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::textarea('address', '', ['class' => 'form-control', 'rows' => 8, 'placeholder' => 'Enter Address',
'required' => 'required'])
],
[
'label' => Form::label('item_id', 'Item *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::select('item_id', $items, null, [
'class' => 'form-control select2',
'placeholder' => 'Select Item',
'required' => 'required'
])
],
[
'label' => Form::label('qty', 'Quantity *', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::number('qty', 1, [ 'class' => 'form-control', 'min' => 1, 'placeholder' => 'Enter Quantity',
'required'
=> 'required' ])
],
[
'label' => Form::label('remarks', 'Remarks', ['class' => 'col-sm-2 col-form-label']),
'input' => Form::textarea('remarks', '', [
'class' => 'form-control',
'rows' => 8,
'placeholder' => 'Write any remarks' ])
]
]
])