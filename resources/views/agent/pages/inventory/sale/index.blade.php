@extends('agent.layouts.lists', [
'page_title' => 'View Sales',
'b_items' => [],
'errors' => $errors
])

@section('title', 'View Sales')
@section('card_title', 'List All Sales')

@section('card_table')
@include('agent.components.datatable', [
'action' => route('api.agent-sale.index'),
'columns' => [
'Sr. No.',
'Item Name',
'Qty',
'Name',
'Mobile No.',
'Address',
'Remarks',
''
]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/agent_sale.js') }}" type="module" defer></script>
@endsection