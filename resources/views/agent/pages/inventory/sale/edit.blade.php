@extends('agent.layouts.form', [
'page_title' => 'POS',
'b_items' => [],
'errors' => $errors
])

@section('title', 'POS')

@section('card_title', 'Update Sale')

@section('form')
{!! Form::open(['method' => 'PUT', 'url' => route('agent.pos.update', [$agentSale->id]), 'files' => true]) !!}
@include('agent.pages.inventory.sale.form')
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        {{ Form::submit('Update', ['class' => 'btn btn-primary rounded px-5']) }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('footer_script')
<script src="{{ url('admin/js/chunks/item.js') }}" type="module" defer></script>
@endsection