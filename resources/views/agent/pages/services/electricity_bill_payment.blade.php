@extends('agent.layouts.lists', [
'page_title' => 'Electricity Bill Pay',
'b_items' => [],
'errors' => $errors
])

@section('title', 'Electricity Bill Pay')
@section('abcd')
    <div class="container-fluid">
    <div class="container-fluid">
    <div class="row dash-block-group">
        
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body" style="background: linear-gradient(90deg, #e67e22, #f39c12);">
                    <p><span style="font-size:20px" class="text-white">Electricity Bill Pay</span><span style="font-size:14px;color:#000"> (बिजली बिल जमा करने का समय सुबह 7:00 से शाम 6:00  बजे तक रहेगा। रविवार को अवकाश रहेगा)(Service time 7:00AM to 6:00PM Only and Except Sunday Govt Holiday)</span>
                    <hr>
                    <div class="row dash-table">
                        <div class="col-4">
                            <div class="text-center box-link">
                                <div style="font-size:19px;font-weight:bold" >Electricity Bill Pay</div>
                                <div class="my-2"><img src="{{url('imgs/002-idea.svg')}}" alt="" class="img-circle"></div>
                                <div class="subheading text-white">Electricity Bill Pay</div>
                                <a href="#"></a>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="text-center box-link">
                                <div style="font-size:19px;font-weight:bold" >Water Bill Pay</div>
                                <div class="my-2"><img src="{{ url('imgs/001-money.svg')}}" alt="" class="img-circle"></div>
                                <div class="subheading text-white">Water Bill Pay </div>
                                <a href="#"></a>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="text-center box-link">
                                <div style="font-size:19px;font-weight:bold" >Utility Recharge</div>
                                <div class="my-2"><img src="{{ url('imgs/003-fingerprint.svg')}}" alt="" class="img-circle"></div>
                                <div class="subheading text-white">Utility Recharge </div>
                                <a href="#"></a>
                            </div>
                        </div>
                        
                    </div>
                    <hr>
                    {{ Form::open() }}
                    <div class="row">
                        <div class="col-3">
                            <select class="form-control" required="required" id="item_id" name="item_id" tabindex="-1" aria-hidden="true">
                                <option selected="selected" value="">Select Item</option>
                                <option value="1">Cap</option>
                            </select>
                        </div>
                        <div class="col-3">
                            <select class="form-control" required="required" id="item_id" name="item_id" tabindex="-1" aria-hidden="true">
                                <option selected="selected" value="">Select Board</option>
                                <option value="1">Cap</option>
                            </select>
                        </div>
                        <div class="col-3">
                            <input class="form-control" placeholder="K. Number" required="required" name="k_number" type="number" value="" id="k_number">
                        </div>
                        <div class="col-3">
                            <input class="form-control btn btn-primary rounded px-5" required="required" name="" type="submit" value="Search" id="">
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
                <img src="{{ url('imgs/earning.png')}}" class="dash-img">
            </div>
            
        </div>
    </div>
@endsection
@section('card_title', 'List Electricity Bill Pay')
@section('card_right_header')
<span class="float-right">
    <a href="" class="btn btn-secondary rounded px-5">Copy</a>
    <a href="" class="btn btn-info rounded px-5">CSV</a>
    <a href="" class="btn btn-success rounded px-5">Excel</a>
    <a href="" class="btn btn-danger rounded px-5">PDF</a>
    <a href="" class="btn btn-primary rounded px-5">Print</a>
    
</span>
@endsection
@section('card_table')

@include('agent.components.datatable', [
'action' => '',
'columns' => [
'Sr. No.',
'Tnx Date',
'Consumer Name',
'Amount',
'K.No.',
'DRK Ref. No.',
'Status',
''
]])

@endsection

@section('extra_script')
<script src="{{ url('admin/js/chunks/electricity_bill_pay.js') }}" type="module" defer></script>
@endsection