@if(Auth::guard('agent')->check())
@php
$total_credit = \App\Model\Wallet::where('agent_id', auth()->guard('agent')->id())->where('type',
'credit')->sum('amount');
$total_debit = \App\Model\Wallet::where('agent_id', auth()->guard('agent')->id())->where('type',
'debit')->sum('amount');
$total_amount = $total_credit - $total_debit;
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ url('admin/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/agent/style.css') }}">
    <link rel="stylesheet" href="{{ url('icomoon/style.css') }}">
    <link rel="stylesheet" href="{{ url('admin/css/select2.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Datatables -->
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">

</head>

<body class="bg-light">
    <div id="wrapper" class="d-flex vh-100">
        <div class='menu-sidebar text-white'>
            <div class="p-3">
                <strong>SN POWERTECH</strong>
            </div>
            <div class="text-center profile-img">
                <div>
                    <img src="{{ url('imgs/admin.jpg') }}" alt="" class="rounded-circle">
                </div>
                <div class="my-1">
                    {{ auth()->guard('agent')->user()->name }}
                </div>
            </div>
            <nav>
                <ul>
                    <li class="{{ \Str::is('agent.dashboard', request()->route()->getName()) ? 'active' : '' }}">
                        <a href="{{ route('agent.dashboard') }}">
                            <i class="icon-home nav-icon"></i>
                            <span class="nav-text">Dashboard</span>
                        </a>
                    </li>
                    <li class="{{ \Str::is('agent.wallet.*', request()->route()->getName()) ? 'active' : '' }}">
                        <a href="{{ route('agent.wallet.index') }}">
                            <i class="icon-account_balance_wallet nav-icon"></i>
                            <span class="nav-text">Wallet</span>
                        </a>
                    </li>
                    <li class="has-dropdown {{ \Str::is('agent.service.*', request()->route()->getName()) ? 'active open' : '' }}">
                        <a href="#">
                            <i class="icon-cart nav-icon"></i>
                            <span class="nav-text">Services</span>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ route('agent.service.water') }}">Water Bill Payment</a>
                            </li>
                            <li>
                                <a href="{{ route('agent.service.electricity') }}">Electricity Bill Payment</a>
                            </li>
                            <li>
                                <a href="{{ route('agent.service.utility') }}">Utility Bill Payment</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-dropdown {{ \Str::is('agent.pos.*', request()->route()->getName()) ? 'active open' : '' }}">
                        <a href="#">
                            <i class="icon-cart nav-icon"></i>
                            <span class="nav-text">POS</span>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ route('agent.pos.index') }}">List</a>
                            </li>
                            <li>
                                <a href="{{ route('agent.pos.create') }}">Create</a>
                            </li>
                        </ul>
                    </li>
                    <li class="{{ \Str::is('agent.agentchange_password.*', request()->route()->getName()) ? 'active' : '' }}">
                        <a href="{{ url('/agent-panel/change_password')}}">
                            <i class="icon-account_balance_wallet nav-icon"></i>
                            <span class="nav-text">Change Password</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class='flex-fill'>
            <div class="panel-head">
                <a href="{{ route('agent.wallet.index') }}" class="mr-auto ml-0">
                    <i class="icon-account_balance_wallet"></i>
                    Wallet (₹{{ number_format($total_amount, 2) }})
                </a>
                <a href="{{ route('agent.dashboard') }}"><i class="icon-bell-o"></i> Notification</a>
                <a href="{{ route('agent.logout') }}"><i class="icon-logout"></i> Logout</a>
            </div>
            <div class="container-fluid py-4">
                @yield('content')
            </div>
        </div>
    </div>

    <script src="{{ url('admin/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ url('admin/js/select2.js') }}" defer></script>
    <script src="{{ url('js/sweetalert.min.js') }}" type="module" defer></script>
    <script src="{{ url('js/agent/script.js') }}" type="module" defer></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" defer></script>
    @yield('footer_script')
</body>

</html>
@else
@include('agent.login')
@endif