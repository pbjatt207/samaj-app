<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ url('admin/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/agent/style.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Agent Login</title>
</head>
<body>
    <section
        class="d-flex align-self-center vh-100"
    >
        <div
            class="LoginBox"
        >
            <div
                class="card"
            >
                <div
                    class="card-body"
                >
                    <div
                        class="mb-3 text-center"
                    >
                        <img src="{{ url('imgs/logo.png') }}" alt="SN Power Tech" class="mw-100">
                    </div>
                    <div id="divSuccessMsg"></div>
                    <form 
                        action="{{ route('agent.login.post') }}" 
                        method="post"
                        id="agentLoginForm"
                        class="ajax-form"
                    >
                        @csrf
                        <div
                            class="form-group"
                        >
                            <label for="mobile_no">Mobile</label>
                            <input 
                                type="tel"
                                minlength="10"
                                maxlength="10"
                                class="form-control"
                                placeholder="Enter Your Mobile Number"
                                name="mobile"
                                id="mobile_no"
                                required
                            >
                        </div>
                        <button
                            type="submit"
                            class="btn btn-primary btn-block"
                        >Send OTP</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <script src="{{ url('admin/js/jquery-3.3.1.min.js') }}"></script>
    <script type="module" src="{{ url('js/agent/script.js') }}"></script>
</body>
</html>
