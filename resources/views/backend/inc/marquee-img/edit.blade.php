	@extends('backend.layout.master')

	@section('title','Marquee-img')

	@section('contant')


	<div class="content-wrapper mt-5 p-5">
		<h2 class="text-center py-5">Edit Image</h2>
		@if( $errors->any() )
		<div class="alert alert-danger" style="color: red;">
			@foreach($errors->all() as $error)
			<li>{!! $error !!}</li>
			@endforeach
		</div>
		@endif

		@if (\Session::has('success'))
		<div class="alert alert-success" style="color: green">
		{!! \Session::get('success') !!}</li>
	</div>
	@endif

	@if (\Session::has('danger'))
	<div class="alert alert-danger" style="color: red;">
	{!! \Session::get('danger') !!}</li>
</div>
@endif
<!--  -->
{{ Form::open(['method'=>'POST', 'files' => 'true', 'class' => 'user']) }}
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			{{ Form::label('title', 'Page Title')}}
			{{ Form::text('title', '', ['class' => 'form-control', 'id'=>'name', 'placeholder'=>'Item name','required'=>'required'])}}
		</div>
		
	</div>
	<div class="col-lg-6">

		<div class="form-group">

			
			{{Form::label('image', ' Choose image')}}
			<div class="">
				
				<!-- <input type="hidden" name="record[image]" value="" id="image"> -->
				{{ Form::file('image', ['class' => 'form-control']) }}
			</div>
			<img src="{{url('imgs/marquee/'.$edit->image)}}" width="100">
		</div>
		
	</div>	
	<div class="text-right mt-4">
		{{ Form::submit('Save', ['class' => 'login-btn btn btn-orange']) }}
	</div>
</div>
	{{ Form::close() }}



@stop






