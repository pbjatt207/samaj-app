@extends('backend.layout.master')

@section('title','dashboard')

@section('contant')

<div class="content-main-block mrg-t-40">
  	<h4 class="admin-form-text">Dashboard</h4>
    <div class="row">
    	<div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="{{url('admin-control/user')}}" class="small-box z-depth-1 hoverable bg-aqua default-color">
          <div class="inner">
            <h3>{{ $user_count }}</h3>
            <p>Total Users</p>
          </div>
          <div class="icon">
            <i class="material-icons">people_outline</i>
          </div>
        </a>
      </div>
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="{{url('admin-control/category')}}" class="small-box z-depth-1 hoverable bg-red danger-color">
          <div class="inner">
            <h3>{{ $category_count }}</h3>
            <p>Total Category</p>
          </div>
          <div class="icon">
            <i class="material-icons">card_membership</i>
          </div>
        </a>
      </div>      
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="{{url('admin-control/blog')}}" class="small-box z-depth-1 hoverable bg-green warning-color">
          <div class="inner">
            <h3>{{ $blog_count }}</h3>
            <p>Total Blog</p>
          </div>
          <div class="icon">
            <i class="material-icons">card_giftcard</i>
          </div>
        </a>
      </div>
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="{{url('admin-control/product')}}" class="small-box z-depth-1 hoverable bg-green success-color">
          <div class="inner">
            <h3>{{ $product_count }}</h3>
            <p>Total Product</p>
          </div>
          <div class="icon">
            <i class="material-icons">monetization_on</i>
          </div>
        </a>
      </div>
     <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="{{url('admin-control/slider')}}" class="small-box z-depth-1 hoverable bg-yellow secondary-color">
          <div class="inner">
            <h3>{{ $slider_count }}</h3>
            <p> Total Slider</p>
          </div>
          <div class="icon">
            <i class="material-icons">monetization_on</i>
          </div>
        </a>
      </div>
	  <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="{{url('admin-control/faqs')}}" class="small-box z-depth-1 hoverable bg-yellow secondary-color">
          <div class="inner">
            <h3>{{ $faq_count }}</h3>
            <p>Total Faqs</p>
          </div>
          <div class="icon">
            <i class="material-icons">monetization_on</i>
          </div>
        </a>
      </div>
	  <!--<div class="col-lg-3 col-xs-6">-->
        <!-- small box -->
   <!--     <a href="{{url('admin/coupon')}}" class="small-box z-depth-1 hoverable bg-aqua pink darken-2">-->
   <!--       <div class="inner">-->
   <!--         <h3>232</h3>-->
   <!--         <p>  Total Expired Coupon </p>-->
   <!--       </div>-->
   <!--       <div class="icon">-->
   <!--         <i class="material-icons">monetization_on</i>-->
   <!--       </div>-->
   <!--     </a>-->
   <!--   </div>-->
	  
    <!--  <div class="col-lg-3 col-xs-6">
        <!-- small box -->
       <!-- <a href="{{url('admin/discussion')}}" class="small-box z-depth-1 hoverable bg-aqua pink darken-2">
          <div class="inner">
            <h3></h3>
            <p>Total Discussion</p>
          </div>
          <div class="icon">
            <i class="material-icons">attach_money</i>
          </div>
        </a>
      </div>
	  -->
	  
	  
    </div>
  </div>

@stop
