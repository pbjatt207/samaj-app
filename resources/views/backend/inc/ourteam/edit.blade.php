@extends('backend.layout.master')

@section('title','ourteam')

@section('contant')
<div class="admin-form-main-block mrgn-t-40">
    <h4 class="admin-form-text"><a href="{{url('admin/ourteam')}}" data-toggle="tooltip" data-original-title="Go back" class="btn-floating"><i class="material-icons">reply</i></a> Edit ourteam</h4> 
    {!! Form::open(['method' => 'POST', 'files' => true]) !!}
      <div class="row admin-form-block z-depth-1">
        <div class="col-md-6">
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              {!! Form::label('name', 'ourteam Name / Title') !!} 
              {!! Form::text('name', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('name') }}</small>
          </div> 
		  
		  
		   <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
              {!! Form::label('slug', 'ourteam Slug') !!} - <p class="inline info"></p>
              {!! Form::text('slug', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('slug') }}</small>
          </div> 
           <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
              {!! Form::label('position', 'ourteam Position') !!} - <p class="inline info"></p>
              {!! Form::text('position', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('position') }}</small>
          </div> 
		  <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
              {!! Form::label('message', 'ourteam Message') !!} - <p class="inline info"></p>
              {!! Form::text('message', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('message') }}</small>
          </div> 
		  
         <!-- <div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }} currency-symbol-block">
            {!! Form::label('icon', 'Category Icon / Symbol') !!}
            <p class="inline info"> - Please select catgeory symbol or category image</p>
              <div class="input-group">
                {!! Form::text('icon', null, ['class' => 'form-control category-icon-picker']) !!}
                <span class="input-group-addon simple-input"><i class="glyphicon glyphicon-th-large"></i></span>
              </div>
            <small class="text-danger">{{ $errors->first('icon') }}</small>
          </div> -->
		 
		  
       
          <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }} input-file-block">
            {!! Form::label('image', 'ourteam Image') !!} 
            {!! Form::file('image', ['class' => 'input-file', 'id'=>'image']) !!}
            <label for="image" class="btn btn-danger js-labelFile" data-toggle="tooltip" data-original-title="ourteam Image">
              <i class="icon fa fa-check"></i>
              <span class="js-fileName">Choose a File</span>
            </label>
            <p class="info">Choose custom image</p>
            <small class="text-danger">{{ $errors->first('image') }}</small>
          </div> 
                 
          <div class="btn-group pull-right">
            <button type="reset" class="btn btn-info">Reset</button>
            <button type="submit" class="btn btn-success">Create</button>
          </div>
          <div class="clear-both"></div>
        </div>  
      </div>
    {!! Form::close() !!}
  </div>
@stop
		    	
				    

			
		
	
