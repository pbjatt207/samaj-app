 @extends('backend.layout.master')

@section('title','ourteam')

@section('contant')
		<div class="content-main-block  mrg-t-40">
    <div class="admin-create-btn-block">
      <a href="{{ url('admin-control/ourteam/add') }}" class="btn btn-danger btn-md">Add Ourteam</a>
      <!-- Delete Modal -->
      {{ Form::open( ['url' => url('admin-control/ourteam/removeMultiple'), 'method'=>'post'] ) }}
      <button type="button" class="btn btn-danger btn-md btn-remove" data-toggle="modal" data-target="#bulk_delete"><i class="material-icons left">delete</i> Delete Selected</button>   
      {{ Form::close() }}
      <!-- Modal -->
      
    </div>
    <div class="content-block box-body">
      <table id="full_detail_table" class="table table-hover table-responsive">
        <thead>
          <tr class="table-heading-row">
            <th>
              <div class="inline">
                <input id="checkboxAll" type="checkbox" class="filled-in check" name="checked[]" value="all" id="checkboxAll">
                <label for="checkboxAll" class="material-checkbox"></label>
              </div>
            #</th>
            <th>Image </th>
            <th>Title</th>
            <th>slug</th>
            <th>Actions</th>
          </tr>
        </thead>
          @if (isset($ourteam))
          <tbody>
            @foreach ($ourteam as $key => $item)
              <tr>
                <td>
                  <div class="inline">
                    <input type="checkbox" class="filled-in material-checkbox-input check" name="checked[]" value="" id="checkbox{{$item->id}}">
                    <label for="checkbox{{$item->id}}" class="material-checkbox"></label>
                  </div>
                  {{$key+1}}
                </td>
                <td>
                  @if($item->icon != null)
                    <i class="fa {{$item->icon}}" style="font-size: 30px;"></i>
                  @elseif($item->image != null)
                    <img src="{{ asset('imgs/ourteams/'.$item->image) }}" class="img-responsive" width="80" alt="image">
                  @else
                    N/A  
                  @endif
                </td>
                <td>{{$item->name}}</td>
                <td>{{$item->slug}}</td>
                <td>
                  <div class="admin-table-action-block">
                    <a href="{{url('admin-control/ourteam/edit/'.$item->id)}}" data-toggle="tooltip" data-original-title="Edit" class="btn-info btn-floating"><i class="material-icons">mode_edit</i></a>
                    <!-- Delete Modal -->
                    <button type="button" class="btn-danger btn-floating" data-toggle="modal" data-target="#{{$item->id}}deleteModal"><i class="material-icons">delete</i> </button>
                    <!-- Modal -->
                    <div id="{{$item->id}}deleteModal" class="delete-modal modal fade" role="dialog">
                      <div class="modal-dialog modal-sm">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <div class="delete-icon"></div>
                          </div>
                          <div class="modal-body text-center">
                            <h4 class="modal-heading">Are You Sure ?</h4>
                            <p>Do you really want to delete these records? This process cannot be undone.</p>
                          </div>
                          <div class="modal-footer">
                            {!! Form::open(['url' => url('admin-control/ourteam/remove/'.$item->id),'method' => 'GET', $item->id]) !!}
                                <button type="reset" class="btn btn-gray translate-y-3" data-dismiss="modal">No</button>
                                <button type="submit" class="btn btn-danger">Yes</button>
                            {!! Form::close() !!}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        @endif  
      </table>
    </div>
  </div>
@stop