    @extends('backend.layout.master')

    @section('title','setting')

    @section('contant')
    <div class="admin-form-main-block mrgn-t-40">
        <h4 class="admin-form-text"><a href="{{url('admin-control/slider')}}" data-toggle="tooltip" data-original-title="Go back" class="btn-floating"><i class="material-icons">reply</i></a>Setting</h4> 
        {!! Form::open(['method' => 'POST', 'files' => true]) !!}
          <div class="row admin-form-block z-depth-1">
            <div class="col-md-12">

              <div class="col-md-6 form-group{{ $errors->has('site_title') ? ' has-error' : '' }}">
                  {!! Form::label('site_title', 'Site Name') !!} - <p class="inline info">Like electronics, clothing</p>
                  {!! Form::text('site_title', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('site_title') }}</small>
              </div>
              <div class="col-md-6 form-group{{ $errors->has('tagline') ? ' has-error' : '' }}">
                {!! Form::label('tagline', 'Tag Line') !!}
                {!! Form::text('tagline', null, ['class' => 'form-control']) !!}
                <small class="text-danger">{{ $errors->first('tagline') }}</small>
              </div>
              <div class="col-md-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::label('email', 'Email') !!}
                {!! Form::text('email', null, ['class' => 'form-control']) !!}
                <small class="text-danger">{{ $errors->first('email') }}</small>
              </div>
              <div class="col-md-6 form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                {!! Form::label('mobile', 'Mobile No') !!}
                {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
                <small class="text-danger">{{ $errors->first('mobile') }}</small>
              </div>
              <div class="col-md-6 form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                  {!! Form::label('address', 'Address') !!} - <p class="inline info"></p>
                  {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('address') }}</small>
              </div>
              
              <div class="col-md-6 form-group{{ $errors->has('facebook_url') ? ' has-error' : '' }}">
                {!! Form::label('facebook_url', 'Facebook Url') !!}
                {!! Form::text('facebook_url', null, ['class' => 'form-control']) !!}
                <small class="text-danger">{{ $errors->first('facebook_url') }}</small>
              </div>
              <div class="col-md-6 form-group{{ $errors->has('twitter_url') ? ' has-error' : '' }}">
                {!! Form::label('twitter_url', 'Twitter Url') !!}
                {!! Form::text('twitter_url', null, ['class' => 'form-control']) !!}
                <small class="text-danger">{{ $errors->first('twitter_url') }}</small>
              </div>
              <div class="col-md-6 form-group{{ $errors->has('insta_url') ? ' has-error' : '' }}">
                {!! Form::label('insta_url', 'Instagram Url') !!}
                {!! Form::text('insta_url', null, ['class' => 'form-control']) !!}
                <small class="text-danger">{{ $errors->first('insta_url') }}</small>
              </div>
              <div class="col-md-6 form-group{{ $errors->has('youtube_url') ? ' has-error' : '' }}">
                {!! Form::label('youtube_url', 'Youtube Url') !!}
                {!! Form::text('youtube_url', null, ['class' => 'form-control']) !!}
                <small class="text-danger">{{ $errors->first('youtube_url') }}</small>
              </div>
              <div class="col-md-6 form-group{{ $errors->has('pinterest_url') ? ' has-error' : '' }}">
                {!! Form::label('pinterest_url', 'Pinterest Url') !!}
                {!! Form::text('pinterest_url', null, ['class' => 'form-control']) !!}
                <small class="text-danger">{{ $errors->first('pinterest_url') }}</small>
              </div>
              
              <div class="col-md-6 form-group{{ $errors->has('logo') ? ' has-error' : '' }} input-file-block">
                <img src="{{ url('imgs/'.$edit->logo) }}" width="50px">
                {!! Form::label('logo', 'Logo') !!} 
                {!! Form::file('logo', ['class' => 'input-file', 'id'=>'logo']) !!}
                <label for="logo" class="btn btn-danger js-labelFile" data-toggle="tooltip" data-original-title="logo">
                  <i class="icon fa fa-check"></i>
                  <span class="js-fileName">Choose a File</span>
                </label>
                <p class="info">Choose custom logo</p>
                <small class="text-danger">{{ $errors->first('logo') }}</small>
              </div>
               <div class="col-md-6 form-group{{ $errors->has('favicon') ? ' has-error' : '' }} input-file-block">
                <img src="{{ url('imgs/'.$edit->favicon) }}" width="50px">
                {!! Form::label('favicon', 'favicon') !!} 
                {!! Form::file('favicon', ['class' => 'input-file', 'id'=>'favicon']) !!}
                <label for="favicon" class="btn btn-danger js-labelFile" data-toggle="tooltip" data-original-title="favicon">
                  <i class="icon fa fa-check"></i>
                  <span class="js-fileName">Choose a File</span>
                </label>
                <p class="info">Choose custom favicon</p>
                <small class="text-danger">{{ $errors->first('favicon') }}</small>
              </div>

              <h3 class="col-lg-12 my-5">Feature</h3>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('feature_icon_i') ? ' has-error' : '' }} currency-symbol-block">
                        {!! Form::label('feature_icon_i', 'Feature Icon / Symbol 1') !!}
                        <p class="inline info"> - Please select feature symbol or category image</p>
                          <div class="input-group">
                            {!! Form::text('feature_icon_i', null, ['class' => 'form-control category-icon-picker']) !!}
                            <span class="input-group-addon simple-input"><i class="glyphicon glyphicon-th-large"></i></span>
                          </div>
                        <small class="text-danger">{{ $errors->first('feature_icon_i') }}</small>
                      </div>
                    {{ Form::label('feature_title_i') }}
                    {{ Form::text('feature_title_i', '', ['class' => 'form-control']) }}
                    {{ Form::label('feature_excerpt_i') }}
                    {{ Form::text('feature_excerpt_i', '', ['class' => 'form-control']) }}
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('feature_icon_ii') ? ' has-error' : '' }} currency-symbol-block">
                        {!! Form::label('feature_icon_ii', 'Feature Icon / Symbol 2') !!}
                        <p class="inline info"> - Please select feature symbol or category image</p>
                          <div class="input-group">
                            {!! Form::text('feature_icon_ii', null, ['class' => 'form-control category-icon-picker']) !!}
                            <span class="input-group-addon simple-input"><i class="glyphicon glyphicon-th-large"></i></span>
                          </div>
                        <small class="text-danger">{{ $errors->first('feature_icon_ii') }}</small>
                      </div>
                    {{ Form::label('feature_title_ii') }}
                    {{ Form::text('feature_title_ii', '', ['class' => 'form-control']) }}
                    {{ Form::label('feature_excerpt_ii') }}
                    {{ Form::text('feature_excerpt_ii', '', ['class' => 'form-control']) }}
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('feature_icon_iii') ? ' has-error' : '' }} currency-symbol-block">
                        {!! Form::label('feature_icon_iii', 'Feature Icon / Symbol 3') !!}
                        <p class="inline info"> - Please select feature symbol or category image</p>
                          <div class="input-group">
                            {!! Form::text('feature_icon_iii', null, ['class' => 'form-control category-icon-picker']) !!}
                            <span class="input-group-addon simple-input"><i class="glyphicon glyphicon-th-large"></i></span>
                          </div>
                        <small class="text-danger">{{ $errors->first('feature_icon_iii') }}</small>
                      </div>
                    {{ Form::label('feature_title_iii') }}
                    {{ Form::text('feature_title_iii', '', ['class' => 'form-control']) }}
                    {{ Form::label('feature_excerpt_iii') }}
                    {{ Form::text('feature_excerpt_iii', '', ['class' => 'form-control']) }}
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('feature_icon_iv') ? ' has-error' : '' }} currency-symbol-block">
                        {!! Form::label('feature_icon_iv', 'Feature Icon / Symbol 4') !!}
                        <p class="inline info"> - Please select feature symbol or category image</p>
                          <div class="input-group">
                            {!! Form::text('feature_icon_iv', null, ['class' => 'form-control category-icon-picker']) !!}
                            <span class="input-group-addon simple-input"><i class="glyphicon glyphicon-th-large"></i></span>
                          </div>
                        <small class="text-danger">{{ $errors->first('feature_icon_iv') }}</small>
                      </div>
                    {{ Form::label('feature_title_iv') }}
                    {{ Form::text('feature_title_iv', '', ['class' => 'form-control']) }}
                    {{ Form::label('feature_excerpt_iv') }}
                    {{ Form::text('feature_excerpt_iv', '', ['class' => 'form-control']) }}
                </div>
              <div class="btn-group pull-right">
                <button type="submit" class="btn btn-success">Update</button>
              </div>
              <div class="clear-both"></div>
            </div>  
          </div>
        {!! Form::close() !!}
      </div>
@stop






