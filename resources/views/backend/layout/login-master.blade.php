@php

$setting = App\Model\Setting::findOrFail(1);

@endphp

<!DOCTYPE html>
<html lang="en">

<head>
	<link rel="icon" type="image/png" sizes="20x20" href="{{url('imgs/'.$setting->fav_icon)}}">
	<title>@yield('title')</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="icon" type="image/png" href="images/icons/favicon.ico" />

	{{Html::style('admin/vendor/bootstrap/css/bootstrap.min.css')}}
	{{Html::style('admin/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}
	{{Html::style('admin/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}
	{{Html::style('admin/vendor/animate/animate.css')}}
	{{Html::style('admin/vendor/css-hamburgers/hamburgers.min.css')}}
	{{Html::style('admin/vendor/select2/select2.min.css')}}
	{{Html::style('admin/css/util.css')}}
	{{Html::style('admin/css/main.css')}}
</head>

<body>
	@yield('contant')
</body>
<script src="{{url('admin/vendor/jquery/jquery-3.2.1.min.js')}}" type="text/javascript"></script>
<script src="{{url('admin/vendor/bootstrap/js/popper.js')}}" type="text/javascript"></script>
<script src="{{url('admin/vendor/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{url('admin/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
<script src="{{url('admin/js/main.js')}}" type="text/javascript"></script>

</html>