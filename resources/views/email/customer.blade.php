<!DOCTYPE html>
<html>
<head>
	<title>{{ $c_subject }}</title>
</head>
<body>
	<div style="max-width: 500px; margin: 15px auto; border: 1px solid #ccc; font-family: Arial;">
		<div style="background: #fff; padding: 20px 15px; text-align: center">
			<img src="{{ url('imgs/logo.png') }}" alt="" style="width:50%;">
		</div>
		<div style="background: #79e6cf; padding: 15px;">
			<h2 style="font-weight: normal;">Thank You !!</h2>
			<p> Your Application has been Submitted Successfully.</p>
		</div>
		
	</div>
</body>
</html>
