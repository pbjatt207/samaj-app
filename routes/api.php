<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['as' => 'api.', 'namespace' => 'api'], function () {

    Route::get('city/list', 'CityController@list')->name('city.list');
    Route::get('state/list', 'StateController@list')->name('state.list');
    Route::get('country/list', 'CountryController@list')->name('country.list');

    Route::get('language/list', 'LanguageController@list')->name('language.list');
    Route::get('religion/list', 'ReligionController@list')->name('religion.list');
    Route::get('cast/list', 'CastController@list')->name('cast.list');
    Route::get('gotra/list', 'GotraController@list')->name('gotra.list');
    Route::get('profession/list', 'ProfessionController@list')->name('profession.list');
    Route::get('meritalstatus/list', 'MeritalStatusController@list')->name('meritalstatus.list');
    Route::get('feeling/list', 'FeelingController@list')->name('feeling.list');
    Route::get('role/list', 'RoleController@list')->name('role.list');

    Route::get('site/list', 'SiteController@list')->name('site.list');
    Route::get('user/list', 'UserController@list')->name('user.list');
    Route::get('post/list', 'PostController@list')->name('post.list');
    Route::get('place/list', 'PlaceController@list')->name('place.list');
    Route::get('event/list', 'EventController@list')->name('event.list');
    Route::get('prayer/list', 'PrayerController@list')->name('prayer.list');
    Route::get('tag/list', 'TagController@list')->name('tag.list');

    Route::post('login', 'LoginController@login')->name('login');
    Route::post('sendotp', 'LoginController@sendotp')->name('sendotp');
    Route::post('verifyotp', 'LoginController@verifyotp')->name('verifyotp');
    Route::post('forgotpassword', 'LoginController@forgotpassword')->name('forgotpassword');

    Route::apiResource('user', 'UserController')->only(['store']);
    Route::get('search_user', 'UserController@search_user')->name('user.search_user');
    Route::apiResources([
        'country'       => 'CountryController',
        'state'         => 'StateController',
        'city'          => 'CityController',

        'language'      => 'LanguageController',
        'religion'      => 'ReligionController',
        'cast'          => 'CastController',
        'gotra'         => 'GotraController',
        'profession'    => 'ProfessionController',
        'meritalstatus' => 'MeritalStatusController',
        'feeling'       => 'FeelingController',
        'role'          => 'RoleController',
        'relation'      => 'RelationController',

        'site'          => 'SiteController',

        'user'          => 'UserController',
    ]);
});

Route::group(['middleware' => 'auth:api', 'as' => 'api.', 'namespace' => 'api'], function () {

    Route::get('homepage-posts', 'HomeController@posts')->name('home.posts');
    Route::get('notification', 'HomeController@notification')->name('home.notification');

    Route::get('post/reaction', 'PostController@reaction')->name('post.reaction');
    Route::post('post/reaction', 'PostController@reactionadd')->name('post.reactionadd');

    Route::get('post/view', 'PostController@view')->name('post.view');
    Route::post('post/view', 'PostController@viewadd')->name('post.viewadd');

    Route::get('post/report', 'PostController@report')->name('post.report');
    Route::post('post/report', 'PostController@reportadd')->name('post.reportadd');

    Route::get('comment/reaction', 'CommentPostController@reaction')->name('comment.reaction');
    Route::post('comment/reaction', 'CommentPostController@reactionadd')->name('comment.reactionadd');

    Route::get('event/reaction', 'EventController@reaction')->name('event.reaction');
    Route::post('event/reaction', 'EventController@reactionadd')->name('event.reactionadd');

    Route::get('connection/list', 'ConnectionController@connection_list')->name('connection.list');
    Route::post('connection/delete', 'ConnectionController@delete')->name('connection.delete');

    Route::get('logout', 'LoginController@logout')->name('logout');
    Route::post('changepassword', 'LoginController@changepassword')->name('changepassword');

    Route::post('user/{id}', 'UserController@update')->name('user.update');
    Route::post('user-detail/{id}', 'UserController@updateDetail')->name('user.updatedetail');

    Route::apiResource('user', 'UserController')->only(['index', 'list', 'destroy']);

    Route::apiResources([
        'event'         => 'EventController',
        'place'         => 'PlaceController',
        'prayer'        => 'PrayerController',
        'tag'           => 'TagController',
        'post'          => 'PostController',
        'comment'       => 'CommentPostController',
        'userrelation'  => 'UserRelationController',
        'connection'    => 'ConnectionController',
    ]);
});
