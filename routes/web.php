<?php

use Illuminate\Support\Facades\Route;

Route::get('/clear', function () {
    return \Artisan::call('optimize:clear');
});

// Route::get('/', 'HomeController@index');
// Route::get('/service/{slug:slug}', 'ServiceController@single');
// Route::post('contact/ajax_request', 'AjaxController@index')->name('ajax-route');
// Route::get('/blog', 'BlogController@index');
// Route::get('/blog/{slug:slug}', 'BlogController@single');
// Route::get('about', 'AboutController@index');
// Route::get('ourteam', 'OurTeamController@index');
// Route::get('contact', 'ContactController@index');

/**
 * Admin Panel
 */
Route::group(['prefix' => 'rt-panel', 'namespace' => 'administrator', 'as' => 'admin.'], function () {
    Route::get('/logout', 'AuthController@logout')->name('logout');
    Route::post('/login', 'AuthController@login')->name('login.post');
    Route::get('/change_password', 'AuthController@change_password_view')->name('adminchange.password');
    Route::post('/change_password', 'AuthController@change_password');
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::group(['middleware' => 'admin'], function () {

        Route::get('event/status/{event}', 'EventController@status')->name('event.status');

        Route::resources([
            'wallet'        => 'WalletController',
            'country'       => 'CountryController',
            'state'         => 'StateController',
            'city'          => 'CityController',

            'language'      => 'LanguageController',
            'religion'      => 'ReligionController',
            'cast'          => 'CastController',
            'gotra'         => 'GotraController',
            'profession'    => 'ProfessionController',
            'meritalstatus' => 'MeritalStatusController',
            'feeling'       => 'FeelingController',
            'role'          => 'RoleController',

            'site'          => 'SiteController',
            'user'          => 'UserController',
            'post'          => 'PostController',
            'place'         => 'PlaceController',
            'event'         => 'EventController',
            'prayer'        => 'PrayerController',
            'tag'           => 'TagController',
        ]);
    });
});
