let validation = function (form) {
    let is_success = true;
    $(form).attr('novalidate');
    $(form).find('.invalid-feedback').remove();
    $(form).find('*').removeClass('is-invalid');

    $(form).find('[required]').not('[type=radio], [type=checkbox]').each(function () {
        if ($(this).val() == undefined || $(this).val() == '' || $(this).val() == null) {
            is_success = false;
            $(this).closest('.form-group').append(
                '<div class="invalid-feedback">Please fill required field.</div>'
            );
            $(this).addClass('is-invalid');
        }
    });

    return is_success;
};
export default validation;
