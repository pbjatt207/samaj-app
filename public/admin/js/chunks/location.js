$(function () {
    // Get States
    $(document).on('change', '.country', function () {
        let target = $(this).data('target') ?? '.state';
        $(target).attr('disabled', 'disabled').html('<option value="">Loading</option>');
        $.ajax({
            url: `/samaj-app/api/state/?type=all&country_id=${$(this).val()}`,
            success: function (res) {
                $(target).removeAttr('disabled').html('<option value="">Select State</option>');
                for (let id in res) {
                    $(target).append(`<option value=${id}>${res[id]}</option>`);
                }
            }
        });
    });

    // Get Cities
    $(document).on('change', '.state', function () {
        let target = $(this).data('target') ?? '.city';
        $(target).attr('disabled', 'disabled').html('<option value="">Loading</option>');
        $.ajax({
            url: `/samaj-app/api/city/?type=all&state_id=${$(this).val()}`,
            success: function (res) {
                $(target).removeAttr('disabled').html('<option value="">Select State</option>');
                for (let id in res) {
                    $(target).append(`<option value=${id}>${res[id]}</option>`);
                }
            }
        });
    });
});
