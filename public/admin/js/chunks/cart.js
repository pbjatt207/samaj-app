localStorage.removeItem('cartItems');
let cartItems = localStorage.getItem('cartItems');
cartItems = cartItems ? JSON.parse(cartItems) : {};

const update_cart = function (cartItems) {
    $('.cart-btn-group').each(function () {
        let id = $(this).data('item_id');

        if (cartItems[id]) {
            $(this).find('.input-sppiner').show().find('input[type=number]').val(cartItems[id].qty);
            $(this).find('.add_to_cart_btn').hide();
        } else {
            $(this).find('.input-sppiner').hide().find('input[type=number]').val(0);
            $(this).find('.add_to_cart_btn').show();
        }
    });

    let html = '', subtotal = 0, total = 0, gstAmt = 0, price, qty;

    if (Object.keys(cartItems).length > 0) {

        for (let id in cartItems) {
            price = $('.cart-btn-group').data('type') && $('.cart-btn-group').data('type') === 'sale' ? cartItems[id].sale_price : cartItems[id].purchase_price;
            qty = cartItems[id].qty;
            subtotal = price * qty;
            total += parseFloat(subtotal);
            gstAmt += parseFloat(subtotal * cartItems[id].gst_rate / 100);

            html += `
            <div class="row align-items-center">
                <div class="col-4">
                    ${cartItems[id].name}
                </div>
                <div class="col-4">
                    ₹${price} x ${qty}
                </div>
                <div class="col-4">
                    ₹${subtotal}
                </div>
            </div>`;
        }

        html += `<hr>
        <div class="row align-items-center">
            <div class="col-8">
                <div>
                    Net Total
                </div>
            </div>
            <div class="col-4">
                ₹${total}
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-8">
                <div>
                    @GST
                </div>
            </div>
            <div class="col-4">
                ₹${gstAmt}
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-8">
                <div>
                    <span class="float-right">(-)</span>
                    Round-Off
                </div>
            </div>
            <div class="col-4">
                ₹${(total + gstAmt) - Math.floor(total + gstAmt)}
            </div>
        </div>
        <hr>
        <div class="row align-items-center" style="font-size: 18px; font-weight: bold;">
            <div class="col-8">
                <div>
                    TOTAL
                </div>
            </div>
            <div class="col-4">
                ₹${Math.floor(total + gstAmt)}
            </div>
        </div>
        `;

        $('.submit-btn').removeAttr('disabled');
    } else {
        $('.submit-btn').attr('disabled', 'disabled');
        html = `No items added to cart.`;
    }

    $('#cartCaculations').html(html);

    cartItems = JSON.stringify(cartItems);
    localStorage.setItem('cartItems', cartItems);

    $('#cart_items_input').val(cartItems);
}

const add_to_cart = function (product, qty) {
    product.qty = parseInt(qty);
    cartItems[product.id] = product;
    update_cart(cartItems);
}

$(function () {
    update_cart(cartItems);

    $(document).on('click', '.add_to_cart_btn', function () {
        let qty = 1,
            product = $(this).data('item');
        add_to_cart(product, qty);
    });

    $(document).on('input', '.input-sppiner input[type=number]', function () {
        let id = $(this).closest('.cart-btn-group').data('item_id');
        if ($(this).val() != '' && $(this).val() > 0) {
            cartItems[id].qty = parseInt($(this).val());
        } else if (parseInt($(this).val()) < 0) {
            cartItems[id].qty = 1;
        }
        update_cart(cartItems);
    });

    $(document).on('click', '.input-sppiner .plus-btn', function () {
        let id = $(this).closest('.cart-btn-group').data('item_id');

        cartItems[id].qty += 1;
        update_cart(cartItems);
    });

    $(document).on('click', '.input-sppiner .minus-btn', function () {
        let id = $(this).closest('.cart-btn-group').data('item_id');
        if (cartItems[id].qty > 1) {
            cartItems[id].qty -= 1;
        } else {
            delete cartItems[id];
        }

        update_cart(cartItems);
    });
});
