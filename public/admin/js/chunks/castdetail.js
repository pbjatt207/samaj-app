$(function () {
    // Get States
    $(document).on('change', '.religion', function () {
        let target = $(this).data('target') ?? '.cast';
        $(target).attr('disabled', 'disabled').html('<option value="">Loading</option>');
        $.ajax({
            url: `/samaj-app/api/cast/?type=all&religion_id=${$(this).val()}`,
            success: function (res) {
                $(target).removeAttr('disabled').html('<option value="">Select Cast</option>');
                for (let id in res) {
                    $(target).append(`<option value=${id}>${res[id]}</option>`);
                }
            }
        });
    });

    // Get Cities
    $(document).on('change', '.cast', function () {
        let target = $(this).data('target') ?? '.gotra';
        $(target).attr('disabled', 'disabled').html('<option value="">Loading</option>');
        $.ajax({
            url: `/samaj-app/api/gotra/?type=all&cast_id=${$(this).val()}`,
            success: function (res) {
                $(target).removeAttr('disabled').html('<option value="">Select Gotra</option>');
                for (let id in res) {
                    $(target).append(`<option value=${id}>${res[id]}</option>`);
                }
            }
        });
    });
});
