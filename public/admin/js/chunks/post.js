$(function () {
    /**
     * View City
     */
    let table_sel = $('.data-table');
    if (table_sel.length) {
        table_sel.DataTable({
            processing: true,
            serverSide: true,
            ajax: table_sel.data('ajax_url'),
            columns: [
                { "data": 'DT_RowIndex', "orderable": false, "searchable": false },
                // { "data": "id", "orderable": false, "searchable": false },
                { "data": "image" },
                { "data": "user_id" },
                { "data": "caption" },
                // { "data": "action", "orderable": false, "searchable": false },
            ],
        });
    }
});
